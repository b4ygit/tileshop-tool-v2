var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify-es').default,
    sourcemaps = require('gulp-sourcemaps')
    stripcomments = require('gulp-strip-comments');

var js_vendor_rules = {
	merge: [
        'theme/js/script.js', 
		'assets/plugins/jquery-3.2.1.min.js', 
        'assets/plugins/bootstrap-slider/bootstrap-slider.min.js',	
        'theme/js/script.js',
        'assets/plugins/modernizr-3.5.0.min.js', 
        'assets/plugins/bootstrap/js/bootstrap.bundle.min.js',
        'assets/plugins/sliderrange/nouislider.min.js',
        'assets/plugins/nice-select/jquery.nice-select.min.js',
        'assets/plugins/swiper/js/swiper.min.js',
        'assets/plugins/fabric/fabric-v3.6.1.min.js',
        'assets/html2canvas.min.js',
        'assets/dom-to-image.min.js',
        //'assets/js/plugins.js',
        'assets/plugins/wow/wow.min.js',
        'assets/plugins/vue/vue.js',
        'assets/new-assets/js/main.js',
    ],
	in: 'assets/dist/js',
	as: 'bundle.min.js',
	sourcemap: 'map'
};

var js_app_rules = {
	merge: [
        'assets/lib/tileshop-main.js', 
        'assets/lib/tileshop-type.js', 
        'assets/lib/tileshop-design-tool-const.js',
        'assets/lib/tileshop-design-tool-funcs.js',
        //'assets/lib/tileshop-design-tool.js',
        'assets/lib/tileshop-style-selected.js',
        'assets/lib/layerSync.js',
        'assets/lib/customJS.js',
        'assets/manufacturerJS.js',
    ],
	in: 'assets/dist/js',
    as: 'app.min.js',
    watch: 'assets/lib/*.js',
	sourcemap: 'map'
};

gulp.task('js:vendor', function () {
	return gulp.src(js_vendor_rules.merge) // Get these files
	.pipe(concat(js_vendor_rules.as)) // concat to app.min.js
	.pipe(uglify()) // compress
	.pipe(sourcemaps.init())
    .pipe(sourcemaps.write(js_vendor_rules.sourcemap))
    .pipe(stripcomments())
	.pipe(gulp.dest(js_vendor_rules.in)) // in 'assets/js/'
});

gulp.task('js:app', function () {
	return gulp.src(js_app_rules.merge) // Get these files
	.pipe(concat(js_app_rules.as)) // concat to app.min.js
	.pipe(uglify()) // compress
	.pipe(sourcemaps.init())
    .pipe(sourcemaps.write(js_app_rules.sourcemap))
    .pipe(stripcomments())
	.pipe(gulp.dest(js_app_rules.in)) // in 'assets/js/'
});

gulp.task('watch', function () {
	gulp.watch(js_app_rules.watch, ['js:app']);
});

gulp.task('default', gulp.series('js:vendor', 'js:app'));