<?php

function tiles_shop_configuration() {
    if ( !current_user_can('administrator') && !current_user_can("manage_tiles_shop"))  {
        wp_die( __( 'You do not have sufficient permissions to access this page. #' , 'tile-tool') );
    }
    add_menu_page( 'Tiles Shop Configuration', 'Tiles Shop Configuration', 'manage_options', 'tiles-shop-configuration', '', 'dashicons-warning', 59);
    add_submenu_page( 'tiles-shop-configuration', 'Tile Tool Logo', 'Tile Tool Logo', 'manage_options', 'logo-configuration', 'logo_configuration');
    add_submenu_page( 'tiles-shop-configuration', 'Visualizer Background Layer', 'Visualizer Background Layer', 'manage_options', 'visualizer-bg-layer-configuration', 'visualizer_bg_layer_configuration');
    add_submenu_page( 'tiles-shop-configuration', 'Customized Price Option', 'Customized Price Option', 'manage_options', 'customized-price-option', 'customized_price_option');
    add_submenu_page( 'tiles-shop-configuration', 'Blank Tile', 'Blank Tile', 'manage_options', 'blank-tile-configuration', 'blank_tile_configuration');
    add_submenu_page( 'tiles-shop-configuration', 'Color Hover', 'Color Hover', 'manage_options', 'color-hover-configuration', 'color_hover_configuration');
    add_submenu_page( 'tiles-shop-configuration', 'Social Share Urls', 'Social Share Urls', 'manage_options', 'social-share-urls', 'social_share_urls');
    remove_submenu_page('tiles-shop-configuration','tiles-shop-configuration');
}

function social_share_urls() {
    if ( !is_admin() && !current_user_can("manage_tiles_shop"))  {
        wp_die( __( 'You do not have sufficient permissions to access this page. #', 'tile-tool' ) );
    }
    echo viewSocialShareUrlsConfiguration();
}
function logo_configuration() {
    if ( !is_admin() && !current_user_can("manage_tiles_shop"))  {
        wp_die( __( 'You do not have sufficient permissions to access this page. #', 'tile-tool' ) );
    }
    echo viewLogoConfiguration();
}
function customized_price_option() {
    if ( !is_admin() && !current_user_can("manage_tiles_shop"))  {
        wp_die( __( 'You do not have sufficient permissions to access this page. #', 'tile-tool' ) );
    }
    echo viewCustomizedPrice();
}
function blank_tile_configuration() {
    if ( !is_admin() && !current_user_can("manage_tiles_shop"))  {
        wp_die( __( 'You do not have sufficient permissions to access this page. #', 'tile-tool' ) );
    }
    echo viewBlankTileConfiguration();
}
function color_hover_configuration() {
    if ( !is_admin() && !current_user_can("manage_tiles_shop"))  {
        wp_die( __( 'You do not have sufficient permissions to access this page. #', 'tile-tool' ) );
    }
    echo viewColorHoverConfiguration();
}
function visualizer_bg_layer_configuration() {
    if ( !is_admin() && !current_user_can("manage_tiles_shop"))  {
        wp_die( __( 'You do not have sufficient permissions to access this page. #', 'tile-tool' ) );
    }
    echo viewVisualizerBackgroundLayer();
}

function registerAdminConfigJs($hook) {
    $list = [
        'tiles-shop-configuration_page_logo-configuration',
        'tiles-shop-configuration_page_color-hover-configuration',
        'tiles-shop-configuration_page_customized-price-option',
        'tiles-shop-configuration_page_blank-tile-configuration',
        'tiles-shop-configuration_page_visualizer-bg-layer-configuration',
        'tiles-shop-configuration_page_social-share-urls'
    ];

    if(in_array($hook, $list)) {
        wp_register_script('adminConfigJS', plugins_url('assets/backend-config-js.js', __FILE__), [], false,
            true);
        wp_enqueue_script('adminConfigJS');
        wp_localize_script('adminConfigJS', 'adminConfigJsParams', [
            'ajax_url' => admin_url('admin-ajax.php'),
        ]);
    }
}

function registerManufacturerJs() {
    wp_register_script( 'manufacturerJS', plugins_url( 'assets/manufacturerJS.js', __FILE__ ), ['jquery'], false, true);
    wp_enqueue_script( 'manufacturerJS' );
    wp_localize_script('manufacturerJS', 'wpParams', [
        'ajax_url' => admin_url('admin-ajax.php'),
    ]);
}

add_action( 'admin_enqueue_scripts', 'registerAdminConfigJs' );
add_action( 'admin_menu', 'tiles_shop_configuration' );

add_action( 'wp_enqueue_scripts', 'registerManufacturerJs' );