<?php

function renderManufacturers()
{
  $html = '<ul class="nav nav-tabs">';
  $htmlShapes = '<div class="tab-content">';

  $manufacturers = get_posts([
    'post_type' => 'manufacturer',
    'post_status' => 'publish',
    'orderby'          => 'ID', 
    'order'            => 'DESC', 
  ]);

  $firstActive = true;
  foreach ($manufacturers as $manufacturer) {
    $active = $firstActive ? "active" : "";
    $relatedShapes = pods_field('manufacturer', $manufacturer->ID, 'shape', false);
    $html .= '
      <li class="nav-item">
        <a class="nav-link ' . $active . ' nav-item-tabs" href="javascript:void(0);" data-manu-id="' . $manufacturer->ID . '" data-tab-id="#' . $manufacturer->post_name . '">' . __($manufacturer->post_title, 'tile-tool') . ' (' . count($relatedShapes) . ')</a>
      </li>';

    $htmlShapes .= '<div class="tab-pane ' . $active . '" id="' . $manufacturer->post_name . '">';
    $htmlShapes .= '<p class="title-text-sharps-tiles">' . __($manufacturer->post_excerpt, 'tile-tool') . '</p>';
    $htmlShapes .= renderRelatedShape($manufacturer->ID, $relatedShapes, "manu_" . $manufacturer->post_name);
    $htmlShapes .= '</div>';
    $firstActive = false;
  }

  $html .= '</ul>';
  $htmlShapes .= '</div>';

  return $html . $htmlShapes;
}
function renderRelatedShape($manuId, $shapes, $swiperID)
{
  $html = '
      <div class="content-slider">
        <div class="swiper-container-home" id="' . $swiperID . '" data-slides-perview="3" data-space-between="16" data-slides-tablet="3"  data-slides-mobile="1" data-slides-loop="false" data-slides-pergroup="1">
          <div class="swiper-wrapper">';

  foreach ($shapes as $idx => $_shape) {
    $_shapeSlug = $_shape['slug'];

    // Get sizes for checking valid shape
    $_sizes = get_terms([
      'taxonomy' => "pa_$_shapeSlug-size",
      'hide_empty' => false,
    ]);
    if (empty($_sizes) || !empty($_sizes->errors)) {
      continue;
    }
    // Get summary from the each shape
    global $wpdb;
    $manuShapeMetadata = $wpdb->get_row($wpdb->prepare(
      "SELECT * FROM " . ($wpdb->prefix . 'tool_manufacturer_shape_data') . " WHERE manu_id=%d AND shape_slug=%s LIMIT 1",
      $manuId,
      $_shapeSlug
    ));
    $summary = '';
    $isEnabled = true;
    if (!empty($manuShapeMetadata)) {
      $summary = $manuShapeMetadata->summary;
      $isEnabled = $manuShapeMetadata->is_enabled;
    }
    if (!$isEnabled && !current_user_can('administrator')) continue;
    // Get extra data for term and taxonomy
    $shapeImageSvg = pods_field('pa_shape', $_shape['term_id'], 'shape_image_svg', true);
    $shapeImageSvg = $shapeImageSvg['guid'];

    $shapeImageActiveSvg = pods_field('pa_shape', $_shape['term_id'], 'shape_image_active_svg', false);
    $shapeImageActiveSvg = $shapeImageActiveSvg['guid'];

    $shapeImagePng = pods_field('pa_shape', $_shape['term_id'], 'shape_image_png', false);
    $shapeImagePng = $shapeImagePng['guid'];

    $shapeImageHoverSvg = pods_field('pa_shape', $_shape['term_id'], 'shape_image_hover_svg', false);
    $shapeImageHoverSvg = $shapeImageHoverSvg['guid'];

    $html .= '
            <div class="swiper-slide">
                  <div class="content-item-sharps-tiles">
                      <a href="?action=style-selected&shape='.$_shapeSlug.'&manu_id='.$manuId.'" class="do-select-shape" data-shape="' . $_shapeSlug . '" data-swipe-index="' . $idx . '">
                          <div class="row-top-sharps">
                              <img style="margin-left:-10px;width:130px" src="' . $shapeImagePng . '" alt="shapeImagePng" />
                              ' . __($_shape['name'], 'tile-tool') . '
                          </div>
                          <div class="row-bottom-sharps" style="font-size:22px">
                              ' . $summary . '
                          </div>
                      </a>
                  </div>
              </div>
    ';
  }

  $html .= '
          </div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
        <div class="swiper-pagination"></div>
      </div>';

  return $html;
}
