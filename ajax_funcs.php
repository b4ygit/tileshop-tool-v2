<?php

/** 
 * Tool ajax function s
 */
function getProductsAjax()
{ 
    $_shapeSlug = $_GET['shape'];
    $_categorySlugs = trim($_GET['categories']);
    $_categorySlugs = !empty($_categorySlugs) ? explode(',', $_categorySlugs) : [];
    $_stock = isset($_GET['stock']) ? $_GET['stock'] : 0;
    $_page = empty($_GET['page']) ? 1 : $_GET['page'];

    $products = getProducts($_shapeSlug, $_categorySlugs, $_stock, $_page);

    response($products);
}

function getLayerMinMaxData()
{
    if ($_GET['action'] !== 'getLayerMinMaxData' && empty($_GET['layerId'])) {
        $return = ['success' => 0, 'message' => 'Error while getting related gallery.'];
    } else {
        $layerId = $_GET['layerId'];
        $min_limit_moving = pods_field('layer', $layerId, 'min_limit_moving', true);
        $max_limit_moving = pods_field('layer', $layerId, 'max_limit_moving', true);
        $min_limit_rotate = pods_field('layer', $layerId, 'min_limit_rotate', true);
        $max_limit_rotate = pods_field('layer', $layerId, 'max_limit_rotate', true);
        $min_limit_scaling = pods_field('layer', $layerId, 'min_limit_scaling', true);
        $max_limit_scaling = pods_field('layer', $layerId, 'max_limit_scaling', true);
        $min_limit_skewing = pods_field('layer', $layerId, 'min_limit_skewing', true);
        $max_limit_skewing = pods_field('layer', $layerId, 'max_limit_skewing', true);
        $return = ['success' => 1, 'message' => [
            'min_limit_moving' => $min_limit_moving,
            'max_limit_moving' => $max_limit_moving,
            'min_limit_rotate' => $min_limit_rotate,
            'max_limit_rotate' => $max_limit_rotate,
            'min_limit_scaling' => $min_limit_scaling,
            'max_limit_scaling' => $max_limit_scaling,
            'min_limit_skewing' => $min_limit_skewing,
            'max_limit_skewing' => $max_limit_skewing
        ]];
    }

    response($return);
}

function getImageOnTopAndTerazzoByManuAndShapeAndSize()
{
    if (
        $_GET['action'] !== 'getImageOnTopAndTerazzoByManuAndShapeAndSize'
        || empty($_GET['manu']) || empty($_GET['shape']) || empty($_GET['size'])
    ) {
        response(['success' => 0, 'message' => __('Error while getting image on top by manufacturer - shape - size.', 'tile-tool')]);
    }

    global $wpdb;
    $result = $wpdb->get_row($wpdb->prepare(
        "SELECT * FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d AND shape_slug = %s AND size_slug = %s LIMIT 1",
        $_GET['manu'],
        $_GET['shape'],
        $_GET['size']
    ));

    $imageOnTopShape = $result->image_url_of_on_top_shape ?? '';
    $imageOnTopTileShadowShape = $result->image_url_of_tiles_shadow_shape ?? '';
    $imageOnTopMainPreview = $result->image_url_of_main_preview_shape ?? '';

    $imageOnTopAddMore = json_decode($result->image_urls_of_add_more_shape, true);
    $imageOnTopAddMoreShape = $imageOnTopAddMore['main'];
    $imageOnTopAddMoreHoverShape = $imageOnTopAddMore['hover'];

    $imageOnTopBackground = json_decode($result->image_urls_of_background_shape, true);
    $imageOnTopBackgroundShape = $imageOnTopBackground['main'];
    $imageOnTopBackgroundHoverShape = $imageOnTopBackground['hover'];

    $imageOnTopSmallPreview = json_decode($result->image_urls_of_small_preview_shape, true);
    $imageOnTopSmallPreviewShape = $imageOnTopSmallPreview['main'];
    $imageOnTopSmallPreviewHoverShape = $imageOnTopSmallPreview['hover'];

    response([
        'success' => 1,
        'message' => [
            'imageOnTopUrl' => $imageOnTopShape,
            'imageOnTopAddMoreUrl' => $imageOnTopAddMoreShape,
            'imageOnTopAddMoreHoverUrl' => $imageOnTopAddMoreHoverShape,
            'imageOnTopBackgroundUrl' => $imageOnTopBackgroundShape,
            'imageOnTopBackgroundHoverUrl' => $imageOnTopBackgroundHoverShape,
            'imageOnTopSmallPreviewUrl' => $imageOnTopSmallPreviewShape,
            'imageOnTopSmallPreviewHoverUrl' => $imageOnTopSmallPreviewHoverShape,
            'imageOnTopTileShadowUrl' => $imageOnTopTileShadowShape,
            'imageOnTopMainPreviewUrl' => $imageOnTopMainPreview,
            'terazzoUrl' => $result->terazo_link ?? '',
        ]
    ]);
}

function getShapeSizeByManufacturer()
{
    if ($_GET['action'] !== 'getShapeSizeByManufacturer' || empty($_GET['manuID'])) {
        response(['success' => 0, 'message' => __('Error while getting shape list by manufacturer.', 'tile-tool')]);
    }

    $return = [];

    $podsField_ManuShapes = pods_field('manufacturer', $_GET['manuID'], 'shape', false);
    if (empty($podsField_ManuShapes)) {
        response(['success' => 0, 'message' => __('Error while getting shape and size list by manufacturer.', 'tile-tool')]);
    } else {
        $shape = $shapeSize = [];
        // get shape by manufacturer only
        foreach ($podsField_ManuShapes as $i => $manuShape) {
            $shape[$manuShape['slug']] = $manuShape['name'];
            // get full shape and size by manufacturer only
            $podsField_ManuFullSizes = pods_field('manufacturer', $_GET['manuID'], 'supported_' . $manuShape['slug'] . '_sizes', false);
            foreach ($podsField_ManuFullSizes as $i => $_size) {
                $shapeSize[$manuShape['slug']][$_size['slug']] = $_size['name'];
            }
        }
        // get size by manufacturer only
        $podsField_ManuSizes = pods_field('manufacturer', $_GET['manuID'], 'supported_' . $_GET['shapeSlug'] . '_sizes', false);
        foreach ($podsField_ManuSizes as $i => $manuSize) {
            $size[$manuSize['slug']] = $manuSize['name'];
        }

        $return['shape'] = $shape;
        $return['size'] = $size;
        $return['shapeSize'] = $shapeSize;
    }

    response(['success' => 1, 'message' => $return]);
}

function upsertProduct()
{
    $return = ['success' => 0, 'message' => __('Error when upsert product.', 'tile-tool')];
    $type = !empty($_GET['type']) ? $_GET['type'] : null;
    if (!empty($_POST)) {
        $data = !empty($_POST['layerCanvas']) ? $_POST['layerCanvas'] : [];
        $productID = !empty($_POST['productID']) ? $_POST['productID'] : null;
        $categories = !empty($_POST['categories']) ? $_POST['categories'] : null;
        $shapeAndSize = !empty($_POST['shapeAndSize']) ? $_POST['shapeAndSize'] : null;
        $productTitle = !empty($_POST['productTitle']) ? __(wp_filter_nohtml_kses($_POST['productTitle'])) : __('Edit product from product ID#') . $productID;
        $featuredImage = !empty($_POST['featuredImage']) ? $_POST['featuredImage'] : null;
        $priceObject = !empty($_POST['priceObject']) ? $_POST['priceObject'] : null;
        $minOrder = !empty($_POST['minOrder']) ? $_POST['minOrder'] : null;
        $thicknesses = !empty($_POST['thicknesses']) ? $_POST['thicknesses'] : null;
        $delivery = !empty($_POST['delivery']) ? wp_filter_nohtml_kses($_POST['delivery']) : null;
        $packaging = !empty($_POST['packaging']) ? wp_filter_nohtml_kses($_POST['packaging']) : null;
        $customDesign = !empty($_POST['customDesign']) ? wp_filter_nohtml_kses($_POST['customDesign']) : null;
        $weight = !empty($_POST['weight']) ? $_POST['weight'] : null;
        $terazzo = !empty($_POST['terazzo']) ? $_POST['terazzo'] : 0;
        $subtext1 = !empty($_POST['subText1']) ? wp_filter_nohtml_kses($_POST['subText1']) : null;
        $subtext2 = !empty($_POST['subText2']) ? wp_filter_nohtml_kses($_POST['subText2']) : null;
        $subtext3 = !empty($_POST['subText3']) ? wp_filter_nohtml_kses($_POST['subText3']) : null;
        $productDesignType = !empty($_POST['productDesignType']) ? $_POST['productDesignType'] : null;
        $productManufacturer = !empty($_POST['productManufacturer']) ? $_POST['productManufacturer'] : null;

        // UPSERT PRODUCT
        $result = _upsertProduct(
            $type,
            $productID,
            $data,
            $productTitle,
            $categories,
            $shapeAndSize,
            $featuredImage,
            $priceObject,
            $minOrder,
            $thicknesses,
            $delivery,
            $packaging,
            $customDesign,
            $weight,
            $terazzo,
            $subtext1,
            $subtext2,
            $subtext3,
            $productDesignType,
            $productManufacturer
        );

        if ($result) {
            $return = [
                'success' => 1,
                'message' => get_site_url(null, 'wp-admin') . "/edit.php?post_type=product"
            ];
        }
    }
    response($return);
}

function layer_title_filter($where, &$wp_query)
{
    global $wpdb;
    $search_term = $wp_query->get('search_prod_title');
    if ($search_term && !empty($search_term)) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql(like_escape($search_term)) . '%\'';
    }
    return $where;
}

function loadMoreLayer()
{
    if (empty($_POST)) {
        response(['success' => 0, 'message' => __('Error when load more layer.', 'tile-tool')]);
    }
    $page = ($_POST['page'] > 1) ? $_POST['page'] : 0;
    $tags = !empty($_POST['tags']) ? $_POST['tags'] : [];

    $posts_per_page = 40;
    $args = [
        'post_type' => 'layer',
        'post_status' => 'publish',
        'posts_per_page' => $posts_per_page,
        'paged' => $page,
        'orderby'     => 'date',
        'order'       => 'ASC'
    ];

    $exclusiveLayerCat = get_term_by('slug', 'exclusive', 'layer_cat');
    ///var_dump($exclusiveLayerCat);die();
    if (!empty($exclusiveLayerCat) && !current_user_can('edit_posts')) {
        $args['tax_query']['relation'] = 'AND';
        $args['tax_query'][] = [
            'taxonomy' => 'layer_cat',
            'field'    => 'term_id',
            'terms'    => [$exclusiveLayerCat->term_id],
            'operator' => 'NOT IN',
        ];
    }

    if (!empty($tags)) {
        $args['tax_query']['relation'] = 'AND';
        $args['tax_query'][] = [
            'taxonomy' => 'layer_tag',
            'field'    => 'term_id',
            'terms'    => $tags,
            'operator' => 'IN',
            'orderby'     => 'name',
            'order'       => 'DESC'
        ];
    }

    wp_reset_query();
    $wp_query = new WP_Query($args);

    $layers = [];
    $end = ($page > 0 && $wp_query->max_num_pages < $page) ? true : false;
    if ($wp_query->have_posts()) {
        $allLayers = $wp_query->get_posts();
        $all_post_found = $wp_query->found_posts;
        if ($all_post_found > 0 && $all_post_found < $posts_per_page) {
            $end = true;
        }
        foreach ($allLayers as $layer) {
            $layerTag = get_the_terms($layer, "layer_tag");
            $layerCat = get_the_terms($layer, "layer_cat");
           // if(empty($layerTag) || empty($layerCat)) {
            //    continue;
           // }
            $layers[$layer->ID] = [
                'src' => get_the_post_thumbnail_url($layer->ID, 'full'),
                'name' => $layer->post_title
            ];
        }
    }

    response([
        'success' => 1,
        'message' => $layers,
        'end' => $end,
        'total' => $wp_query->found_posts
    ]);
}

function getRelatedDataByFilters()
{
    $return = ['success' => 0, 'message' => __('Error when get price object by filter.', 'tile-tool')];

    if (empty($_GET)) {
        response($return);
    }

    [
        'sizeSlug' => $sizeSlug,
        'shapeSlug' => $shapeSlug,
        'manufacturerId' => $manufacturerId,
        'productId' => $productId,
    ] = $_GET;

    $productDetail = getProductDetailById($productId);

    global $wpdb;
    $manuRelatedData = $wpdb->get_row($wpdb->prepare(
        "SELECT * FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d AND shape_slug = %s AND size_slug = %s LIMIT 1",
        $manufacturerId,
        $shapeSlug,
        $sizeSlug
    ));

    if (empty($manuRelatedData)) {
        response($return);
    }

    $imageOnTopShape = $manuRelatedData->image_url_of_on_top_shape ?? '';
    $imageOnTopTileShadowShape = $manuRelatedData->image_url_of_tiles_shadow_shape ?? '';
    $imageOnTopMainPreview = $manuRelatedData->image_url_of_main_preview_shape ?? '';

    $imageOnTopAddMore = json_decode($manuRelatedData->image_urls_of_add_more_shape, true);
    $imageOnTopAddMoreShape = $imageOnTopAddMore['main'];
    $imageOnTopAddMoreHoverShape = $imageOnTopAddMore['hover'];

    $imageOnTopBackground = json_decode($manuRelatedData->image_urls_of_background_shape, true);
    $imageOnTopBackgroundShape = $imageOnTopBackground['main'];
    $imageOnTopBackgroundHoverShape = $imageOnTopBackground['hover'];

    $imageOnTopSmallPreview = json_decode($manuRelatedData->image_urls_of_small_preview_shape, true);
    $imageOnTopSmallPreviewShape = $imageOnTopSmallPreview['main'];
    $imageOnTopSmallPreviewHoverShape = $imageOnTopSmallPreview['hover'];

    $manuRelatedData = (array) $manuRelatedData;
    if ($productDetail['designType'] == 'customized') {
        $base_price = $manuRelatedData['base_price'];
        $priceAreaPair = [];
        foreach ($productDetail['priceObject']['priceAreaPair'] as $area => $price) {
            $priceAreaPair[$area] = $base_price - ($base_price * $price / 100);
        }
        $productDetail['priceObject']['priceAreaPair'] = $priceAreaPair;
    }

    response([
        'success' => 1,
        'message' => [
            'priceObject' => $productDetail['priceObject'],
            'colorUnitPrice' => $manuRelatedData['color_unit_price'],
            'basePrice' => $manuRelatedData['base_price'],
            'bpp' => $manuRelatedData['bpp'],
            'ppa' => $manuRelatedData['ppa'],
            'ppb' => $manuRelatedData['ppb'],
            'ppp' => $manuRelatedData['ppp'],
            'wpt' => $manuRelatedData['wpt'],
            'wpb' => $manuRelatedData['wpb'],
            'visualizerSizeRatio' => $manuRelatedData['option_1'],
            'gridLink' => $manuRelatedData['option_2'],
            'fragmentId' => $manuRelatedData['template_configuration_data'],
            'terazzoUrl' => $manuRelatedData['terazo_link'],
            'imageOnTopUrl' => $imageOnTopShape,
            'imageOnTopAddMoreUrl' => $imageOnTopAddMoreShape,
            'imageOnTopAddMoreHoverUrl' => $imageOnTopAddMoreHoverShape,
            'imageOnTopBackgroundUrl' => $imageOnTopBackgroundShape,
            'imageOnTopBackgroundHoverUrl' => $imageOnTopBackgroundHoverShape,
            'imageOnTopSmallPreviewUrl' => $imageOnTopSmallPreviewShape,
            'imageOnTopSmallPreviewHoverUrl' => $imageOnTopSmallPreviewHoverShape,
            'imageOnTopTileShadowUrl' => $imageOnTopTileShadowShape,
            'imageOnTopMainPreviewUrl' => $imageOnTopMainPreview,
        ]
    ]);
}

function saveDesign()
{
    $return = ['success' => 0, 'message' => __('Please login first for using this action.', 'tile-tool')];
    if (!is_user_logged_in()) {
        response($return);
    }

    if (empty($_POST['layerCanvas']) || empty($_POST['productDetail'])) {
        $return = ['success' => 0, 'message' => 'Invalid request or Errror.'];
    } else {
        $productDetail = $_POST['productDetail'];
        $data = $_POST['layerCanvas'];

        global $wpdb;
        $wpdb->insert($wpdb->prefix . 'tool_share_links', [
            'id' => null,
            'customizedData' => json_encode($data),
            'productID' => $productDetail['productID'],
            'shapeSlug' => $productDetail['shapeSlug'],
            'catID' => $productDetail['catID'] ?? 0,
            'sizeID' => $productDetail['sizeID'],
            'shortLink' => $_SERVER['HTTP_REFERER'],
            'thickness' => $productDetail['thickness'] ?? null,
            'terazo' => $productDetail['terazo'] ?? null,
            'image_data' => $productDetail['image'],
            'user_id' => wp_get_current_user()->ID,
            'created' => current_time('mysql'),
            'updated' => current_time('mysql')
        ]);

        if (!empty($wpdb->last_error)) {
            $message = (string) $wpdb->last_error;
        } else {
            $message = __('Desgin tile was saved.', 'tile-tool');
        }

        $return = ['success' => 1, 'message' => $message];
    }

    response($return);
}

function shareDesign()
{
    $return = ['success' => 0, 'message' => __('Invalid request or Errror.', 'tile-tool')];

    if (empty($_POST)) {
        response($return);
    }

    // if (!is_user_logged_in()) {
    //     $return = ['success' => 0, 'message' => __('Please login first for using this action.', 'tile-tool')];
    // } else {
    $data = (!empty($_POST['layerCanvas']) && $_POST['fromProductDetail']) ? $_POST['layerCanvas'] : [];
    $message = '';
    $productDetail = $_POST['productDetail'];

    global $wpdb;
    $wpdb->insert(
        $wpdb->prefix . 'tool_share_links',
        [
            'id' => null,
            'customizedData' => json_encode($data),
            'productID' => $productDetail['productID'], 
            'shapeSlug' => $productDetail['shapeSlug'],
            'catID' => $productDetail['catID'] ?? 0,
            'sizeID' => $productDetail['sizeID'],
            'shortLink' => $_SERVER['HTTP_REFERER'],
            'thickness' => $productDetail['thickness'] ?? 'null',
            'terazo' => $productDetail['terazo'] ?? 'null',
            'image_data' => $productDetail['image'],
            'created' => current_time('mysql'),
            'updated' => current_time('mysql')
        ]
    );

    if (!empty($wpdb->last_error)) {
        $message = (string) $wpdb->last_error;
    } else {
        $shareID = $wpdb->insert_id;

        $message = [
            "share_link" => $_POST['link'] . '?action=shareLink&id=' . $shareID,
            "share_id" => $shareID,
        ];
    }

    $return = ['success' => 1, 'message' => $message];

    response($return);
}

function typo3Action()
{
    $success = 0;
    $return = ['success' => $success, 'message' => __('Invalid request or Errror.', 'tile-tool')];

    if (empty($_POST)) {
        response($return);
    }

    $data = $_POST["typo3Data"];
    $message = $uid = '';

    $request = new WP_Http();
    global $wpdb;
    $typo3URL = pods_field('typo3_configuration', false, 'domain_url');

    $t3ReturnURL = pods_field('typo3_configuration', false, 't3_return');
    $t3TaxID = pods_field('typo3_configuration', false, 't3_tax');
    $t3PID = pods_field('typo3_configuration', false, 't3_pid');
    //$t3Product = pods_field('typo3_configuration', false, 't3_product');

    $t3Shapes = pods_field('typo3_configuration', false, 't3_shapes');
    $t3Shapes = json_decode($t3Shapes);
    $t3Shape = null;
    foreach ($t3Shapes as $shape => $t3ShapeID) {
        if ($shape == $data['shapeSlug']) {
            $t3Shape = $t3ShapeID;
            break;
        }
    }
 	if($t3Shape >=13) $t3Product=2;
 	else $t3Product=1;
    $isValidTypo3Fields = true;
    $errMess = "";
    foreach ([
        $t3ReturnURL,
        $t3TaxID,
        $t3PID,
        $t3Product,
        $t3Shape,
    ] as $field) {
        if ($field == null) {
            $isValidTypo3Fields = false;
            $errMess = "Missing field:" . $field;
            break;
        }
    }

    if (!$isValidTypo3Fields) {
        $return = ['success' => $success, 'message' => "Missing TYPO3 Data. " . $errMess];
        response($return);
    }

    $data['sku'] = (int)$data['sku'];
    $data['productType'] = 3;
    $data['colors'] = (int)$data['colors'];
    $data['taxClassId'] = (int)$t3TaxID;
    $data['pid'] = (int)$t3PID;
    $product_uid = (int)$data['product'];
    if ($product_uid > 0) $data['product'] = $product_uid;
    else $data['product'] = (int)$t3Product;
    $shape_uid = (int)$data['shape'];
    if ($shape_uid > 0) $data['shape'] = $shape_uid;
    else $data['shape'] = (int)$t3Shape;

    switch ($_POST["typo3Type"]) {
        case 'POST':
            unset($data['uid']);
            unset($data["shapeSlug"]);
            $response = $request->post($typo3URL, ['body' => $data]);
            if (is_wp_error($response)) {
                $message = "Error while calling POST to TYPO3";
                break;
            }
            $respBody = json_decode($response['body'], true);
            if (isset($respBody['error'])) {
                $message = $respBody['error'];
            } else {
                $success = 1;
                $uid = $respBody['uid'];
                $message = str_replace("TID", $uid, $t3ReturnURL);
            }

            break;
        case 'PATCH':
            $body = [
                'colors' => $data['colors'],
                'sku' => $data['sku'],
                'shape' => $data['shape'],
                'product' => $data['product'],
                'appImage' => $data['appImage']
            ];

            $response = $request->request($typo3URL . "/" . $data['uid'], ['method' => 'PATCH', 'body' => $body]);
            if (is_wp_error($response)) {
                $message = "Error while calling PATCH to TYPO3";
                break;
            }

            $respBody = json_decode($response['body'], true);
            if (isset($respBody['error'])) {
                $message = $respBody['error'];
            } else {
                $success = 1;
                $uid = $respBody['uid'];
                $message = str_replace("TID", $uid, $t3ReturnURL);
            }

            break;
    }

    if ($success) {
        $wpdb->update(
            $wpdb->prefix . 'tool_share_links',
            [
                'uid' => $uid,
                'updated' => current_time('mysql')
            ],
            [
                'id' => $data['sku']
            ]
        );

        if (!empty($wpdb->last_error)) {
            $message = "Error: " + ((string) $wpdb->last_error);
            $success = 0;
        }

        $message = urldecode($message);
    }

    $return = ['success' => $success, 'message' => $message];

    response($return);
}

function getGalleries()
{
    $return = ['success' => 0, 'message' => 'Error while getting related gallery.'];
    if (empty($_GET['templateConfigurationData'])) {
        response($return);
    }

    $g_templateConfigurationData = $_GET['templateConfigurationData'];
    $thumbnails = pods_field('gallery', $g_templateConfigurationData, 'upload_html_thumbnail', false);
    $return_thumbnails = [];
    foreach ($thumbnails as $i => $thumbnail) {
        $return_thumbnails[$thumbnail['post_title']] = $thumbnail['guid'];
    }

    response(['success' => 1, 'message' => $return_thumbnails]);
}

function getHtmlFragment()
{
    $return = ['success' => 0, 'message' => 'Error while getting related html fragment.'];
    if (empty($_POST['imgSrc']) || empty($_POST['fragmentPodItemTitle']) || empty($_POST['templateConfigurationData'])) {
        response($return);
    }

    $g_fragmentPodItemTitle = $_POST['fragmentPodItemTitle'];
    $g_imgSrc = $_POST['imgSrc'];
    $g_templateConfigurationData = $_POST['templateConfigurationData'];
    $fragment_files = pods_field('gallery', $g_templateConfigurationData, 'upload_html_fragment_files', false);

    try {
        $_SESSION['visualizerImgSrc'] = $g_imgSrc;
    } catch (\Exception $e) {
        $_SESSION['visualizerImgSrcErr'] = $e->getMessage();
    }

    $message = [
        'imgSrc' => $g_imgSrc,
        'htmlFragment' => '',
        'visualizeImageUrl' => get_site_url() . '/getVisualizerImage.php'
    ];
    foreach ($fragment_files as $fragment_file) {
        if ($fragment_file['post_title'] == $g_fragmentPodItemTitle) {
            $path = parse_url($fragment_file['guid']);
            $path = $path['path'];
            $message['htmlFragment'] = get_site_url() . $path;
            // $message['htmlFragment'] = $fragment_file['guid'];
            break;
        }
    }

    response(['success' => 1, 'message' => $message]);
}

function addToCart()
{
    if (empty($_POST)) {
        response(['success' => 0, 'message' => __('Invalid request. Please recheck again or contact admin.', 'tile-tool')]);
    }

    $_SESSION['customCheckout'] = !empty($_SESSION['customCheckout']) ? $_SESSION['customCheckout'] : [];
    $sID = session_id();
    $productID = $_POST['productDetail']['productID'];
    $productHash = md5($productID . $sID . time());
    $editCartItemLink = home_url('/tile-tool') . '?action=editCart&id=' . $productHash;

    $_SESSION['customCheckout'][$productHash] = $_POST;

    response([
        'success' => 1,
        'message' => __('Your cart was added new item successful.', 'tile-tool'),
        'cartItem' => count($_SESSION['customCheckout']),
        'redirectLink' => $editCartItemLink
    ]);
}

function editCartItem()
{
    if (
        empty($_POST) || empty($_POST['cartItemId'])
        || !isset($_SESSION['customCheckout']) || empty($_SESSION['customCheckout'])
        || !isset($_SESSION['customCheckout'][$_POST['cartItemId']]) || empty($_SESSION['customCheckout'][$_POST['cartItemId']])
    ) {
        response(['success' => 0, 'message' => '']);
    }

    $hashId = $_POST['cartItemId'];
    unset($_POST['cartItemId']);
    $_SESSION['customCheckout'][$hashId] = $_POST;

    response([
        'success' => 1,
        'message' => __('Your cart was edited successful.', 'tile-tool'),
        'redirectLink' => home_url('/tile-tool') . '?action=cart'
    ]);
}

function removeCartItem()
{
    $return = ['success' => 0, 'message' => __('Remove selected item out of cart had error.', 'tile-tool')];
    if (empty($_POST) || empty($_POST['id'])) {
        response($return);
    }
    $itemOrderId = $_POST['id'];
    try {
        unset($_SESSION['customCheckout'][$itemOrderId]);
        $return = [
            'success' => 1,
            'message' => 'Remove selected item out of cart successful.'
        ];
    } catch (Exception $e) {
        $return['message'] = $e->getMessage();
    }

    response($return);
}

function getShippingZonesByCountry()
{
    if (!empty($_GET)) {
        response(['success' => 0, 'message' => __('Invalid request. Please recheck again or contact admin.', 'tile-tool')]);
    }

    $g_action = (!empty($_GET['action']) && $_GET['action'] === 'getShippingZonesByCountry') ? $_GET['action'] : '';
    $g_formatted_zone_location = !empty($_GET['formatted_zone_location']) ? $_GET['formatted_zone_location'] : '';
    if (empty($g_action) || empty($g_formatted_zone_location)) {
        response(['success' => 0, 'message' => '']);
    }

    $shippingZones = listAllShippingZones();
    $result = [];
    foreach ($shippingZones as $i => $shippingZone) {
        if ($shippingZone['formatted_zone_location'] == $g_formatted_zone_location) {
            $result[$shippingZone['id']] = $shippingZone['zone_name'];
        }
    }

    response(['success' => 1, 'message' => $result]);
}

function calculateTotalBill()
{
    $return = ['success' => 0, 'message' => __('Invalid request. Please recheck again or contact admin.', 'tile-tool')];

    if (
        empty($_POST)
        || !isset($_POST['shippingZone']) || empty($_POST['shippingZone'])
        || !isset($_POST['shippingCountryName']) || empty($_POST['shippingCountryName'])
        || !isset($_SESSION['customCheckout']) || empty($_SESSION['customCheckout'])
    ) {
        response($return);
    }

    $message = calculateCart([
        'country' => $_POST['shippingCountryName'],
        'zone' => $_POST['shippingZone'],
    ]);

    if (empty($message)) {
        response($return);
    }

    response([
        'success' => 1,
        'message' => $message
    ]);
}

function submitShipping()
{
    $return = ['success' => 0, 'message' => __('Invalid request. Please recheck again or contact admin.', 'tile-tool')];

    if (
        empty($_POST)
        || !isset($_POST['options']) || empty($_POST['options'])
        || !isset($_SESSION['customCheckout']) || empty($_SESSION['customCheckout'])
    ) {
        response($return);
    }

    $options = $_POST['options'];
    $itemKeys = array_keys($options);
    $itemKeys = array_flip($itemKeys);
    foreach ($_SESSION['customCheckout'] as $hashId => $item) {
        if (!isset($itemKeys[$hashId])) {
            continue;
        }

        $productDetail = $item['productDetail'];
        $thicknesses = json_decode($productDetail['thickness'], true);
        if (
            !isset($options[$hashId]['thickness'])
            || empty($options[$hashId]['thickness'])
            || !in_array($options[$hashId]['thickness'], $thicknesses)
        ) {
            $productDetail['selectedThickness'] = $thicknesses[0];
        } else {
            $productDetail['selectedThickness'] = $options[$hashId]['thickness'];
        }
        $productDetail['isPreSale'] = $options[$hashId]['isPreSale'] ?? false;

        $_SESSION['customCheckout'][$hashId]['productDetail'] = $productDetail;
    }

    response([
        'success' => 1,
        'message' => home_url('/tile-tool') . '?action=shipping',
    ]);
}

function submitPayment()
{
    $return = ['success' => 0, 'message' => __('Invalid request. Please recheck again or contact admin.', 'tile-tool')];

    if (
        empty($_POST)
        || !isset($_POST) || empty($_POST['shipping'])
        || !isset($_POST['shipping']['zone']) || empty($_POST['shipping']['zone'])
        || !isset($_POST['shipping']['country']) || empty($_POST['shipping']['country'])
        || !isset($_SESSION['customCheckout']) || empty($_SESSION['customCheckout'])
    ) {
        response($return);
    }

    $billingDetail = $shippingDetail = array_merge($_POST['shipping'], [
        'country' => $_POST['shipping']['country'],
        'zone' => $_POST['shipping']['zone'],
    ]);

    if (isset($_POST['billing']) && !empty($_POST['billing'])) {
        $billingDetail = $_POST['billing'];
    }

    if ($userId = get_current_user_id()) {
        global $wpdb;
        $baseQuery = "SELECT id FROM " . ($wpdb->prefix . 'tool_user_addresses') . " WHERE user_id=%d AND name=%s AND company=%s ";

        $name = $shippingDetail['first_name'] . $shippingDetail['last_name'];
        if ($shippingDetail['isNew']) {
            $shippingAddress = $wpdb->get_results($wpdb->prepare(
                $baseQuery . " AND type=1 LIMIT 1;",
                $userId,
                $name,
                $shippingDetail['company']
            ));
            if (!empty($shippingAddress)) {
                response(['success' => 0, 'message' => __('Already existed shipping address.', 'tile-tool')]);
            }
        }

        $name = $billingDetail['first_name'] . $billingDetail['last_name'];
        if ($billingDetail['isNew']) {
            $billingAddress = $wpdb->get_results($wpdb->prepare(
                $baseQuery . " AND type=2 LIMIT 1;",
                $userId,
                $name,
                $_POST['billing']['company']
            ));
            if (!empty($billingAddress)) {
                response(['success' => 0, 'message' => __('Already existed billing address.', 'tile-tool')]);
            }
        }
    }

    $_SESSION['paymentDetail'] = calculateCart([
        'country' => $_POST['shipping']['country'],
        'zone' => $_POST['shipping']['zone'],
    ]);

    $woocommerce = WooAPI::getClient();
    if ($woocommerce instanceof Exception) {
        response([
            'success' => 0,
            'message' => $woocommerce->getMessage()
        ]);
    }

    $lineItems = [];
    foreach ($_SESSION['customCheckout'] as $item) {
        $productDetail = $item['productDetail'];

        $lineItems[] = [
            'product_id' => $productDetail['productID'],
            'quantity' => $productDetail['productType'] == 'fixed' ? intval($productDetail['totalPieces']) : 1,
            'total' => $item['productDetailBill']['total']
        ];
    }

    $data = [
        'order' => [
            'billing_address' => [
                'first_name' => $billingDetail['first_name'],
                'last_name' => $billingDetail['last_name'],
                'address_1' => $billingDetail['company'],
                'city' => $billingDetail['city'],
                'postcode' => $billingDetail['zip'],
                'email' => $billingDetail['email'],
                'phone' => $billingDetail['phone']
            ],
            'shipping_address' => [
                'first_name' => $shippingDetail['first_name'],
                'last_name' => $shippingDetail['last_name'],
                'address_1' => $shippingDetail['company'],
                'city' => $shippingDetail['city'],
                'postcode' => $shippingDetail['zip'],
                'email' => $shippingDetail['email'],
                'phone' => $shippingDetail['phone']
            ],
            'line_items' => $lineItems
        ]
    ];

    if ($userId = get_current_user_id()) {
        $data['order'] = array_merge($data['order'], [
            'customer_id' => $userId
        ]);
    }

    $order = [];
    try {
        $order = $woocommerce->post('orders', $data);
        if (empty($order)) {
            throw new \Exception('Cannot create order.', 500);
        }
        // Set shareLink and shippingInformation for order detail
        $shareLink = [];
        global $wpdb;
        foreach ($_SESSION['customCheckout'] as $item) {
            $productDetail = $item['productDetail'];
            $customizedData = json_encode($item['layerCanvas']);

            $wpdb->insert(
                $wpdb->prefix . 'tool_share_links',
                [
                    'id' => null,
                    'customizedData' => $customizedData,
                    'productID' => $productDetail['productID'],
                    'shapeSlug' => $productDetail['shapeSlug'],
                    'catID' => null,
                    'sizeID' => $productDetail['sizeId'],
                    'thickness' => $productDetail['selectedThickness'],
                    'shortLink' => '',
                    'terazo' => $productDetail['terazo'],
                    'created' => current_time('mysql'),
                    'updated' => current_time('mysql')
                ]
            );

            if ($wpdb->last_error != '' || !$wpdb->insert_id) {
                throw new \Exception($wpdb->last_error, 500);
            }
            $shareLink[] = get_permalink(get_page_by_path('tile-tool')) . '?action=shareLink&id=' . $wpdb->insert_id;
        }

        $shop_order = pods('shop_order');
        $shop_order->save([
            'share_link' => $shareLink,
            'shipping_information' => _get_shipping_information_view($data['order']['shipping_address']),
            'order_information' => _get_order_information_view($_SESSION['customCheckout'])
        ], null, $order['order']['id']);
        if (!empty($shippingDetail['note'])) {
            $woocommerce->post('orders/' . $order['order']['id'] . '/notes', [
                'order_note' => [
                    'note' => $shippingDetail['note'],
                    'customer_note' => true
                ]
            ]);
        }
        $_SESSION['orderID'] = $order['order']['id'];
    } catch (\Exception $e) {
        response([
            'success' => 0,
            'message' => $e->getMessage()
        ]);
    }

    response([
        'success' => 1,
        'message' => home_url('/tile-tool') . '?action=payment'
    ]);
}

function _get_order_information_view($data)
{
    $html = '<div class="row">';
    $html .= '<ul>';

    $count = 0;
    foreach ($data as $item) {
        $productDetail = $item['productDetail'];

        $html .= '<li>';
        $html .= '<div class="col-sm-2">';
        $html .= '<label><strong>Product ' . ++$count . ': ' . $productDetail['productName'] . ': </strong></label> $' . $productDetail['pricing'];
        $html .= '</div>';

        $html .= '<div class="col-sm-2">';
        $html .= '<label>';
        $html .= '<strong>Selected thickness:</strong> ' . $productDetail['selectedThickness'];
        $html .= '</label>';
        $html .= '</div>';

        $html .= '<div class="col-sm-2">';
        $html .= '<label>';
        $html .= '<strong>Number of tiles:</strong> ' . $productDetail['totalPieces'];
        $html .= '</label>';
        $html .= '</div>';

        $html .= '<div class="col-sm-2">';
        $html .= '<label>';
        $html .= '<strong>Total square:</strong> ' . $productDetail['area'] . ' m<sup>2</sup>';
        $html .= '</label>';
        $html .= '</div>';

        $html .= '<div class="col-sm-2">';
        $html .= '<label>';
        $html .= '<strong>Is Pre-Sale:</strong> ' . ($productDetail['isPreSale'] ? 'true' : 'false');
        $html .= '</label>';
        $html .= '</div>';

        $html .= '<div class="col-sm-2">';
        $html .= '<label>';
        $html .= '<strong>Product Type:</strong> ' . $productDetail['productType'];
        $html .= '</label>';
        $html .= '</div>';
        $html .=  '</li>';
    }

    $html .= '</ul>';

    $html .= '<hr>';
    $html .= '<div class="col-sm-2">';
    $html .= '<label>';
    $html .= '<strong>Shipping Detail Price:</strong> ' . $item['productDetailBill']['shippingFee'];
    $html .= '</label>';
    $html .= '</div>';

    $html .= '<div class="col-sm-2">';
    $html .= '<label>';
    $html .= '<strong>Total Bill:</strong> $' . $item['productDetailBill']['bill'];
    $html .= '</label>';
    $html .= '</div>';

    $html .= '<div class="col-sm-2">';
    $html .= '<label>';
    $html .= '<strong>Tax:</strong> ' . $item['productDetailBill']['tax'] . '% ~ $' . $item['productDetailBill']['taxFee'];
    $html .= '</label>';
    $html .= '</div>';

    $html .= '<div class="col-sm-2">';
    $html .= '<label>';
    $html .= '<strong>Order Total:</strong> $' . $item['productDetailBill']['total'];
    $html .= '</label>';
    $html .= '</div>';

    $html .= '</div>';

    return $html;
}

function _get_shipping_information_view($shippingInformation)
{
    $html = '<div class="row">';
    foreach ($shippingInformation as $name => $value) {
        $html .= '<div class="col-sm-2">';
        $html .= '<label><strong>' . $name . ':</strong></label> ' . $value;
        $html .= '</div>';
    }
    $html .= '</div>';

    return $html;
}

function submitOrder()
{
    $return = ['success' => 0, 'message' => __('Invalid request. Please recheck again or contact admin.', 'tile-tool')];

    if (
        empty($_POST)
        || !isset($_POST) || empty($_POST['paymentMethod'])
        || !isset($_SESSION['orderID']) || empty($_SESSION['orderID'])
    ) {
        response($return);
    }

    $woocommerce = WooAPI::getClient();
    if ($woocommerce instanceof Exception) {
        response([
            'success' => 0,
            'message' => $woocommerce->getMessage()
        ]);
    }

    $order = $woocommerce->get('orders/' . $_SESSION['orderID']);
    if (empty($order)) {
        response([
            'success' => 0,
            'message' => 'Empty order'
        ]);
    }

    // Store Order ID in session so it can be re-used after payment failure
    WC()->session->order_awaiting_payment = $order['order']['id'];

    // Process Payment
    $paymentMethod = $_POST['paymentMethod'];
    $available_gateways = WC()->payment_gateways->get_available_payment_gateways();
    $result = $available_gateways[$paymentMethod]->process_payment($order['order']['id']);

    // Redirect to success/confirmation/payment page
    if ($result['result'] == 'success') {
        // $result = apply_filters( 'woocommerce_payment_successful_result', $result, $order['order']['id'] );
        // $message = $result['redirect'];

        $message = home_url('/tile-tool') . '?action=thank-you&order-code=' . $order['order']['id'];
        unset($_SESSION['customCheckout']);
        unset($_SESSION['paymentDetail']);
        unset($_SESSION['shippingDetail']);
        unset($_SESSION['billingDetail']);
        unset($_SESSION['orderID']);
        // WC()->session->order_awaiting_payment = null;
    }

    response([
        'success' => 1,
        'message' => $message
    ]);
}

function deleteAddress()
{
    $return = ['success' => 0, 'message' => __('Cannot delete address. Please verify again or contact admin.', 'tile_tool')];

    if (
        empty($_POST)
        || !isset($_POST) || empty($_POST['id'])
    ) {
        response($return);
    }

    $id = $_POST['id'];
    $userId = get_current_user_id();
    if (!$userId) {
        $return['message'] = 'Unauthenticated.';
        response($return);
    }

    global $wpdb;
    $result = $wpdb->get_row($wpdb->prepare(
        "SELECT id FROM " . ($wpdb->prefix . 'tool_user_addresses') . " WHERE id=%d AND user_id=%d;",
        $id,
        $userId
    ));

    if (empty($result)) {
        $return['message'] = "Unauthorize or user's address is not existed.";
        response($return);
    }

    $result = $wpdb->delete($wpdb->prefix . 'tool_user_addresses', ['id' => $id]);

    response([
        'success' => 1,
        'message' => "Delete user's address is successful."
    ]);
}

function updateAddress()
{
    $return = ['success' => 0, 'message' => __('Cannot update address. Please verify again or contact admin.', 'tile_tool')];

    if (
        empty($_POST)
        || !isset($_POST) || empty($_POST['id'])
    ) {
        response($return);
    }

    $id = $_POST['id'];
    $userId = get_current_user_id();
    if (!$userId) {
        $return['message'] = 'Unauthenticated.';
        response($return);
    }

    global $wpdb;
    $result = $wpdb->get_row($wpdb->prepare(
        "SELECT id FROM " . ($wpdb->prefix . 'tool_user_addresses') . " WHERE id=%d AND user_id=%d;",
        $id,
        $userId
    ));

    if (empty($result)) {
        $return['message'] = __("Unauthorize or user's address is not existed.", 'tile-tool');
        response($return);
    }

    $message = __('Successfully update address.', 'tile-tool');
    $success = 1;
    try {
        $result = $wpdb->update(
            $wpdb->prefix . 'tool_user_addresses',
            [
                'first_name' => $_POST['first_name'] ?? null,
                'last_name' => $_POST['last_name'] ?? null,
                'name' => ($_POST['first_name'] . ' ' . $_POST['last_name']) ?? null,
                'phone' => $_POST['phone'] ?? null,
                'email' => $_POST['email'] ?? null,
                'company' => $_POST['company'] ?? null,
                'country' => $_POST['country'] ?? null,
                'zone' => (int) $_POST['zone'] ?? null,
                'zip' => $_POST['zip'] ?? null,
                'city' => $_POST['city'] ?? null,
                'type' => (int) $_POST['type'] ?? null,
                'user_id' => (int) $userId,
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => $id,
                'user_id' => $userId
            ],
            [
                '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%s', '%d', '%d', '%s', '%s'
            ],
            [
                '%d', '%d'
            ]
        );

        if (!$result) {
            throw new \Exception(__('Invalid request when update address, please check again or contact admin.', 'tile-tool'), 400);
        }
    } catch (\Exception $ex) {
        $message = $ex->getMessage();
        $success = 0;
    }

    response([
        'success' => $success,
        'message' => $message
    ]);
}

function addNewAddress()
{
    $return = ['success' => 0, 'message' => __('Cannot add new address. Please verify again or contact admin.', 'tile_tool')];

    if (empty($_POST) || !isset($_POST)) {
        response($return);
    }

    $userId = get_current_user_id();
    if (!$userId) {
        $return['message'] = 'Unauthenticated.';
        response($return);
    }

    $message = __('Successfully add new address.', 'tile-tool');
    $success = 1;
    try {
        $now = date('Y-m-d H:i:s');

        global $wpdb;
        $result = $wpdb->insert(
            $wpdb->prefix . 'tool_user_addresses',
            [
                'id' => null,
                'first_name' => $_POST['first_name'] ?? null,
                'last_name' => $_POST['last_name'] ?? null,
                'name' => ($_POST['first_name'] . ' ' . $_POST['last_name']) ?? null,
                'phone' => $_POST['phone'] ?? null,
                'email' => $_POST['email'] ?? null,
                'company' => $_POST['company'] ?? null,
                'country' => $_POST['country'] ?? null,
                'zone' => (int) $_POST['zone'] ?? null,
                'zip' => $_POST['zip'] ?? null,
                'city' => $_POST['city'] ?? null,
                'type' => (int) $_POST['type'] ?? null,
                'user_id' => (int) $userId,
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%s', '%d', '%d', '%s', '%s'
            ]
        );

        if (!$result) {
            throw new \Exception(__('Invalid request when add new address, please check again or contact admin.', 'tile-tool'), 400);
        }
    } catch (\Exception $ex) {
        $message = $ex->getMessage();
        $success = 0;
    }

    response([
        'success' => $success,
        'message' => $message
    ]);
}

function updateAccount()
{
    $return = ['success' => 0, 'message' => __('Cannot update account. Please verify again or contact admin.', 'tile_tool')];

    if (
        empty($_POST) || !isset($_POST)
        || !isset($_POST['id']) || empty($_POST['id'])
    ) {
        response($return);
    }

    $user = wp_get_current_user();
    if (!$user) {
        $return['message'] = 'Unauthenticated.';
        response($return);
    }

    if ($user->ID != $_POST['id']) {
        $return['message'] = 'Unauthorized.';
        response($return);
    }

    try {
        $woocommerce = WooAPI::getClient();
        if ($woocommerce instanceof Exception) {
            response([
                'success' => 0,
                'message' => $woocommerce->getMessage()
            ]);
        }

        $customer = $woocommerce->get('customers/' . $user->ID);

        $customer['customer']['first_name'] = strip_tags($_POST['firstName']);
        $customer['customer']['last_name'] = strip_tags($_POST['lastName']);
        $customer['customer']['email'] = $_POST['email'];

        $customer = $woocommerce->put('customers/' . $user->ID, $customer);


        if ($_POST['password'] && $_POST['password'] === $_POST['repassword']) {
            $user->data->user_pass = $_POST['password'];
            wp_update_user($user);
        }

        wp_logout();
        $return['message'] = __('Successfully update account.', 'tile-tool');
        $return['success'] = 1;
    } catch (\Exception $e) {
        $return['message'] = __('Error while updating account. Please contact admin for more detail.', 'tile-tool');
    }

    response($return);
}

function deleteAccount()
{
    $return = ['success' => 0, 'message' => __('Cannot delete account. Please verify again or contact admin.', 'tile_tool')];

    if (
        empty($_POST) || !isset($_POST)
        || !isset($_POST['customerId']) || empty($_POST['customerId'])
    ) {
        response($return);
    }

    $user = wp_get_current_user();
    if (!$user) {
        $return['message'] = 'Unauthenticated.';
        response($return);
    }

    if ($user->ID != $_POST['customerId']) {
        $return['message'] = 'Unauthorized.';
        response($return);
    }

    try {
        $woocommerce = WooAPI::getClient();
        if ($woocommerce instanceof Exception) {
            response([
                'success' => 0,
                'message' => $woocommerce->getMessage()
            ]);
        }

        $woocommerce->delete('customers/' . $user->ID, ['force' => true]);
        wp_logout();

        $return['message'] = __('Successfully delete account.', 'tile-tool');
        $return['success'] = 1;
    } catch (\Exception $e) {
        $return['message'] = __('Error while deleting account. Please contact admin for more detail.', 'tile-tool');
    }

    response($return);
}
function customAjaxLogin()
{
    if (!check_ajax_referer('ajax-login-nonce', 'security')) {
        return (['success' => 0, 'message' => __('Invalid security code', 'tile_tool')]);
    } elseif (is_user_logged_in()) {
        return (['success' => 0, 'message' => __('Already logged in.', 'tile_tool')]);
    }

    $return = ['success' => 0, 'message' => __('Cannot sign in. Please verify again or contact admin.', 'tile_tool')];
    if (
        empty($_POST) || !isset($_POST)
        || !isset($_POST['username']) || empty($_POST['username'])
        || !isset($_POST['password']) || empty($_POST['password'])
    ) {
        response($return);
    }

    $info = [
        'user_login' => trim($_POST['username']),
        'user_password' => trim($_POST['password']),
        'remember' => true
    ];

    $user_signon = wp_signon($info, false);
    if (is_wp_error($user_signon)) {
        $return['message'] = __('Wrong username or password.');
    } else {
        $return['message'] = __('Login successful, redirecting...', 'tile-tool');
        $return['success'] = 1;
    }

    response($return);
}
function customAjaxSignup()
{
    if (!check_ajax_referer('ajax-register-nonce', 'security')) {
        return (['success' => 0, 'message' => __('Invalid security code', 'tile_tool')]);
    } elseif (is_user_logged_in()) {
        return (['success' => 0, 'message' => __('Already logged in.', 'tile_tool')]);
    }

    $return = ['success' => 0, 'message' => __('Cannot sign up. Please verify again or contact admin.', 'tile_tool')];
    if (
        empty($_POST) || !isset($_POST)
        || !isset($_POST['email']) || empty($_POST['email'])
        || !isset($_POST['username']) || empty($_POST['username'])
        || !isset($_POST['password']) || empty($_POST['password'])
        || !isset($_POST['repassword']) || empty($_POST['repassword'])
    ) {
        response($return);
    }

    $email = trim($_POST['email']);
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $repassword = trim($_POST['repassword']);

    if ($repassword !== $password) {
        return (['success' => 0, 'message' => __('Password and repassword is not the same.', 'tile_tool')]);
    }

    $info = [
        'user_pass' => sanitize_text_field($password),
        'user_email' => sanitize_email($email),
        'role' => 'customer'
    ];
    $info['display_name'] = $info['user_login'] = sanitize_user($username);

    // Register the user
    $user_register = wp_insert_user($info);
    if (is_wp_error($user_register)) {
        $error  = $user_register->get_error_codes();

        if (in_array('empty_user_login', $error)) {
            $return['message'] = __($user_register->get_error_message('empty_user_login'));
        } elseif (in_array('existing_user_login', $error)) {
            $return['message'] = __('This username is already registered.');
        } elseif (in_array('existing_user_email', $error)) {
            $return['message'] = __('This email address is already registered.');
        }
    } else {
        $user_signon = wp_signon([
            'user_login' => $username,
            'user_password' => $password,
            'remember' => true
        ], false);
        if (is_wp_error($user_signon)) {
            $return['message'] = __('Cannot login.');
            response($return);
        }

        $return['message'] = __('Register new user successful, redirecting...', 'tile-tool');
        $return['success'] = 1;
    }
    response($return);
}
/**
 * Register functions with wp hook
 */
add_action('wp_ajax_nopriv_getProductsAjax', 'getProductsAjax');
add_action('wp_ajax_getProductsAjax', 'getProductsAjax');

add_action('wp_ajax_nopriv_getLayerMinMaxData', 'getLayerMinMaxData');
add_action('wp_ajax_getLayerMinMaxData', 'getLayerMinMaxData');

add_action('wp_ajax_nopriv_getShapeSizeByManufacturer', 'getShapeSizeByManufacturer');
add_action('wp_ajax_getShapeSizeByManufacturer', 'getShapeSizeByManufacturer');

add_action('wp_ajax_nopriv_getImageOnTopAndTerazzoByManuAndShapeAndSize', 'getImageOnTopAndTerazzoByManuAndShapeAndSize');
add_action('wp_ajax_getImageOnTopAndTerazzoByManuAndShapeAndSize', 'getImageOnTopAndTerazzoByManuAndShapeAndSize');

add_action('wp_ajax_nopriv_upsertProduct', 'upsertProduct');
add_action('wp_ajax_upsertProduct', 'upsertProduct');

add_action('wp_ajax_nopriv_loadMoreLayer', 'loadMoreLayer');
add_action('wp_ajax_loadMoreLayer', 'loadMoreLayer');

add_action('wp_ajax_nopriv_getRelatedDataByFilters', 'getRelatedDataByFilters');
add_action('wp_ajax_getRelatedDataByFilters', 'getRelatedDataByFilters');

add_action('wp_ajax_nopriv_saveDesign', 'saveDesign');
add_action('wp_ajax_saveDesign', 'saveDesign');

add_action('wp_ajax_nopriv_shareDesign', 'shareDesign');
add_action('wp_ajax_shareDesign', 'shareDesign');

add_action('wp_ajax_nopriv_typo3Action', 'typo3Action');
add_action('wp_ajax_typo3Action', 'typo3Action');

add_action('wp_ajax_nopriv_getGalleries', 'getGalleries');
add_action('wp_ajax_getGalleries', 'getGalleries');

add_action('wp_ajax_nopriv_getHtmlFragment', 'getHtmlFragment');
add_action('wp_ajax_getHtmlFragment', 'getHtmlFragment');

add_action('wp_ajax_nopriv_addToCart', 'addToCart');
add_action('wp_ajax_addToCart', 'addToCart');

add_action('wp_ajax_nopriv_editCartItem', 'editCartItem');
add_action('wp_ajax_editCartItem', 'editCartItem');

add_action('wp_ajax_nopriv_removeCartItem', 'removeCartItem');
add_action('wp_ajax_removeCartItem', 'removeCartItem');

add_action('wp_ajax_nopriv_getShippingZonesByCountry', 'getShippingZonesByCountry');
add_action('wp_ajax_getShippingZonesByCountry', 'getShippingZonesByCountry');

add_action('wp_ajax_nopriv_calculateTotalBill', 'calculateTotalBill');
add_action('wp_ajax_calculateTotalBill', 'calculateTotalBill');

add_action('wp_ajax_nopriv_submitShipping', 'submitShipping');
add_action('wp_ajax_submitShipping', 'submitShipping');

add_action('wp_ajax_nopriv_submitPayment', 'submitPayment');
add_action('wp_ajax_submitPayment', 'submitPayment');

add_action('wp_ajax_nopriv_submitOrder', 'submitOrder');
add_action('wp_ajax_submitOrder', 'submitOrder');

add_action('wp_ajax_nopriv_deleteAddress', 'deleteAddress');
add_action('wp_ajax_deleteAddress', 'deleteAddress');

add_action('wp_ajax_nopriv_updateAddress', 'updateAddress');
add_action('wp_ajax_updateAddress', 'updateAddress');

add_action('wp_ajax_nopriv_addNewAddress', 'addNewAddress');
add_action('wp_ajax_addNewAddress', 'addNewAddress');

add_action('wp_ajax_nopriv_updateAccount', 'updateAccount');
add_action('wp_ajax_updateAccount', 'updateAccount');

add_action('wp_ajax_nopriv_deleteAccount', 'deleteAccount');
add_action('wp_ajax_deleteAccount', 'deleteAccount');

add_action('wp_ajax_nopriv_customAjaxLogin', 'customAjaxLogin');
add_action('wp_ajax_customAjaxLogin', 'customAjaxLogin');

add_action('wp_ajax_nopriv_customAjaxSignup', 'customAjaxSignup');
add_action('wp_ajax_customAjaxSignup', 'customAjaxSignup');
