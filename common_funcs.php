<?php
function checkPermissionIfProductDefault($productIdDefault = PRODUCT_DEFAULT_ID)
{
    if ($productIdDefault != PRODUCT_DEFAULT_ID) {
        return true;
    }

    return (!current_user_can('administrator') || !current_user_can("manage_tiles_shop")) ? false : true;
}

function return404()
{
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
}

function checkRequiredParametersOnRoute($expected = [])
{
    $typePageLink = get_permalink(get_page_by_path('tile-tool')) . '?action=' . ACTION_DEFAULT;
    $actual = $_GET;

    $isValid = true;

    if (empty($expected)) {
        return;
    }
    foreach ($expected as $item) {
        if (!isset($actual[$item])) {
            $_SESSION['type-error-message'] = "Missing parameter: $item.";
            $isValid = false;
            break;
        }
    }

    if (!$isValid) {
        wp_redirect($typePageLink);
    }
}

function response($data)
{
    echo json_encode($data, JSON_PRETTY_PRINT);
    wp_die();
}

function listAllShippingZones($withShippingMethods = false)
{
    $shippingZones = WC_Shipping_Zones::get_zones();
    if (empty($shippingZones)) {
        return [];
    }

    $result = [];
    foreach ($shippingZones as $shippingZone) {
        $result[$shippingZone['formatted_zone_location']][] = [
            'zoneId' => $shippingZone['id'],
            'zoneName' => $shippingZone['zone_name'],
            'shipping_methods' => $withShippingMethods ? $shippingZone['shipping_methods'] : ''
        ];
    }

    return !empty($result) ? $result : [];
}

function getAllShippingMethodByCountryAndZoneId($city, $zoneID)
{
    $shippingZones = listAllShippingZones(true);
    $shippingMethods = [];
    foreach ($shippingZones[$city] as $zone) {
        if ($zoneID == $zone['zoneId']) {
            foreach ($zone['shipping_methods'] as $k => $method) {
                if ($method->enabled == 'yes') {
                    $methodData = ['id' => $k, 'cost' => $method->cost];
                    switch ($method->title) {
                        case 'BasicBox':
                            $shippingMethods['basicBox'] = $methodData;
                            break;
                        case 'Box_1sqm':
                            $shippingMethods['box1sqm'] = $methodData;
                            break;
                        case 'BasicPallet':
                            $shippingMethods['basicPallet'] = $methodData;
                            break;
                        case 'Pallet_1sqm':
                            $shippingMethods['pallet1sqm'] = $methodData;
                            break;
                    }
                }
            }
            break;
        }
    }

    return $shippingMethods;
}

function calculateCart($data = [])
{
    if (
        empty($data)
        || !isset($data['zone']) || empty($data['zone'])
        || !isset($data['country']) || empty($data['country'])
        || !isset($_SESSION['customCheckout']) || empty($_SESSION['customCheckout'])
    ) {
        return [];
    }

    $zone = $data['zone'];
    $country = $data['country'];

    $methods = getAllShippingMethodByCountryAndZoneId($country, $zone);
    $tax = get_posts([
        'post_type' => 'country_tax',
        'post_status' => 'publish',
        'post_title' => $country
    ]);

    if (empty($methods) || empty($tax)) {
        return [];
    }

    $tax = pods_field('country_tax', $tax[0]->ID, 'tax-percent', true);
    if (empty($tax)) {
        return [];
    }

    $totalWeight = $totalPieces = $totalBoxes = $totalPallet = $shippingFee = $taxFee = $orderTotal = $totalBill = 0;
    $orderedWeight = $orderedArea = $orderedPricing = $areaPerBox = $areaPerPallet = 0;

    $prodDetailBill = [];
    $cartItems = $_SESSION['customCheckout'];
    foreach ($cartItems as $hash => $item) {
        $productDetail = $item['productDetail'];

        $_orderedArea = $productDetail['area'];
        $_orderedPricing = $productDetail['pricing'];

        $_wpb = $productDetail['wpb'];
        $_wpt = $productDetail['wpt'];

        $_totalPieces = $productDetail['totalPieces'];
        $_totalBoxes = $productDetail['totalBoxes'];
        $_totalPallet = $productDetail['palletes'];

        $_ppa = $productDetail['piecesPerM2'];
        $_ppb = $productDetail['tilesPerBox'];
        $_bpp = ceil($_totalBoxes / $_totalPallet);

        $orderedArea += $_orderedArea;
        $orderedPricing += $_orderedPricing;

        $totalPieces += $_totalPieces;
        $totalBoxes += $_totalBoxes;
        $totalPallet += $_totalPallet;

        $_areaPerBox = $_ppb / $_ppa;
        $areaPerBox += $_areaPerBox;

        $_areaPerPallet = $_areaPerBox * $_bpp;
        $areaPerPallet += $_areaPerPallet;

        if ($_orderedArea >= $_areaPerPallet) {
            // Enough for packaging from 1 and more pallet
            // Then calculate based on pallet shipping fee
            $numberOfPallet = $_orderedArea / $_areaPerPallet;
            $fractionalPart = fmod($numberOfPallet, 1);
            $fractionalPartToArea = ($fractionalPart * $_bpp * $_ppb) / $_ppa;
            $_shippingFee = (floor($numberOfPallet) * $methods['basicPallet']['cost']) + ($fractionalPartToArea * $methods['pallet1sqm']['cost']);
        } else {
            // Enough for packaging in boxes
            // Then calculate based on box shipping fee
            $numberOfBox = $_orderedArea / $_areaPerBox;
            $fractionalPart = fmod($numberOfBox, 1);
            $fractionalPartToArea = ($fractionalPart * $_ppb) / $_ppa;
            $_shippingFee = (floor($numberOfBox) * $methods['basicBox']['cost']) + ($fractionalPartToArea * $methods['box1sqm']['cost']);
        }

        $_shippingFee = wc_format_decimal($_shippingFee, 2);
        $_totalBill = $_shippingFee + floatval($_orderedPricing);
        $shippingFee += $_shippingFee;

        $totalBill += $_totalBill;

        $_taxFee = $_totalBill * intval($tax) / 100;
        $_orderTotal = $_totalBill + $_taxFee;

        $prodDetailBill[$hash] = [
            'bill' => wc_format_decimal($_totalBill, 2),
            'taxFee' => wc_format_decimal($_taxFee, 2),
            'tax' => wc_format_decimal(intval($tax), 2),
            'total' => wc_format_decimal($_orderTotal, 2),
            'shippingFee' => wc_format_decimal($_shippingFee, 2),
        ];

        $taxFee += $_taxFee;
        $orderTotal += $_orderTotal;
        $orderedWeight += $totalBoxes * $_wpb;
    }

    return [
        'totalWeight' => wc_format_decimal($orderedWeight, 2),
        'totalPieces' => wc_format_decimal($totalPieces, 0),
        'totalBoxes' => wc_format_decimal($totalBoxes, 2),
        'totalPallet' => wc_format_decimal($totalPallet, 2),
        'shippingFee' => $shippingFee,
        'totalBill' => wc_format_decimal($totalBill, 2),
        'tax' => $tax,
        'taxFee' => wc_format_decimal($taxFee, 2),
        'orderTotal' => wc_format_decimal($orderTotal, 2),
        'zone' => $zone,
        'country' => $country,
        'prodDetailBill' => $prodDetailBill
    ];
}

class WooAPI
{
    private static $client;

    static function getClient()
    {
        if (self::$client == NULL) {
            try {
                self::$client = new \Automattic\WooCommerce\Client(
                    esc_url(home_url('/')),
                    API_KEY,
                    API_SEC,
                    [
                        'verify_ssl' => false
                    ]
                );
            } catch (Exception $ex) {
                return $ex;
            }
        }

        return self::$client;
    }
}
