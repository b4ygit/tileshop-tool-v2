<?php

function upsertBlankTileConfiguration()
{
    $return = ['success' => 0, 'message' => __('Update blank tile configuration had error.', 'tile-tool')];
    if (!empty($_POST) && !empty($_POST['productID'])) {
        $productId = $_POST['productID'];
        try {
            $existedProduct = get_post($productId);
            if (empty($existedProduct)) {
                throw new Exception('Not existed product with provided ID');
            }

            if (get_option('blank-tile-configuration')) {
                update_option('blank-tile-configuration', $productId);
            } else {
                add_option('blank-tile-configuration', $productId, 'Blank Tile Configuration');
            }
            $return = [
                'success' => 1,
                'message' => 'Update blank tile configuration successful.',
            ];
        } catch (Exception $e) {
            $return['message'] = $e->getMessage();
        }
    }

    response($return);
}

function upsertCustomizedPriceObject()
{
    $return = ['success' => 0, 'message' => 'Update customized price had error.'];
    if (!empty($_POST) && !empty($_POST['priceObject'])) {
        $priceObject = $_POST['priceObject'];
        try {
            if (get_option('customized-price-object')) {
                update_option('customized-price-object', $priceObject);
            } else {
                add_option('customized-price-object', $priceObject, 'Customized Price Object');
            }
            $return = [
                'success' => 1,
                'message' => __('Update customized price successful', 'tile-tool'),
            ];
        } catch (Exception $e) {
            $return['message'] = $e->getMessage();
        }
    }
    response($return);
}

function upsertCustomizedVisualizerBgLayerObject()
{
    $return = ['success' => 0, 'message' => 'Update customized visualizer background layer had error.'];
    if (empty($_POST) || empty($_POST['visualizerBgLayerObj'])) {
        response($return);
    }

    $visualizerBgLayerObj = $_POST['visualizerBgLayerObj'];
    try {
        if (get_option('customized-visualizer-bg-layer-object')) {
            update_option('customized-visualizer-bg-layer-object', $visualizerBgLayerObj);
        } else {
            add_option('customized-visualizer-bg-layer-object', $visualizerBgLayerObj, 'Visualizer Background Layer');
        }
        $return = [
            'success' => 1,
            'message' => __('Update customized visualizer background layer successful', 'tile-tool'),
        ];
    } catch (Exception $e) {
        $return['message'] = $e->getMessage();
    }

    response($return);
}

function upsertColorHoverConfiguration()
{
    $return = [
        'success' => 0,
        'message' => __('Update color hover configuration had error.', 'tile-tool'),
    ];
    if (!empty($_POST) && !empty($_POST['colorHover'])) {
        $colorHover = $_POST['colorHover'];
        try {

            if (get_option('color-hover-configuration')) {
                update_option('color-hover-configuration', $colorHover);
            } else {
                add_option('color-hover-configuration', $colorHover, 'Color Hover Configuration');
            }
            $return = [
                'success' => 1,
                'message' => 'Update color hover configuration successful.',
            ];
        } catch (Exception $e) {
            $return['message'] = $e->getMessage();
        }
    }
    response($return);
}

function updateManuShapeMetadata()
{
    if (empty($_POST) || empty($_POST['manuID']) || empty($_POST['shapeSlug'])) {
        response(['success' => 0, 'message' => __('Error while updating manufacturer shape metadata.', 'tile-tool')]);
    }


    $where = [
        'manu_id' => $_POST['manuID'],
        'shape_slug' => $_POST['shapeSlug'],
    ];
    $data = [
        'summary' => $_POST['summary'],
        'is_enabled' => $_POST['isEnabled']
    ];

    try {
        global $wpdb;
        $existed = $wpdb->get_row($wpdb->prepare(
            "SELECT 1 FROM " . ($wpdb->prefix . 'tool_manufacturer_shape_data') . " WHERE manu_id=%d AND shape_slug=%s LIMIT 1",
            $_POST['manuID'],
            $_POST['shapeSlug']
        ));
        if (!empty($existed)) {
            $wpdb->update($wpdb->prefix . 'tool_manufacturer_shape_data', $data, $where);
        } else {
            $wpdb->insert($wpdb->prefix . 'tool_manufacturer_shape_data', $where + $data);
        }

        response(['success' => 1, 'message' => 'Data was updated successfully.']);
    } catch (Exception $e) {
        response(['success' => 0, 'message' => $e->getMessage()]);
    }
}

function updateManufacturerData()
{
    if (empty($_POST) || empty($_POST['manuID']) || empty($_POST['shapeSlug']) || empty($_POST['sizeSlug'])) {
        response(['success' => 0, 'message' => __('Error while updating manufacturer.', 'tile-tool')]);
    }

    $thickness = preg_replace('/\s+/', '', $_POST['thickness']);
    $thickness = trim($thickness, ',');
    $thickness = explode(',', $thickness);
    foreach ($thickness as $i => $t) {
        $thickness[$i] = preg_replace('/\s+/', '', $t);
    }
    $thickness = array_unique($thickness);

    $imageOnTop = $_POST['image_url_of_on_top_shape'];
    $imageMainPreview = $_POST['image_url_of_main_preview_shape'];
    $imageTileShadowOnTop = $_POST['image_url_of_on_top_tile_shadow_shape'];

    $imagesAddMoreOnTop = json_encode([
        'main' => $_POST['image_url_of_on_add_more_shape'],
        'hover' => $_POST['image_url_of_on_add_more_hover_shape'],
    ]);
    $imagesBackgroundOnTop = json_encode([
        'main' => $_POST['image_url_of_on_background_shape'],
        'hover' => $_POST['image_url_of_on_background_hover_shape'],
    ]);
    $imagesSmallPreviewOnTop = json_encode([
        'main' => $_POST['image_url_of_on_small_preview_shape'],
        'hover' => $_POST['image_url_of_on_small_preview_hover_shape'],
    ]);

    $where = [
        'manu_id' => $_POST['manuID'],
        'shape_slug' => $_POST['shapeSlug'],
        'size_slug' => $_POST['sizeSlug']
    ];
    $data = [
        'color_unit_price' => $_POST['color_unit_price'],
        'terazo_price' => $_POST['terazo_price'],
        'terazo_link' => $_POST['terazo_link'],
        'base_price' => $_POST['base_price'],
        'thickness' => json_encode($thickness),
        'bpp' => $_POST['bpp'],
        'ppa' => $_POST['ppa'],
        'ppb' => $_POST['ppb'],
        'ppp' => $_POST['ppp'],
        'wpt' => $_POST['wpt'],
        'wpb' => $_POST['wpb'],
        'image_url_of_on_top_shape' => $imageOnTop,
        'image_url_of_main_preview_shape' => $imageMainPreview,
        'image_url_of_tiles_shadow_shape' => $imageTileShadowOnTop,
        'image_urls_of_add_more_shape' => $imagesAddMoreOnTop,
        'image_urls_of_small_preview_shape' => $imagesSmallPreviewOnTop,
        'image_urls_of_background_shape' => $imagesBackgroundOnTop,
        'template_configuration_data' => $_POST['template_configuration_data'],
        'option_1' => $_POST['option_1'],
        'option_2' => $_POST['option_2']
    ];

    try {
        global $wpdb;
        $existed = $wpdb->get_row($wpdb->prepare(
            "SELECT 1 FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d AND shape_slug=%s AND size_slug=%s LIMIT 1",
            $_POST['manuID'],
            $_POST['shapeSlug'],
            $_POST['sizeSlug']
        ));
        if (!empty($existed)) {
            $wpdb->update($wpdb->prefix . 'tool_manufacturer_data', $data, $where);
        } else {
            $wpdb->insert($wpdb->prefix . 'tool_manufacturer_data', $where + $data);
        }

        response(['success' => 1, 'message' => 'Data was updated successfully.']);
    } catch (Exception $e) {
        response(['success' => 0, 'message' => $e->getMessage()]);
    }
}

function upsertLogoConfiguration()
{
    $return = ['success' => 0, 'message' => 'Update logo had error.'];
    if (empty($_POST) || empty($_POST['logoUrl'])) {
        response($return);
    }

    $logoUrl = $_POST['logoUrl'];
    try {
        if (get_option('logo-configuration')) {
            update_option('logo-configuration', $logoUrl);
        } else {
            add_option('logo-configuration', $logoUrl, 'Logo Configuration');
        }
        $return = [
            'success' => 1,
            'message' => __('Update logo successful', 'tile-tool'),
        ];
    } catch (Exception $e) {
        $return['message'] = $e->getMessage();
    }

    response($return);
}
function upsertSocialShareUrlsConfiguration()
{
    $return = ['success' => 0, 'message' => 'Update social share urls had error.'];
    if (empty($_POST)) {
        response($return);
    }

    $fbShareUrl = $_POST['fbShareUrl'];
    $instaShareUrl = $_POST['instaShareUrl'];
    $pintsShareUrl = $_POST['pintsShareUrl'];
    try {
        if (get_option('facebook-share-url')) {
            update_option('facebook-share-url', $fbShareUrl);
        } else {
            add_option('facebook-share-url', $fbShareUrl, 'Facebook Share Url Configuration');
        }

        if (get_option('instagram-share-url')) {
            update_option('instagram-share-url', $instaShareUrl);
        } else {
            add_option('instagram-share-url', $instaShareUrl, 'Instagram Share Url Configuration');
        }

        if (get_option('pinterest-share-url')) {
            update_option('pinterest-share-url', $pintsShareUrl);
        } else {
            add_option('pinterest-share-url', $pintsShareUrl, 'Pinterest Share Url Configuration');
        }

        $return = [
            'success' => 1,
            'message' => __('Update social share urls successful', 'tile-tool'),
        ];
    } catch (Exception $e) {
        $return['message'] = $e->getMessage();
    }

    response($return);
}

add_action('wp_ajax_upsertBlankTileConfiguration', 'upsertBlankTileConfiguration');
add_action('wp_ajax_upsertLogoConfiguration', 'upsertLogoConfiguration');
add_action('wp_ajax_upsertSocialShareUrlsConfiguration', 'upsertSocialShareUrlsConfiguration');
add_action('wp_ajax_upsertCustomizedPriceObject', 'upsertCustomizedPriceObject');

add_action('wp_ajax_nopriv_updateManufacturerData', 'updateManufacturerData');
add_action('wp_ajax_updateManufacturerData', 'updateManufacturerData');

add_action('wp_ajax_nopriv_updateManuShapeMetadata', 'updateManuShapeMetadata');
add_action('wp_ajax_updateManuShapeMetadata', 'updateManuShapeMetadata');

// add_action( 'wp_ajax_nopriv_upsertCustomizedVisualizerBgLayerObject', 'upsertCustomizedVisualizerBgLayerObject' );
add_action('wp_ajax_upsertCustomizedVisualizerBgLayerObject', 'upsertCustomizedVisualizerBgLayerObject');
