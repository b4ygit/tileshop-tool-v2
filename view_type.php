<?php

require_once 'view_type_funcs.php';
require_once 'tool_funcs.php'; 

$manufacturers = renderManufacturers();

$error_message = $_SESSION['type-error-message'] ?? null;
if (!empty($error_message)) {
    unset($_SESSION['type-error-message']);
}
 
$header = renderHeader('type');
$menu = renderCollapseMenu();

$html = <<<HTML
  <div id="root">

    $header

    <!-- $menu -->

    <main id="main" class="view-type-template">
      <section class="header-block position-relative reset-max-width" id="headerTypeTool">
        <div class="container pt-2">
          <div class="title-box">
            <h3 class="tool-title">Design your own dream tiles from $45,00/ m2<sup>2</sup></h3>
            <p class="paragraph">We have made the first online tile design tool for you and we partnered up with the best manufacturer worldwide.<br/> Get industry-leading quality tiles with your design in weeks directly to your door-steps!<br/>
            </p>
          </div>
          <div class="checklist-group">
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="check-list first-list">
                  <ul class="hiw-list">
                    <li>Use cement or porcelain tiles for your design</li>
                    <li>Designing your own tiles just takes minutes</li>
                    <li>You can customize over 800 tile designs</li>
                  </ul>
                  <div class="btn-group">
                    <a target="_blank" href="https://www.tiles.design/custom-tile" class="link blue-style mt-2">FIND OUT HOW IT WORKS</a>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <div class="check-list">
                  <ul class="hiw-list"> 
                    <li>Low minimum orders from 20 m<sup>2</sup> each design</sup></li>
                    <li>WE DELIVER worldwide TO 78 COUNTRIES</li>
                    <li>MANY SAFE AND SECURE PAYMENT METHODS</li>
                  </ul>
                  <div class="btn-group">
                    <div class="row">
                      <div class="col-auto"><a target="_blank" href="https://www.tiles.design/samples" class="link blue-style mt-2">ORDER SAMPLE TILES</a></div>
                      <div class="col-auto"><a target="_blank" href="https://www.tiles.design/pricing" class="link mt-2">SEE PRICING AND DISCOUNTS</a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="select-shape-block warp-shape-block">
                <div class="container-fluid shape-container">
                    <div class="title-box">
                        <h3 class="tool-title">CHOOSE A TILE AND SHAPE</h3>

                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <!-- Nav tabs -->
                                <div class="content-tabs-sharps-tiles">
                                    $manufacturers
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    </main>

    <footer class="footer-design">
      <div class="footer-design-wrap border-top" id="footer-design-wrap">
        <p class="text-welcome m-0">Call us for any inquiry! <a style="color: #4f94f5 !important;" href="tel:+1-313-444-6424">&nbsp;+1-313-444-6424</a></p>
        <a target="_blank" href="https://calendly.com/tiles-design/showcase" class="link   blue-style large btn-next-step bt-book-session w-100" id="btn-nxt-step">Book a free demo</a>
      </div>
    </footer>
  </div>

HTML;

echo $html;
