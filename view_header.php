<?php

function renderHeader($activePage)
{  
  $shapeActiveClass = $styleActiveClass = $designActiveClass = $cartActiveClass = '';
  $shape = '';
  $productId = 0;
  $backButtonLabel = "Exit Tool";
  $backButtonURL = "http://www.tiles.design"; 
  $typeLink = $styleLink = $designLink = '#';
  $chooseShapeLabel = esc_html__('Select a Tile', 'tile-tool');
  $chooseStyleLabel = esc_html__('Choose a Style', 'tile-tool');
  $designTileLabel = esc_html__('Design your Tile', 'tile-tool');
  switch ($activePage) {
    case 'type':
      $shapeActiveClass = 'active';

      $typeLink = '?';
      break;
    case 'style':
      $styleActiveClass = 'active';
      $shape = $_GET['shape'];
      $shapeActiveClass = 'prev-step';

      $typeLink = '?';
      $styleLink = "?action=style-selected&shape=$shape";

      $backButtonLabel = "BACK TO SHAPES";
      $backButtonURL = "?";
      $chooseShapeLabel = esc_html__('Select another Tile', 'tile-tool');
      break;
    case 'design':
      $designActiveClass = 'active';
      $shape = $_GET['shape'];
      $manuID = $_GET['manu_id'];
      $productId = $_GET['product_id'];
      $shapeActiveClass = 'prev-step';
      $styleActiveClass = 'prev-step';
      $chooseShapeLabel = esc_html__('Select another Tile', 'tile-tool');
      $chooseStyleLabel = esc_html__('Choose another Style', 'tile-tool');
      $typeLink = '?';
      $styleLink = "?action=style-selected&shape=$shape&manu_id=$manuID";
      $designLink = "?action=design&product_id=$productId&shape=$shape&manu_id=$manuID";

      $backButtonLabel = "BACK TO DESIGNS";
      $backButtonURL = $styleLink;
      break;
    case 'cart':
      $cartActiveClass = 'active';

      $shapeActiveClass = 'prev-step';
      $styleActiveClass = 'prev-step';
      $designActiveClass = 'prev-step';
      break;
  }


  $shoppingCartLabel = esc_html__('Shopping Cart', 'tile-tool');
  $menuLabel = esc_html__('Menu', 'tile-tool');

  $loadingImg = esc_url(plugins_url('assets/index.ripple-radio-preloader.svg', __FILE__));
  $logoImgUrl = get_option('logo-configuration') ?? esc_url(plugins_url('assets/new-assets/img/logo.png', __FILE__));

  $cartItem = isset($_SESSION['customCheckout']) ? count($_SESSION['customCheckout']) : 0;
  $cartItem = (int)$cartItem ? '(<span id="cartItem">' . $cartItem . '</span>)' : '';
  $cartLink = $cartItem ? '?action=cart' : '#';

  $html = <<<HTML
    <header class="header no-border" id="header">
        <div class="container-fluid h-100">
            <div class="row justify-content-center justify-content-sm-between">
                <div class="col-auto">
                <a href="https://www.tiles.design" class="logo"> 
                    <img src="$logoImgUrl" alt="Back to home">
                    <p class="logo-claim">Custom tiles made easy</p>
                </a>
                </div>
                <div class="col justify-content-between step-header-block">
                <ul class="header-step row justify-content-between d-none d-lg-flex">
                    <li class="col $shapeActiveClass"><a href="$typeLink">$chooseShapeLabel</a></li> 
                    <li class="col $styleActiveClass"><a href="$styleLink">$chooseStyleLabel</a></li>
                    <li class="col $designActiveClass"><a href="$designLink">$designTileLabel</a></li>

                </ul>
                </div>
                <div class="col-auto d-flex align-items-center">
                <a href="$backButtonURL" class="link pl-2 link-tool-back border-blue">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" viewBox="0 0 14 10" fill="none">
              <path d="M5.14346 0.673555C5.38252 0.90793 5.38252 1.29699 5.14814 1.53606L2.30283 4.39074H13.145C13.4778 4.39074 13.7497 4.66262 13.7497 5.00012C13.7497 5.33762 13.4778 5.60949 13.145 5.60949H2.30283L5.15283 8.46418C5.38721 8.70324 5.38252 9.08762 5.14814 9.32668C4.90908 9.56106 4.52939 9.56106 4.29033 9.32199L0.427832 5.43137C0.37627 5.37512 0.334082 5.31418 0.30127 5.23918C0.268457 5.16418 0.254395 5.08449 0.254395 5.00481C0.254395 4.84543 0.315332 4.69543 0.427832 4.57824L4.29033 0.687618C4.52002 0.443868 4.90439 0.43918 5.14346 0.673555Z" fill="#A1AAB6"></path>
            </svg>
            $backButtonLabel
          </a>

                </div>
            </div>
        </div>
    </header>
    <input type="hidden" id="loadingImg" value="$loadingImg"/>


<div id="screenTooSmall">
 <div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width: 1000px; max-height: 605px;margin-top: -5px;">
      <div class="modal-content">
        <div class="modal-header justify-content-center border-0">
          <h5 class="modal-title pt-3" id="toolVisualizarModalCenterTitle">Sorry your Screen is too small!</h5>
        </div>
        <div class="modal-header justify-content-center border-0">
        	To use our tile design tool please come back on a tablet or a desktop! 
          </div>
         <div class="modal-header justify-content-center border-0">
         <img src="https://www.tiles.design/fileadmin/user_upload/assets/Laptop_Design-Tool.jpg" height="150px" />
          
          </div>
    	
      </div>
    </div>
</div>
';
HTML;

  return $_SERVER['HTTP_HOST'] == TYPO3_HOST ? '' : $html;
}

function renderShippingPaymentHeader($activePage)
{
  $shippingActiveClass = $paymentActiveClass = '';
  switch ($activePage) {
    case 'shipping':
      $shippingActiveClass = 'active';
      break;
    case 'payment':
      $paymentActiveClass = 'active';

      $shippingActiveClass = 'prev-step';
      break;
  }

  $backToCartLabel = esc_html__('Back to your cart', 'tile-tool');
  $shippingLabel = esc_html__('Shipping address', 'tile-tool');
  $paymentLabel = esc_html__('Payment', 'tile-tool');

  $loadingImg = esc_url(plugins_url('assets/index.ripple-radio-preloader.svg', __FILE__));
  $logoImgUrl = get_option('logo-configuration') ?? esc_url(plugins_url('assets/new-assets/img/logo.png', __FILE__));

  $cartLink = home_url('/tile-tool') . '?action=cart';

  $html = <<<HTML
    <header class="header no-border" id="header">
      <div class="container-fluid h-100">
        <div class="row justify-content-center justify-content-sm-between">
          <div class="col-auto">
            <a href="index.html" class="logo">
              <img src="$logoImgUrl" alt="">
            </a>
          </div>
          <div class="col justify-content-between step-header-block">
            <ul class="header-step row justify-content-between d-none d-lg-flex">
              <li class="col prev-step"><a href="$cartLink"><i class="fa fa-angle-left"></i> $backToCartLabel</a></li>
              <li class="col $shippingActiveClass">$shippingLabel</li>
              <li class="col $paymentActiveClass">$paymentLabel</li>
            </ul>
          </div>
          <div class="col-auto d-flex align-items-center">
            <button class="btn btn-link btn-menu p-0 ml-3" type="button" data-toggle="collapse" data-target="#collapseMenu" aria-controls="collapseMenu" aria-expanded="false" aria-label="Toggle docs navigation">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="30" height="30" focusable="false">
                <title>Menu</title>
                <path stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" d="M4 7h22M4 15h22M4 23h22"></path>
              </svg>
            </button>
          </div>
        </div>
      </div>
    </header>
    <input type="hidden" id="loadingImg" value="$loadingImg"/>
HTML;

  return $html;
}
