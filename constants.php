<?php

$styleSelectedPageLink = get_permalink(get_page_by_path('tile-tool')) . '?action=style-selected&shape=';
$typePageLink = get_permalink(get_page_by_path('tile-tool')) . '?action=type';
$blankTileUrl = get_permalink(get_page_by_path('tile-tool')) . '?action=design&product_id=' . get_option('blank-tile-configuration');

$logoImgUrl = get_option('logo-configuration') ?? esc_url(plugins_url('assets/new-assets/img/logo.png', __FILE__));
$iconArrowLeftUrl = esc_url(plugins_url('assets/new-assets/img/icon-arrow-left.svg', __FILE__));
$polygonSvgUrl = esc_url(plugins_url('assets/new-assets/img/polygon1.svg', __FILE__));
$polygonJpgUrl = esc_url(plugins_url('assets/new-assets/img/polygon1.jpg', __FILE__));
$polygonActiveSvgUrl = esc_url(plugins_url('assets/new-assets/img/polygon1-active.svg', __FILE__));
$thankYouUrl = esc_url(plugins_url('assets/new-assets/img/thanksyou.jpg', __FILE__));

$iconAddSvgUrl = esc_url(plugins_url('assets/new-assets/img/icon-add.svg', __FILE__));
$iconAddPlusSvgUrl = esc_url(plugins_url('assets/new-assets/img/icon-add-plus.svg', __FILE__));
$iconAddPlusHoverSvgUrl = esc_url(plugins_url('assets/new-assets/img/icon-add-plus-white.svg', __FILE__));

$patternIcon = esc_url(plugins_url('assets/new-assets/img/icon-tile1.png', __FILE__));
$patternIcon4 = esc_url(plugins_url('assets/new-assets/img/icon-tile4.png', __FILE__));
$patternIcon7 = esc_url(plugins_url('assets/new-assets/img/icon-tile7.png', __FILE__));

$iconTileResult = esc_url(plugins_url('assets/new-assets/img/tiles-result.png', __FILE__));
$iconTile2 = esc_url(plugins_url('assets/new-assets/img/tiles2.png', __FILE__));

$iconColorPickerUrl = esc_url(plugins_url('assets/new-assets/img/icon-color-picker.svg', __FILE__));
$iconResetUrl = esc_url(plugins_url('assets/new-assets/img/icon-reset.svg', __FILE__));
$iconResetBlackUrl = esc_url(plugins_url('assets/new-assets/img/icon-reset-black.svg', __FILE__));

$iconMoveUrl = esc_url(plugins_url('assets/new-assets/img/icon-move.svg', __FILE__));
$iconRotateUrl = esc_url(plugins_url('assets/new-assets/img/icon-rotate.svg', __FILE__));
$iconScaleUrl = esc_url(plugins_url('assets/new-assets/img/icon-scale.svg', __FILE__));
$iconSkewUrl = esc_url(plugins_url('assets/new-assets/img/icon-skew.svg', __FILE__));

$iconToolMoveUrl = esc_url(plugins_url('assets/new-assets/img/tool-move.png', __FILE__));
$iconRotateFrameUrl = esc_url(plugins_url('assets/new-assets/img/rotate-frame.png', __FILE__));

$iconUser = esc_url(plugins_url('assets/new-assets/img/icon-user.svg', __FILE__));
$iconDashboard = esc_url(plugins_url('assets/new-assets/img/admin-icon-dashboard.svg', __FILE__));
$iconCart = esc_url(plugins_url('assets/new-assets/img/admin-icon-cart.svg', __FILE__));
$iconDesign = esc_url(plugins_url('assets/new-assets/img/admin-icon-design.svg', __FILE__));

$cartImg = esc_url(plugins_url('assets/new-assets/img/iso-4.png', __FILE__));
$iconShare = esc_url(plugins_url('assets/new-assets/img/icon-share.svg', __FILE__));
$iconSave = esc_url(plugins_url('assets/new-assets/img/icon-w-star.svg', __FILE__));
$iconVisualize = esc_url(plugins_url('assets/new-assets/img/ios-eye2.svg', __FILE__));
$iconSize = esc_url(plugins_url('assets/new-assets/img/icon-size.png', __FILE__));
$iconEye = esc_url(plugins_url('assets/new-assets/img/icon-eye.png', __FILE__));
$iconFitlerChecked = esc_url(plugins_url('assets/new-assets/img/filter-checked.png', __FILE__));

$pricePerTileLabel = esc_html__('<strong> price per tile (usd) </strong>', 'tile-tool');
$equalLabel = esc_html__('equal', 'tile-tool');
$areaLabel = esc_html__('<strong> area (m<sup>2</sup>) </strong>', 'tile-tool');

$fixedPriceManagerBlockLabel = esc_html__('Manage price with area', 'tile-tool');

$noi_that_over_lay = esc_url(plugins_url('assets/new-assets/img/noithat-overlay.png', __FILE__));

$shippingLink = home_url('/tile-tool') . '?action=shipping';
