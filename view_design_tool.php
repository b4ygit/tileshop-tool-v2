<?php

require_once 'view_design_tool_funcs.php';

list($productId, $shape, $size, $productConfigData, $uid, $manuID) = extractRequest($_GET['action'], $_GET);
if (empty($productId) || empty($shape)) {
  return404();
}
if($_GET['manuID']>0) $manuID=$_GET['manuID'];

if($manuID==844) $productName="CEMENT TILE";
else $productName="PORCELAIN TILE";
$uidHtml = '<input id="typo3-uid" type="hidden" value="' . $uid . '" />';
$referrerHost = null;
if (isset($_SERVER['HTTP_REFERER'])) {
  $referrerHost = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
} 
$isTypo3Referrer = 0;
if ($referrerHost != null && $referrerHost == 'fliesen.io') {
  $isTypo3Referrer = 1;
}
$isTypo3ReferrerHtml = '<input id="is-typo3-referrer" type="hidden" value="' . $isTypo3Referrer . '" />';

$productDetail = getProductInfoByFilter($productId, $shape, $size, $manuID);
if (empty($productDetail)) {
  return404();
}
$queryString = $_SERVER["QUERY_STRING"];
$queryParams = [];
parse_str($queryString, $queryParams);
unset($queryParams['action']);
unset($queryParams['product_id']);
$blankTileUrl .= "&" . implode('&', array_map(function ($item) {
  return $item[0] . '=' . $item[1];
}, array_map(null, array_keys($queryParams), $queryParams)));

if (in_array($_GET['action'], ['shareLink', 'editCart'])) {
  $productDetail['configData'] = json_decode($productConfigData, true);
}

$saveDesignLabel = __('SAVE DESIGN', 'tile-tool');
$saveChangeLabel = __('SAVE CHANGE', 'tile-tool');

$cartLabel = __('Add to cart', 'tile-tool');
$cartBtnFunction = 'onClickAddToCartBtn(this);';

$editItemLabel = __('Edit item', 'tile-tool');
$editItemBtnFunction = 'onClickEditCartItemBtn(this);';

$addNewBtnGroupHide = '';
$editBtnGroupHide = 'hide';

$cartItemId = '';
if ($_GET['action'] == 'editCart') {
  $addNewBtnGroupHide = 'hide';
  $editBtnGroupHide = '';
  $cartItemId = '<input type="hidden" id="cartItemId" value="' . $_GET['id'] . '" />';
}

$isCheckedTerazzo = '';
if ($_GET['action'] == 'editCart') {
  $item = $_SESSION['customCheckout'][$_GET['id']];
  $isCheckedTerazzo = $item['productDetail']['terazo'] ? 'checked' : '';
} else if ($_GET['action'] == 'shareLink') {
  $item = getTileToolShareLink($_GET['id'], ['terazo']);
  $isCheckedTerazzo = $item->terazo ? 'checked' : '';
}

$layerTags = renderLayerTags();
$initialData = renderInitialData($productDetail, $shape, $manuID);
$layersData = renderLayersData($productDetail);
$patternListHtml = renderPatternList($productDetail);
$colorSelectionListHtml = renderColorSelectionList($productDetail);
list($sizeSelectionListHtml, $totalSizes) = renderSizeSelectionList($productDetail);
$hideArrowSizeList = $totalSizes == 1 ? 'hide-arrow' : '';
$priceNodeHtml = renderPriceNode($productDetail);
$visualizerBgLayerHtml = renderVisualizerBgLayer($productDetail);
list($visualizerFragmentHtml, $totalFragments) = renderFragmentHtml($productDetail);
$hideArrowFragmentList = $totalFragments == 1 ? 'hide-arrow' : '';
$m_categoryList = renderAllCategoryList($productDetail);
$m_manuList = renderAllManuList($productDetail);
$m_productTypeList = renderAllProductTypeList($productDetail);
$m_priceAreaPairs = renderPriceAreaPairs($productDetail);
$styleSelectedPageLink = $styleSelectedPageLink . $shape;

$hideIfNotFixed = $productDetail['designType'] == 'fixed' ? '' : 'hide';
$hideIfNotCustomized = $productDetail['designType'] == 'customized' ? '' : 'hide';

$hideIfNotAdmin = current_user_can('administrator') || current_user_can("manage_tiles_shop") ? '' : 'hide';
$hideIfNotAdminAndNotDefaultProductOrInShareMode = ((current_user_can('administrator') || current_user_can("manage_tiles_shop"))
  && (in_array($productId, [PRODUCT_DEFAULT_ID, get_option('blank-tile-configuration')]) || $_GET['action'] == 'shareLink')) ? '' : 'hide';
$hideManagerModeWhenPageIsNotDesignPage = $_GET['action'] != 'design' ? 'hide' : '';

$minimumOrder = $productDetail['minimumOrder'];
$packagingInfo = $productDetail['packaging'];
$deliveryInfo = $productDetail['delivery'];

$subText_1 = $productDetail['subtext_1'];
$subText_2 = $productDetail['subtext_2'];
$subText_3 = $productDetail['subtext_3'];

$productTitle = $productDetail['title'];
$productInStockLabel = $productDetail['in_stock'] ? 'In Stock' : 'Out of Stock';
$productStockQuantity = $productDetail['in_stock'] && $productDetail['stock_quantity'] && $productDetail['managing_stock'] ? $productDetail['stock_quantity'] : 0;
$hideProductStockQuantity = !$productStockQuantity ? 'hide' : '';

$cartItem = isset($_SESSION['customCheckout']) ? count($_SESSION['customCheckout']) : 0;

$fbShareUrl = get_option('facebook-share-url');
$instaShareUrl = get_option('instagram-share-url');
$pintsShareUrl = get_option('pinterest-share-url');

$header = renderHeader('design');
$menu = renderCollapseMenu();
$loginModalHtml = renderLoginModal();
$signUpModalHtml = renderSignupModal();

$loadingImg = esc_url(plugins_url('assets/index.ripple-radio-preloader.svg', __FILE__));

$backLabel = __('BACK');
$cementTileCreatorLabel = $productName . " " . $productDetail['title'];
$terazzoLabel = __('TERAZZO');
$managerModeLabel = __('MANAGER MODE');
$patternLabel = __('PATTERN');
$productInfoLabel = __('PRODUCT INFORMATION');
$typeLabel = __('Type');
$cementTileLabel = __('Cement tile');
$minimumOrderLabel = __('Minimum Order');
$tilesLabel = __('tiles');
$dimensionLabel = __('Dimensions');
$thicknessLabel = __('Thickness');
$deliveryLabel = __('Delivery');
$boxesLabel = __('Boxes');
$colorLabel = __('Color');
$moveLabel = __('Move');
$rotateLabel = __('Rotate');
$scaleLabel = __('Scale');
$skewLabel = __('Skew');
$colorsInUserLabel = __('Colors in Use');
$resetAllLabel = __('Reset all');
$backgroundColorLabel = __('BACKGROUND COLOR');
$shareLabel = __('SHARE');
$visualizeLabel = __('VISUALIZE');
$saveLabel = __('SAVE');
$productDesignTypeLabel = __('Product Design Type');
$manuListLabel = __('Manufacturer List');
$cateListLabel = __('Category List');
$shapeListLabel = __('Shape List');
$sizeListLabel = __('Size List');
$productTitleLabel = __('Product Title');
$packagingLabel = __('Packaging');
$subtextLabel = __('Subtext');
$actionLabel = __('Action');
$createLabel = __('Create');
$updateLabel = __('Update');
$totalLabel = __('TOTAL');
$tilesUpperLabel = __('Tiles');
$totalSmallLabel = __('Total');
$addAsNewLabel = __('Add as new Tile');
$updateBasketTile = __('Update Basket Tile');
$getStartLabel = __('Get started with tile design');
$createAccLabel = __('CREATE ACCOUNT');
$signInLabel = __('SIGN IN');
$orLoginSocialLabel = __('Or login with your social account');
$sendLabel = __('SEND');
$deliveryTimeLabel = __('Delivery time');
$areaInfoLabel = __('Area info');
$shareDesignLabel = __('Share your design!');
$copyLinkLabel = __('Copy link');
$addNewElementLabel = __('Add a element to your tile and then customize it');
$tileVisualizerLabel = __('Visualize');
$tileSizeLabel = __('Tile size');
$roomDemoLabel = __('Room demo');
$infoLabel = __('INFORMATION');
$editDesignLabel = __('EDIT DESIGN');
$noLabel = __('NO');
$yesLabel = __('YES');

$letsPlayWithDesignLabel = __("Let's play with design - or");
$startProductTourLabel = __("START A PRODUCT TOUR");

$html = <<<HTML
    $initialData
    $uidHtml
    $isTypo3ReferrerHtml
    $cartItemId
    <input type="hidden" id="loadingImg" value="$loadingImg"/>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
  <!-- Add your site or application content here -->
  <div id="root">

    $header

    <!-- $menu  -->

    <main id="main">
      <!-- gallery -->

      <section class="d-flex align-items-center justify-content-center" id="tool-section-align">
        <div class="tool-container-wrap w-100">
          <div hide-mobileclass="container hide-mobile position-relative" id="toolTitle">
            <h3 class="tool-title  text-center">$cementTileCreatorLabel</h3>
          </div>
          <div class="tool-responsive" id="toolResponsive">
            <div class="container px-3" id="tool-container">
              <div class="row no-gutters justify-content-center">
                <div class="col">
                  <div class="d-flex justify-content-between tool-left-text">
                    <div class="text-uppercase d-flex align-items-center hide" id="terazzoBlock" style="margin-left: 12px;">
                      <label class="m-0 mr-1 d-flex align-items-center"><input type="checkbox" autocomplete="off" id="terazoOpt" class="d-none" $isCheckedTerazzo onchange="onChangeTerazzo(this);">
                        <div class="check-slider"></div><span class="ml-1">$terazzoLabel</span>
                      </label>
                      <sup><button class="no-style btn-tooltip-info ml-0" data-toggle="tooltip" data-placement="bottom" data-html="true" title="tooltip stock"></button></sup>
                    </div>
                    <div  class="group-item d-flex">
                      <!-- <div class="text-uppercase d-flex align-items-center choose-pattern-dropdown ">
                        <div class="d-flex">
                          <div class="mr-2 d-flex align-items-center icon banner-select-icon"><img src="$patternIcon" alt="" class=""></div>
                          <label class="m-0 d-flex align-items-center label-text">
                            <span class="ml-1">PATTERN</span>
                          </label>
                          <select name="" id="dropdownPattern-list-popup" class="nice-select" onchange="onChangeFragmentVisualizer(this);">
                            $visualizerFragmentHtml
                          </select>
                        </div>
                      </div> -->
                      <div class="text-uppercase d-flex align-items-center $hideIfNotAdmin $hideManagerModeWhenPageIsNotDesignPage">
                        <label class="m-0 mr-1 d-flex align-items-center"><input type="checkbox" autocomplete="off" onclick="onToggleManagerMode(this)" class="d-none">
                          <div class="check-slider"></div><span class="ml-1">$managerModeLabel</span>
                        </label>
                      </div>
                      <div class="d-flex">
                        <div class="mr-4 d-flex align-items-center">
                          <div class="dropdown dropdownPattern dropdownPatternTool">
                            <div class="selected-pattern"><img src="$patternIcon" alt="" id="selected-pattern-img"></div>
                            <button class="btn dropdown-toggle" type="button" id="dropdownPattern" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            $patternLabel
                            </button>
                            <div class="dropdown-menu dropdown-menu-right py-0" aria-labelledby="dropdownPattern" id="dropdownPattern-list">
                              <a class="dropdown-item" href="#" data-value="0" data-text="PATTERN"><img src="$patternIcon" alt=""></a>
                              <a class="dropdown-item" href="#" data-value="1" data-text="PATTERN"><img src="$patternIcon4" alt=""></a>
                              <a class="dropdown-item" href="#" data-value="2" data-text="PATTERN"><img src="$patternIcon7" alt=""></a>
                            </div>
                          </div>
                        </div>
                        <div class="hide text-uppercase d-flex align-items-center">
                          <label class="m-0 mr-1 d-flex align-items-center"><input type="checkbox" autocomplete="off" id="checkbox-grid" class="d-none">
                            <div class="check-slider"></div><span class="ml-1">Ruler</span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="visual-design border">
                    <div id="grid-layout"></div>
                    <!-- <div id="terazzo-layout"></div> -->
                    <div class="visual-design-content">
                      <!-- <img src="assets/img/polygon.png" alt="" class="frame-img"> -->
                      <div id="layerCanvasBorder" class="thumb-block" style="width: 400px;">
                        <div id="layerCanvas" style="width: 400px; height: 400px;">
			<div id="layerCanvas-bg" style="width: 380px; height: 380px; position: absolute; right: 50px; bottom: 16px;"></div>
                        <!-- <div id="layerCanvas" style="width: 379px; height: 379px;"> -->
                        $layersData
                        <img src="" id="mainCanvasImageOnTop" alt="mainCanvasImageOnTop" style="z-index:99;"/>
                        <img src="" id="mainCanvasTerazzoLayout" alt="mainCanvasTerazzoLayout" style="z-index:98;"/>
                        <img src="" id="typo3Image" alt="typo3Image" style="z-index:100;" />
                        <img src="" id="mainCanvasTileShadowLayout" alt="mainCanvasTileShadowLayout" style="z-index:101;"/>

			</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="fixedTool" class="col-auto d-flex hide">
                  <div class="pattern-box d-flex flex-column justify-content-end">

                  </div>
                  <div style="background: #fff; margin-top:17px !important;" id="control-layer">
                    <div>
                      <div style="background: #ECECEC;"><span class="lbl-product-infomation">$productInfoLabel</span></div>
                      <div class="tbl-cart-info-detail-wrapper">
                        <table class="w-100 tbl-cart-info-detail">
                          <thead>
                            <tr>
                              <th>$typeLabel</th>
                              <!-- <th>Color</th> -->
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="pb-3"><span class="black">$cementTileLabel</span><br>ISO 2001 certified</td>
                              <!-- <td class="pb-3">
                                <div class="color-box color-box-height-auto">
                                  <div class="row mb-3">
                                    <div class="col-3">
                                      <label class="lbl-item"><input type="radio" name="color" class="d-none">
                                        <div class="color-item" style="background: #C6D8E0"></div>
                                      </label>
                                    </div>
                                    <div class="col-3">
                                      <label class="lbl-item"><input type="radio" name="color" class="d-none">
                                        <div class="color-item" style="background: #34505C"></div>
                                      </label>
                                    </div>
                                    <div class="col-3">
                                      <label class="lbl-item"><input type="radio" name="color" class="d-none">
                                        <div class="color-item" style="background: #607C89"></div>
                                      </label>
                                    </div>
                                    <div class="col-3">
                                      <label class="lbl-item"><input type="radio" name="color" class="d-none">
                                        <div class="color-item" style="background: rgba(0, 0, 0, 0.12)"></div>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </td> -->
                            </tr>
                          </tbody>
                          <thead>
                            <tr>
                              <th>$minimumOrderLabel</th>
                              <th>$productInStockLabel</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="pb-3"><span class="black">$minimumOrder m2<br><span class="text-opacity"><span id="fixedTileMinOrderTiles">60</span> $tilesLabel</span></td>
                              <td class="pb-3"><span class="black $hideProductStockQuantity"><span id="fixedTileCurrentOrderedSquare">63</span> m2<br><span class="text-opacity"><span id="fixedTileCurrentOrderedTiles">$productStockQuantity</span> $tilesLabel</span></td>
                            </tr>
                          </tbody>
                          <thead>
                            <tr>
                              <th>$dimensionLabel</th>
                              <th>$thicknessLabel</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="pb-3"><span class="black"><span id="fixedTileSelectedSize">300 x 200</span> mm</span><br><span id="fixedTileShape">Hexagon</span></td>
                              <td class="pb-3"><span class="black"><span id="fixedTileSelectedThickness">17</span> mm</span><br></td>
                            </tr>
                          </tbody>
                          <thead>
                            <tr>
                              <th>$deliveryLabel</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="pb-3"><span class="black">$deliveryInfo</span><br><span id="fixedTileCurrentBoxes">13</span> $boxesLabel</td>
                              <td class="pb-3"></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!-- <div class="pb-4">
                      <div class="text-center">
                        <a href="tool.html" class="link grey"><img src="assets/img/icon-edit-white.svg" alt="icon customize this design" class="mr-2">
                          CUSTOMIZE THIS DESIGN</a>
                      </div>
                    </div> -->
                  </div>
                </div>


                <div id="customizedTool" class="col-auto d-flex hide">
                  <div class="pattern-box d-flex flex-column justify-content-end">
                    <div class="pattern-list-before $hideIfNotCustomized">
                      <button id="toggleModalElementSlider" class="btn-add-pattern">
                        <img src="$iconAddSvgUrl" alt="icon add" id="addLayerCover" width="40px" height="40px">
                      </button>
                      <p class="add-element">Add Element</p>
                    </div>

                    $patternListHtml

                  </div>
                  <div style="background: #fff;margin-top:17px !important;" id="control-layer">
                    <div class="tool-layer active" id="tool-layer">
                      <ul class="nav nav-tabs tabs-tools justify-content-between" id="myTab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="color-tab" data-toggle="tab" data-tab="pane-color" href="#pane-color" role="tab" aria-controls="color" aria-selected="true"><span class="tab-color-current tab-color-layer mr-2" style="background-color: #5F707F;"></span>
                          $colorLabel</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="move-tab" data-toggle="tab" data-tab="pane-move" href="#pane-move" role="tab" aria-controls="move" aria-selected="false"><img src="$iconMoveUrl" alt="" class="mr-2">
                          $moveLabel</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="rotate-tab" data-toggle="tab" data-tab="pane-rotate" href="#pane-rotate" role="tab" aria-controls="rotate" aria-selected="false"><img src="$iconRotateUrl" alt="" class="mr-2">
                            $rotateLabel</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="scale-tab" data-toggle="tab" data-tab="pane-scale" href="#pane-scale" role="tab" aria-controls="scale" aria-selected="false"><img src="$iconScaleUrl" alt="" class="mr-2">
                            $scaleLabel</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="skew-tab" data-toggle="tab" data-tab="pane-skew" href="#pane-skew" role="tab" aria-controls="skew" aria-selected="false"><img src="$iconSkewUrl" alt="" class="mr-2">
                            $skewLabel</a>
                        </li>
                      </ul>
                      <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="pane-color" role="tabpanel" aria-labelledby="color-tab">
                          <div class="color-box px-4 pt-3" id="color-box">
                            <div class="row tool-header">
                              <div class="col-6">
                                $colorsInUserLabel
                              </div>
                              <div class="col-6 text-right">
                                <button class="btn-reset-color" data-type="color"><img src="$iconResetUrl" alt="" class="mr-2">
                                  $resetAllLabel</button>
                              </div>
                            </div>
                            <div class="row mb-2" id="recent-color-list">
                              <div class="col-auto d-flex align-items-center hide">
                                <button class="btn-color-picker p-2"><img src="$iconColorPickerUrl" alt=""></button>
                              </div>
                              <!-- Recently selected color -->
                              <!-- Recently selected color -->
                            </div>
                            <!-- List of color -->
                            $colorSelectionListHtml
                            <!-- List of color -->
                          </div>

                        </div>
                        <div class="tab-pane fade" id="pane-move" role="tabpanel" aria-labelledby="move-tab">
                          <div class="px-4 pt-3">
                            <div class="row mb-3 justify-content-end">
                              <div class="col-6 text-right">
                                <button class="btn-reset-color" data-type="move"><img src="$iconResetUrl" alt="" class="mr-2">
                                $resetAllLabel</button>
                              </div>
                            </div>
                            <div class="row no-gutters">
                              <div class="col canvas-wrapper">
                                <div class="border preview-canvas-parent">
                                </div>
                              </div>
                              <div class="col-auto pl-4 col-slider-ver">
                                <div class="d-flex">
                                    <input id="move-up-down" data-slider-id="move-up-down" type="text" class="slider-range" />
                                  <div class="d-flex flex-column justify-content-between align-items-center pl-3">
                                    <button class="btn-move-step rotate-90" data-step="3" data-type="up" data-slider="move-up-down"><i class="ti-angle-left"></i><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="2" data-type="up" data-slider="move-up-down"><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="1" data-type="up" data-slider="move-up-down"><i class="ti-angle-left"></i></button>
                                    <input type="text" autocomplete="off" id="move-up-down-value" class="input-move-step border">
                                    <button class="btn-move-step rotate-90" data-step="1" data-type="down" data-slider="move-up-down"><i class="ti-angle-right"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="2" data-type="down" data-slider="move-up-down"><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="3" data-type="down" data-slider="move-up-down"><i class="ti-angle-right"></i><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                  </div>
                                </div>
                              </div>

                            </div>
                            <div class="mt-4 row no-gutters">
                              <div class="col-9">
                                <input id="move-left-right" data-slider-id="move-left-right" type="text" class="slider-range" />
                                <div class="d-flex justify-content-between align-items-center mt-3">
                                  <button class="btn-move-step" data-step="3" data-type="left" data-slider="move-left-right"><i class="ti-angle-left"></i><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                  <button class="btn-move-step" data-step="2" data-type="left" data-slider="move-left-right"><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                  <button class="btn-move-step " data-step="1" data-type="left" data-slider="move-left-right"><i class="ti-angle-left"></i></button>
                                  <input type="text" autocomplete="off" id="move-left-right-value" class="input-move-step border">
                                  <button class="btn-move-step" data-step="1" data-type="right" data-slider="move-left-right"><i class="ti-angle-right"></i></button>
                                  <button class="btn-move-step" data-step="2" data-type="right" data-slider="move-left-right"><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                  <button class="btn-move-step" data-step="3" data-type="right" data-slider="move-left-right"><i class="ti-angle-right"></i><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="pane-rotate" role="tabpanel" aria-labelledby="rotate-tab">
                          <div class="px-4 pt-3">
                            <div class="row mb-3 justify-content-end">
                              <div class="col-6 text-right">
                                <button class="btn-reset-color" data-type="rotate"><img src="$iconResetUrl" alt="" class="mr-2">
                                  Reset all</button>
                              </div>
                            </div>
                            <div class="row no-gutters">
                            <div class="col canvas-wrapper">
                            <div class="border preview-canvas-parent">
                              </div>

</div>
                              <div class="mt-4 row col justify-content-center no-gutters">
                              <div class="col-8">
                                <input id="tool-rotate" data-slider-id="tool-rotate" type="text" class="slider-range" />
                                <div class="row justify-content-center">
                                  <div class="col-12">
                                    <div class="d-flex justify-content-between align-items-center mt-3">
                                      <button class="btn-move-step" data-step="15" data-type="rotate-left" data-slider="tool-rotate"><i class="ti-angle-left"></i><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                      <button class="btn-move-step" data-step="5" data-type="rotate-left" data-slider="tool-rotate"><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                      <button class="btn-move-step" data-step="1" data-type="rotate-left" data-slider="tool-rotate"><i class="ti-angle-left"></i></button>
                                      <input type="text" autocomplete="off" id="tool-rotate-value" class="input-move-step border">
                                      <button class="btn-move-step" data-step="1" data-type="rotate-right" data-slider="tool-rotate"><i class="ti-angle-right"></i></button>
                                      <button class="btn-move-step" data-step="5" data-type="rotate-right" data-slider="tool-rotate"><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                      <button class="btn-move-step" data-step="15" data-type="rotate-right" data-slider="tool-rotate"><i class="ti-angle-right"></i><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                    </div>
                                  </div>
                                </div>


                              </div>
                            </div>
                            </div>

                          </div>
                        </div>
                        <div class="tab-pane fade" id="pane-scale" role="tabpanel" aria-labelledby="scale-tab">
                          <div class="px-4 pt-3">
                            <div class="row mb-3 justify-content-end">
                              <div class="col-6 text-right">
                                <button class="btn-reset-color" data-type="scale"><img src="$iconResetUrl" alt="" class="mr-2">
                                $resetAllLabel</button>
                              </div>
                            </div>
                            <div class="row no-gutters">
                            <div class="col canvas-wrapper">
                            <div class="border preview-canvas-parent">
                              </div>

</div>
                              <div class="col-auto pl-4 col-slider-ver">
                                <div class="d-flex">
                                  <input id="scale-up-down" data-slider-id="scale-up-down" type="text" class="slider-range" />
                                  <div class="d-flex flex-column justify-content-between align-items-center pl-3">
                                    <button class="btn-move-step rotate-90" data-step="3" data-type="up" data-slider="scale-up-down"><i class="ti-angle-left"></i><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="2" data-type="up" data-slider="scale-up-down"><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="1" data-type="up" data-slider="scale-up-down"><i class="ti-angle-left"></i></button>
                                    <input type="text" autocomplete="off" id="scale-up-down-value" class="input-move-step border">
                                    <button class="btn-move-step rotate-90" data-step="1" data-type="down" data-slider="scale-up-down"><i class="ti-angle-right"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="2" data-type="down" data-slider="scale-up-down"><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="3" data-type="down" data-slider="scale-up-down"><i class="ti-angle-right"></i><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                  </div>
                                </div>
                              </div>

                            </div>
                            <div class="mt-4 row no-gutters">
                              <div class="col-9">
                                <input id="scale-left-right" data-slider-id="scale-left-right" type="text" class="slider-range" />
                                <div class="d-flex justify-content-between align-items-center mt-3">
                                  <button class="btn-move-step" data-step="3" data-type="left" data-slider="scale-left-right"><i class="ti-angle-left"></i><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                  <button class="btn-move-step" data-step="2" data-type="left" data-slider="scale-left-right"><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                  <button class="btn-move-step" data-step="1" data-type="left" data-slider="scale-left-right"><i class="ti-angle-left"></i></button>
                                  <input type="text" autocomplete="off" id="scale-left-right-value" class="input-move-step border">
                                  <button class="btn-move-step" data-step="1" data-type="right" data-slider="scale-left-right"><i class="ti-angle-right"></i></button>
                                  <button class="btn-move-step" data-step="2" data-type="right" data-slider="scale-left-right"><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                  <button class="btn-move-step" data-step="3" data-type="right" data-slider="scale-left-right"><i class="ti-angle-right"></i><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="pane-skew" role="tabpanel" aria-labelledby="skew-tab">
                          <div class="px-4 pt-3">
                            <div class="row mb-3 justify-content-end">
                              <div class="col-6 text-right">
                                <button class="btn-reset-color" data-type="skew"><img src="$iconResetUrl" alt="" class="mr-2">
                                  $resetAllLabel</button>
                              </div>
                            </div>
                            <div class="row no-gutters">
                            <div class="col canvas-wrapper">
                            <div class="border preview-canvas-parent">
                              </div>

</div>

                              <div class="col-auto pl-4 col-slider-ver">
                                <div class="d-flex">
                                  <input id="skew-up-down" data-slider-id="skew-up-down" type="text" class="slider-range" />
                                  <div class="d-flex flex-column justify-content-between align-items-center pl-3">
                                    <button class="btn-move-step rotate-90" data-step="3" data-type="up" data-slider="skew-up-down"><i class="ti-angle-left"></i><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="2" data-type="up" data-slider="skew-up-down"><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="1" data-type="up" data-slider="skew-up-down"><i class="ti-angle-left"></i></button>
                                    <input type="text" autocomplete="off" id="skew-up-down-value" class="input-move-step border">
                                    <button class="btn-move-step rotate-90" data-step="1" data-type="down" data-slider="skew-up-down"><i class="ti-angle-right"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="2" data-type="down" data-slider="skew-up-down"><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                    <button class="btn-move-step rotate-90" data-step="3" data-type="down" data-slider="skew-up-down"><i class="ti-angle-right"></i><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="mt-4 row no-gutters">
                              <div class="col-9">
                                <input id="skew-left-right" data-slider-id="skew-left-right" type="text" class="slider-range" />
                                <div class="d-flex justify-content-between align-items-center mt-3">
                                  <button class="btn-move-step" data-step="3" data-type="left" data-slider="skew-left-right"><i class="ti-angle-left"></i><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                  <button class="btn-move-step" data-step="2" data-type="left" data-slider="skew-left-right"><i class="ti-angle-left"></i><i class="ti-angle-left"></i></button>
                                  <button class="btn-move-step" data-step="1" data-type="left" data-slider="skew-left-right"><i class="ti-angle-left"></i></button>
                                  <input type="text" autocomplete="off" id="skew-left-right-value" class="input-move-step border">
                                  <button class="btn-move-step" data-step="1" data-type="right" data-slider="skew-left-right"><i class="ti-angle-right"></i></button>
                                  <button class="btn-move-step" data-step="2" data-type="right" data-slider="skew-left-right"><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                  <button class="btn-move-step" data-step="3" data-type="right" data-slider="skew-left-right"><i class="ti-angle-right"></i><i class="ti-angle-right"></i><i class="ti-angle-right"></i></button>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tool-bg-layer" id="tool-bg-layer">
                      <ul class="nav nav-tabs tabs-tools justify-content-between" style="background-color: #ECECEC;">
                        <!-- <li class="nav-item flex-grow-0"> -->
                        <li class="nav-item">
                          <a class="nav-link active" data-tab="pane-bg-color"><span class="tab-color-current tab-color-background mr-2" style="background-color: #5F707F;"></span>
                            $backgroundColorLabel</a>
                        </li>
                      </ul>
                      <div class="tab-content" id="myBgTabContent">
                        <div class="tab-pane fade show active" id="pane-bg-color" role="tabpanel" aria-labelledby="color-tab">
                          <div class="color-box px-4 pt-3" id="background-color-box">
                            <div class="row tool-header">
                              <div class="col-6">
                                $colorsInUserLabel
                              </div>
                              <div class="col-6 text-right">
                                <button class="btn-reset-color"><img src="$iconResetUrl" alt="" class="mr-2">
                                  $resetAllLabel</button>
                              </div>
                            </div>
                            <div class="row mb-2" id="recent-bg-color-list">
                              <div class="col-auto d-flex align-items-center hide">
                                <button class="btn-color-picker p-2"><img src="$iconColorPickerUrl" alt=""></button>
                              </div>
                              <!-- Recently selected color -->
                              <!-- Recently selected color -->
                            </div>
                            <!-- List of color -->
                            $colorSelectionListHtml
                            <!-- List of color -->
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row hide">
                <div class="col-auto">
                  <div class="text-center mt-3 btn-visualiser d-inline-flex justify-content-center">
                    <a id="shareBtn" href="javascript:void(0);" class="link transparent " data-toggle="modal" data-target="#toolShareModalCenter"><i class="mr-2"><img src="$iconShare" width="20" height="20" alt="icon share"></i>
                      $shareLabel</a>
                    <a href="#" class="link blue-style" data-toggle="modal" data-target="#toolVisualizarModalCenter"><i class="mr-2">
                      <!-- <img src="$iconVisualize" alt=""> -->
                      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.9909 5.25C8.22218 5.25 5.25031 7.62188 1.69718 11.4984C1.43937 11.7844 1.43468 12.2109 1.6925 12.4969C4.73468 15.8578 7.41593 18.75 11.9909 18.75C16.5097 18.75 19.8987 15.1078 22.3081 12.4688C22.5566 12.1969 22.5706 11.7844 22.3316 11.4984C19.8753 8.52188 16.4769 5.25 11.9909 5.25ZM12.1972 16.2141C9.71281 16.3266 7.66906 14.2828 7.78625 11.8031C7.88468 9.6375 9.63781 7.88438 11.8034 7.78594C14.2878 7.67344 16.3316 9.71719 16.2144 12.1969C16.1159 14.3625 14.3628 16.1156 12.1972 16.2141Z" fill="white"/>
                        <path d="M11.9997 9.79688C11.9997 9.51563 12.0513 9.24844 12.145 9.00469C12.0981 9.00469 12.0513 9 11.9997 9C10.27 9 8.87782 10.4719 9.00907 12.2297C9.12157 13.6969 10.3028 14.8781 11.77 14.9906C13.5278 15.1219 14.9997 13.7297 14.9997 12C14.9997 11.9391 14.995 11.8781 14.995 11.8172C14.7325 11.9344 14.4466 12 14.1419 12C12.9606 12 11.9997 11.0109 11.9997 9.79688Z" fill="white"/>
                      </svg>
                    </i>
                      $visualizeLabel</a>
                    <a id="saveBtn" href="javascript:void(0);" onclick="javascript:onSaveDesign(this);" class="link transparent"><i class="mr-2"><img src="$iconSave" width="20" height="20" alt="icon save"></i>
                      $saveLabel</a>
                  </div>
                </div>
                <div class="col-auto">
                  <div class="text-center mt-3 btn-visualiser d-inline-flex justify-content-center">
                    <input id="link" class="hide" type="text" readonly value="link" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- MANAGER TOOL -->
          <br/>
          <br/>
          <div id="managerSection" class="hide container">
                <div class="row" id="manageDesignType">
                    <label>$productDesignTypeLabel</label>
                    <div class="col-12">$m_productTypeList</div>
                </div>
                <div class="row" id="manageManu">
                    <label>$manuListLabel</label>
                    <div class="col-12">$m_manuList</div>
                </div>
                <div class="row" id="manageCat">
                    <label>$cateListLabel</label>
                    <div class="col-12">$m_categoryList</div>
                </div>
                <div class="row" id="manageShape">
                    <label>$shapeListLabel</label>
                    <div class="col-12" id="shapeList"></div>
                </div>
                <div class="row" id="manageSize">
                    <label>$sizeListLabel</label>
                    <div class="col-12" id="sizeList"></div>
                </div>
                <div class="row" id="manageProductTitle">
                    <label>$productTitleLabel</label>
                    <div class="col-12">
                        <input type="text" autocomplete="off" id="productTitle" class="form-control" placeholder="Product title" value="$productTitle">
                    </div>
                </div>
                <div class="row" id="manageThickness"></div>
                <div class="row $hideIfNotFixed" id="managePrice">
                    <label>$fixedPriceManagerBlockLabel</label>
                    <div class="col-12">$m_priceAreaPairs</div>
                </div>
                <div class="row" id="manageProductTitle">
                    <label>$deliveryLabel</label>
                    <div class="col-12">
                        <input type="text" autocomplete="off" id="delivery" class="form-control" placeholder="Delivery" value="$deliveryInfo">
                    </div>
                </div>
                <div class="row" id="manageProductTitle">
                    <label>$packagingLabel</label>
                    <div class="col-12">
                        <input type="text" autocomplete="off" id="packaging" class="form-control" placeholder="Packaging" value="$packagingInfo">
                    </div>
                </div>
                <div class="row" id="manageProductTitle">
                    <label>$subtextLabel 1</label>
                    <div class="col-12">
                        <input type="text" autocomplete="off" id="subtext-1" class="form-control" placeholder="Subtext 1" value="$subText_1">
                    </div>
                </div>
                <div class="row" id="manageProductTitle">
                    <label>$subtextLabel 2</label>
                    <div class="col-12">
                        <input type="text" autocomplete="off" id="subtext-2" class="form-control" placeholder="Subtext 2" value="$subText_2">
                    </div>
                </div>
                <div class="row" id="manageProductTitle">
                    <label>$subtextLabel 3</label>
                    <div class="col-12">
                        <input type="text" autocomplete="off" id="subtext-3" class="form-control" placeholder="Subtext 3" value="$subText_3">
                    </div>
                </div>
                <div class="row" id="actionBtn">
                    <label>$actionLabel</label>
                    <div class="col-12">
                        <button class="manageBtn link grey $hideIfNotAdminAndNotDefaultProductOrInShareMode" value="create" onclick="upsertProduct(this)">$createLabel</button>
                        <button class="manageBtn link" onclick="upsertProduct(this)" value="update">$updateLabel</button>
                    </div>
                </div>
            </div>
            <!-- MANAGER TOOL -->
        </div>
      </section>
    </main>

    <footer class="footer-design">
      <div class="footer-design-wrap border-top" id="footer-design-wrap">
        <div class="container-fluid p-0">
          <div class="row">
            <div class="col-12 col-lg col-xl d-flex">
              <div class="w-100">
                <div class="row h-100 justify-content-between">
                  <div class="col-auto col-sm-6 col-lg-auto col-xl-auto d-flex flex-column justify-content-center px-3 pl-4 border-bottom-mobile f-block-height">
                  <p class="text-welcome m-0">Need some help? Please read our&nbsp;<a style="margin-top:2px" target="_blank" class="blue" href="https://www.tiles.design/faq/design-tool">FAQ!</a></p>
                  </div>
                  <!-- <div id="pricingBlock" class="hide col-auto col-sm-6 col-lg-auto col-xl-auto d-flex flex-column justify-content-center border-right px-3 pl-4 border-bottom-mobile f-block-height">
                    <div class="text-right text-box"><strong class="total-text">$totalLabel</strong>
                      <strong class="text-wrapper">
                        <strong class="blue-text calculatedPrice" id="calculatedPrice">$6000,00</strong>
                          <sup>
                            <button class="no-style btn-tooltip-info" data-toggle="tooltip" data-html="true" id="pricingDetailTooltip" title='<span class="grey">Price per m2</span> <b id="pricePerTile">$80</b> <br><span class="grey" id="tilesPerM2">Tiles per m2</span> <b>25</b> <br><span class="grey" id="tilesPerBox">Tiles per box</span> <b>12</b>'></button>
                        </sup>
                      </strong>
                    </div>
                    <input type="hidden" id="piecesPerM2" value="" />
                    <input type="hidden" id="pricePerM2" value="" />
                    <input type="hidden" id="tilesPerBox" value="" />
                    <input type="hidden" id="totalBoxes" value="" />
                    <input type="hidden" id="totalPieces" value="" />
                    <input type="hidden" id="totalPalletes" value="" />
                    <div class="text-right text-label text-box"><span class="first-text">$boxesLabel</span><strong class="blue-text" id="totalBoxesLabel">80</strong></div>
                    <div class="text-right text-label text-box"><span class="first-text">$tilesUpperLabel</span><strong class="blue-text" id="totalTilesLabel">2333</strong></div>
                  </div> -->
                  <div id="sizeBlock" class="hide col col-sm-6 col-lg-auto col-xl-auto d-flex flex-column justify-content-center pr-4 border-bottom-mobile f-block-height dimensions-block">
                    <label class="m-0 float-right">Dimensions</label>
                    <div class="select-dimensions $hideArrowSizeList">
                      <select name="size-selection" id="size-selection" class="float-right" onchange="onChangeSizeSelection(this)">
                        $sizeSelectionListHtml
                      </select>
                    </div>
                  </div>
                  <!-- <div class="hide col-12 col-md col-lg col-xl d-flex align-items-center px-5 f-block-height slider-range-wrapper">
                    <div class="w-100" id="slider-range-m-wrap">
                      <div class="tooltips">
                        $priceNodeHtml
                      </div>
                      <input id="slider-range-m" type="text" /><span class="slider-range-m-unit">m<sup>2</sup></span>
                      <input class="priceSlider" id="priceRangeControl" type="number" step="0.01" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 13" data-slider-id="priceRangeSlider"/>
                      <span class="slider-range-m-unit">m<sup>2</sup></span>
                    </div>
                  </div> -->
                  <!-- <div class="col-12 col-md-auto col-lg-auto col-xl-auto f-block-height">
                    <div class="container-fluid p-0">
                      <div class="row">
                        <p class="text-welcome m-0">$letsPlayWithDesignLabel &nbsp;<a id="blankTileLink" class="blue" href="$blankTileUrl">$startProductTourLabel</a></p>
                      </div>
                    </div>
                  </div> -->
                  <div class="col-12 col-md-auto col-lg-auto col-xl-auto f-block-height $addNewBtnGroupHide" id="addNewBtnGroup">
                    <div class="container-fluid p-0">
                      <div class="row" style="float: right;">
                        <!-- <div class="col-12 col-sm-auto col-md-auto d-flex justify-content-start align-items-center border-right f-block-height">
                          <div class="px-3">
                            $infoLabel
                          </div>
                        </div> -->
                        <!-- <div class="col-12 col-sm col-md d-flex align-items-center justify-content-end px-4 px-lg-5 f-block-height f-block-height-2">
                          <table class="w-100 tbl-cart-info-detail">
                            <thead>
                              <tr>
                                <th class="pr-1">Type</th>
                                <th class="pr-1">Delivery time</th>
                                <th>$minimumOrderLabel</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="pr-1"><span class="black">Cement tile</span><br>$subText_1</td>
                                <td class="pr-1"><span class="black">$deliveryInfo</span><br>$subText_2</td>
                                <td><span class="black">{$minimumOrder}m<sup>2</sup></span><br>$subText_3</td>
                              </tr>
                            </tbody>
                          </table>
                        </div> -->
                        <div class="col-12 col-md-auto f-block-height" style="padding: 0 !important;">
                          <!-- <a href="javascript:;" class="link transparent large btn-next-step w-100" data-dismiss="modal" aria-label="Close">continueToEditLabel</a> -->
                          <a href="#" class="link large  btn-next-step w-100 next-step-blue" style="border-top: unset;" data-toggle="modal" data-target="#toolVisualizarModalCenter">
                             <img alt="" src="https://app.tiles.design/media/uploads/assets/visualize.png" class="mr-2">
                               $visualizeLabel
                            </a>
                        </div>
                        <div class="col-12 col-md-auto f-block-height" style="padding: 0 !important;">
                          <a href="javascript:;" onclick="saveDesignV2(this);" class="link grey-style-next large btn-next-step w-100">
                           <img alt="" src="https://app.tiles.design/media/uploads/assets/cart.png" class="mr-2">
                            $saveDesignLabel</a>
                        </div>
                      </div>
                    <!-- <div> -->
                    <!-- <a href="#" class="link transparent large btn-next-step-checkout w-100" style="padding-left: 12px !important;" data-toggle="modal" data-target="#toolVisualizarModalCenter">
                      $visualizeLabel</a> -->
                      <!-- <a href="javascript:void(0);" onclick="saveDesignV2(this);" style="padding-left: 60px !important;" class="link blue-style large btn-next-step-checkout w-100">
                        <div> -->
                          <!-- <img src="$cartImg" alt="" class="mr-3">  -->
                          <!-- <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.52077 20.9992C8.14197 20.9877 8.63626 20.4749 8.6248 19.8537C8.61333 19.2325 8.10046 18.7382 7.47927 18.7496C6.85807 18.7611 6.36378 19.274 6.37524 19.8952C6.38671 20.5164 6.89958 21.0106 7.52077 20.9992Z" fill="#fff"/>
                            <path d="M19.1492 19.9017C19.164 19.2805 18.6724 18.765 18.0513 18.7502C17.4302 18.7354 16.9146 19.2269 16.8998 19.8481C16.885 20.4692 17.3766 20.9847 17.9977 20.9995C18.6188 21.0143 19.1344 20.5228 19.1492 19.9017Z" fill="#fff"/>
                            <path d="M21.7403 6.19687C21.7075 6.08437 21.6091 6.00937 21.4966 5.99999L6.22939 4.52343C6.09814 4.50937 5.93877 4.42499 5.87783 4.30312C5.69971 3.9703 5.58721 3.7828 5.30596 3.43124C4.94502 2.99062 4.26533 3.00468 3.01846 2.9953C2.59658 2.99062 2.25439 3.23905 2.25439 3.65624C2.25439 4.06405 2.57783 4.31718 2.98564 4.31718C3.39346 4.31718 3.98408 4.34062 4.20439 4.40624C4.42471 4.47187 4.60283 4.8328 4.66846 5.14687C4.66846 5.15155 4.66846 5.15624 4.67314 5.16093C4.68252 5.21718 4.76689 5.63905 4.76689 5.64374L6.64189 15.5625C6.75439 16.2422 6.98408 16.8047 7.32158 17.2359C7.71533 17.7422 8.23564 17.9953 8.86377 17.9953H19.9544C20.3106 17.9953 20.6153 17.7234 20.6294 17.3672C20.6481 16.9922 20.3481 16.6828 19.9731 16.6828H8.85439C8.76064 16.6828 8.62471 16.6828 8.46533 16.5516C8.30127 16.4109 8.07627 16.0875 7.92627 15.3328L7.72471 14.2219C7.72471 14.2078 7.72939 14.1984 7.74346 14.1937L20.7606 11.9906C20.8825 11.9719 20.9763 11.8734 20.9903 11.7469L21.7403 6.31874C21.7497 6.28124 21.7497 6.23905 21.7403 6.19687Z" fill="#fff"/>
                          </svg> -->
                          <!-- $saveDesignLabel
                        </div> -->
                        <!-- <div class="total-text hide">
                          <strong>$totalSmallLabel</strong> <strong class="white-text calculatedPrice" id="calculatedPrice">$6000,00</strong>
                        </div>
                      </a> -->
                      <!-- <a href="javascript:void(0);" onclick="$editItemBtnFunction" class="link blue-style large btn-next-step-checkout w-100 hide">
                        <div>
                          <img src="$cartImg" alt="" class="mr-3"> $editItemLabel
                        </div>
                      </a> -->
                    <!-- </div> -->
                  </div>
                  <!-- EDIT TILE BUTTON GROUP -->
                  <div class="col-12 col-md-auto col-lg-auto col-xl-auto f-block-height btn-group-edit-tile $editBtnGroupHide" id="editBtnGroup">
                    <div class="btn-group-edit-tile-inner d-flex">
                      <a href="javascript:void(0);" onclick="saveDesignV2(this);" class="link blue-style large btn-next-step-checkout w-100">
                        <div>
                          <!-- <img src="$cartImg" alt="" class="mr-3">  -->
                          <svg width="24" height="24" class="mr-3" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.52077 20.9992C8.14197 20.9877 8.63626 20.4749 8.6248 19.8537C8.61333 19.2325 8.10046 18.7382 7.47927 18.7496C6.85807 18.7611 6.36378 19.274 6.37524 19.8952C6.38671 20.5164 6.89958 21.0106 7.52077 20.9992Z" fill="#fff"/>
                            <path d="M19.1492 19.9017C19.164 19.2805 18.6724 18.765 18.0513 18.7502C17.4302 18.7354 16.9146 19.2269 16.8998 19.8481C16.885 20.4692 17.3766 20.9847 17.9977 20.9995C18.6188 21.0143 19.1344 20.5228 19.1492 19.9017Z" fill="#fff"/>
                            <path d="M21.7403 6.19687C21.7075 6.08437 21.6091 6.00937 21.4966 5.99999L6.22939 4.52343C6.09814 4.50937 5.93877 4.42499 5.87783 4.30312C5.69971 3.9703 5.58721 3.7828 5.30596 3.43124C4.94502 2.99062 4.26533 3.00468 3.01846 2.9953C2.59658 2.99062 2.25439 3.23905 2.25439 3.65624C2.25439 4.06405 2.57783 4.31718 2.98564 4.31718C3.39346 4.31718 3.98408 4.34062 4.20439 4.40624C4.42471 4.47187 4.60283 4.8328 4.66846 5.14687C4.66846 5.15155 4.66846 5.15624 4.67314 5.16093C4.68252 5.21718 4.76689 5.63905 4.76689 5.64374L6.64189 15.5625C6.75439 16.2422 6.98408 16.8047 7.32158 17.2359C7.71533 17.7422 8.23564 17.9953 8.86377 17.9953H19.9544C20.3106 17.9953 20.6153 17.7234 20.6294 17.3672C20.6481 16.9922 20.3481 16.6828 19.9731 16.6828H8.85439C8.76064 16.6828 8.62471 16.6828 8.46533 16.5516C8.30127 16.4109 8.07627 16.0875 7.92627 15.3328L7.72471 14.2219C7.72471 14.2078 7.72939 14.1984 7.74346 14.1937L20.7606 11.9906C20.8825 11.9719 20.9763 11.8734 20.9903 11.7469L21.7403 6.31874C21.7497 6.28124 21.7497 6.23905 21.7403 6.19687Z" fill="#fff"/>
                          </svg>
                          $addAsNewLabel
                        </div>
                      </a>
                      <a href="javascript:void(0);" onclick="$editItemBtnFunction" class="link grey-style large btn-next-step-checkout w-100">
                        <div>
                          <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M15.375 7.03438C15.2937 6.95312 15.1875 6.90938 15.075 6.90938C14.9625 6.90938 14.8562 6.95312 14.775 7.03438L14.3687 7.44063C14.5031 5.50938 13.7593 3.6125 12.3281 2.28437C11.1531 1.19375 9.61558 0.59375 8.00308 0.59375C7.1687 0.59375 6.35308 0.753125 5.58433 1.06562C4.78745 1.39062 4.07495 1.86563 3.4687 2.48125C3.0562 2.9 2.69995 3.37187 2.4187 3.88125C2.35933 3.9875 2.34683 4.1125 2.3812 4.22813C2.41558 4.34375 2.49995 4.44063 2.6062 4.49375C2.6687 4.525 2.73745 4.54063 2.8062 4.54063C2.9687 4.54063 3.11558 4.45312 3.1937 4.3125C3.44058 3.86562 3.74995 3.45312 4.11245 3.09062C4.63745 2.56562 5.24683 2.15937 5.92808 1.88125C6.58745 1.6125 7.28433 1.47813 7.99683 1.47813C8.71245 1.47813 9.40933 1.6125 10.0687 1.88125C10.75 2.15937 11.3625 2.56562 11.8875 3.09375C12.5218 3.73125 12.9937 4.51875 13.2562 5.375C13.475 6.09062 13.5437 6.85313 13.4625 7.6L12.8718 7.02188C12.7937 6.94688 12.6906 6.90312 12.5812 6.90312C12.4718 6.90312 12.3656 6.94687 12.2875 7.025C12.125 7.1875 12.125 7.45 12.2875 7.6125L13.625 8.95312C13.6843 9.0125 13.7625 9.04375 13.8437 9.04375C13.925 9.04375 14.0031 9.0125 14.0625 8.95312L15.3781 7.63437C15.5406 7.46875 15.5406 7.2 15.375 7.03438ZM13.3937 9.49687C13.3312 9.46562 13.2625 9.45 13.1937 9.45C13.0312 9.45 12.8843 9.5375 12.8062 9.67813C12.5593 10.125 12.25 10.5375 11.8875 10.9C11.3625 11.425 10.7531 11.8344 10.0687 12.1094C9.40933 12.3781 8.71245 12.5125 7.99683 12.5125C7.2812 12.5125 6.58433 12.3781 5.92808 12.1094C5.24683 11.8313 4.63433 11.425 4.10933 10.9C2.93433 9.71875 2.35933 8.05937 2.53745 6.40312L3.11245 6.98125C3.18745 7.05625 3.28745 7.09688 3.3937 7.09688C3.49995 7.09688 3.59995 7.05625 3.67495 6.98125L3.71558 6.94063C3.8687 6.7875 3.8687 6.53437 3.71558 6.38125L2.37495 5.0375C2.31558 4.97813 2.23745 4.94688 2.1562 4.94688C2.07495 4.94688 1.99683 4.97813 1.93745 5.0375L0.624951 6.35938C0.459326 6.525 0.459326 6.79375 0.624951 6.95937C0.706201 7.04063 0.812451 7.08438 0.924951 7.08438C1.03745 7.08438 1.1437 7.04063 1.22495 6.95937L1.6312 6.55C1.57495 7.35625 1.67183 8.175 1.91245 8.94063C2.22183 9.92188 2.74995 10.7937 3.48433 11.5281C4.0937 12.1406 4.80308 12.6125 5.59683 12.9375C6.36245 13.25 7.17183 13.4062 7.99995 13.4062C8.82808 13.4062 9.63745 13.2469 10.4031 12.9375C11.1968 12.6156 11.9062 12.1406 12.5156 11.5281C12.9375 11.1062 13.2968 10.6281 13.5843 10.1094C13.6437 10.0031 13.6562 9.87813 13.6218 9.7625C13.5843 9.64687 13.5031 9.55 13.3937 9.49687Z" fill="white"/>
                            </svg>
                            $updateBasketTile
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- <canvas class="hide" id="hiddenCanvas"></canvas> -->
    <img class="hide" id="hiddenImage" />
    <img id="shareImage" class="hide" />
    <canvas id="hidden-canvas" class="hide"></canvas>
    <canvas id="share-canvas" class="hide"></canvas>
    <canvas id="share-canvas-2" class="hide"></canvas>

  </div>
  <!-- Tool Add Element Modal -->
  <div class="modal fade login-modal" id="toolAddElementModalCenter" tabindex="-1" role="dialog" aria-labelledby="toolAddElementModalCenterTitle" aria-hidden="true" style="overflow-y: hidden !important;">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width: 1000px; max-height: 605px;margin-top: -5px;">
    <div class="modal-content">
        <div class="modal-header justify-content-center border-0">
          <h5 class="modal-title pt-3" id="toolAddElementModalCenterTitle">$addNewElementLabel</h5>
          <button type="button" class="close clicktoclose hide" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body p-0">
          <div class="text-center px-3 px-md-5">
            $layerTags
          </div>
          <div class="text-center px-3 px-md-5 mt-3" style="height: 350px;">
            <div class="px-0 px-md-5">
              <input type="hidden" id="totalLayers" />
              <div class="position-relative" id="swiper-new-arrivals">
                <div id="swiper-add-element" class="swiper-container" data-slides-perview="9" data-slides-mobile="1" data-slides-tablet="1" data-slides-pergroup="1" data-slides-percolumn="1" data-space-between="10" data-next="#swiper-button-next-element" data-prev="#swiper-button-prev-element" data-pagination="#swiper-pagination-element">
                  <div class="swiper-wrapper">
                    <!-- Load More Layers -->
                  </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-prev" id="swiper-button-prev-element"> <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M6.80912 7.99995L10.781 4.0312C11.0747 3.73745 11.0747 3.26245 10.781 2.97183C10.4872 2.67808 10.0122 2.6812 9.71849 2.97183L5.21849 7.4687C4.93412 7.75307 4.92787 8.20932 5.19662 8.50307L9.71536 13.0312C9.86224 13.1781 10.056 13.2499 10.2466 13.2499C10.4372 13.2499 10.631 13.1781 10.7779 13.0312C11.0716 12.7374 11.0716 12.2624 10.7779 11.9718L6.80912 7.99995Z" fill="#A1AAB6"/> </svg> </div>
                <div class="swiper-button-next" id="swiper-button-next-element" onclick="onLoadMoreLayerNextSlide(this)" data-next-page="5" data-end-list="0"> <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M9.85722 8L5.88535 4.03125C5.5916 3.7375 5.5916 3.2625 5.88535 2.97188C6.1791 2.68125 6.6541 2.68125 6.94785 2.97188L11.4478 7.46875C11.7322 7.75313 11.7385 8.20938 11.4697 8.50313L6.95097 13.0312C6.8041 13.1781 6.61035 13.25 6.41973 13.25C6.2291 13.25 6.03535 13.1781 5.88848 13.0312C5.59473 12.7375 5.59473 12.2625 5.88848 11.9719L9.85722 8Z" fill="#A1AAB6"/> </svg> </div>
              </div>
              <!-- Add Pagination -->
              <div class="swiper-pagination mt-3 mb-2 position-relative" id="swiper-pagination-element"></div>
            </div>
          </div>
          <footer class="footer-design m-0">
            <div class="footer-design-wrap position-static border-top">
              <div class="container-fluid p-0">
                <div class="row">
                  <div class="col d-flex blue-text align-items-center pl-4 pr-0 px-md-5 f-block-height">
                    <div>$addNewElementLabel</div>
                  </div>
                  <div class="col-auto f-block-height">
                    <button type="button" data-dismiss="modal" aria-label="Cancel" id="btnCancelAddMoreLayer" style="border-top: unset;" class="link transparent large btn-next-step w-100">Cancel</button>
                  </div>
                  <div class="col-auto f-block-height">
                    <button id="btnAddMoreLayer" type="button" disabled class="nohover link link-hover large btn-next-step">ADD ELEMENT</button>
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>
  </div>

  <!-- Tool visualizar modal -->
  <div class="modal fade login-modal" id="toolVisualizarModalCenter" tabindex="-1" role="dialog" aria-labelledby="toolVisualizarModalCenterTitle" aria-hidden="true" style="overflow-y: hidden !important;">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width: 1000px; max-height: 605px;margin-top: -5px;">
      <div class="modal-content">
        <div class="modal-header justify-content-center border-0">
          <h5 class="modal-title pt-3" id="toolVisualizarModalCenterTitle">$tileVisualizerLabel</h5>
          <!-- <button type="button" class="btn-bookmark"></button> -->
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="border-bottom pb-2">
          <div class="row">
            <div class="col-auto">
              <div class="tiles-result-img border rounded p-1 ml-3 mr-lg-5">
                <img src="$iconTileResult" alt="tiles" id="visualizerImageDemo">
              </div>
            </div>
            <div class="col col-md-auto">
              <div class="row">
                <div class="col-sm-4 mt-3 mt-sm-0 select-group">
                  <label>Choose pattern</label>
                  <div class="d-flex choose-pattern-dropdown-popup $hideArrowFragmentList">
                    <div class="mr-2 d-flex align-items-center"><img src="$patternIcon" alt="" class="banner-select-icon" id="visualizerFragmentIconSelection"></div>
                    <!-- <label class="label-text">Pattern</label> -->
                    <select name="" id="dropdownPattern-list-popup" class="nice-select" onchange="onChangeFragmentVisualizer(this);">
                      $visualizerFragmentHtml
                    </select>
                  </div>
                </div>
                <div class="col-sm-4 mt-3 mt-sm-0 select-group">
                  <label>$tileSizeLabel</label>
                  <div class="d-flex">
                    <div class="mr-2 d-flex align-items-center $hideArrowSizeList"><img src="$iconSize" alt="" class="banner-select-icon icon-14px"></div>
                    <select name="" id="sizeVisualizerSelect" class="nice-select" onchange="onChangeSizeVisualizer(this);">
                      $sizeSelectionListHtml
                    </select>
                  </div>
                </div>
                <div class="col-sm-4 mt-3 mt-sm-0 select-group">
                  <label>$roomDemoLabel</label>
                  <div class="d-flex">
                    <div class="mr-2 d-flex align-items-center"><img src="$iconEye" alt="" class="banner-select-icon"></div>
                    <select name="" id="bgLayerVisualizerSelect" class="nice-select" onchange="onChangeBgLayerVisualizer(this);">
                      $visualizerBgLayerHtml
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-12 col-md d-flex align-items-end justify-content-end">
              <label class="mr-3 mt-3 mt-md-0 d-flex align-items-center hide">
                <input type="checkbox" autocomplete="off" onclick="onClickVisualizer3DViewBtn(this);" class="d-one">
                <div class="check-slider"></div><span class="ml-1">3D View</span>
              </label>
              <button class="hide mr-3 mt-3 mt-md-0" data-toggle="modal" data-dismiss="modal" data-target="#toolShareModalCenter" id="visualizerShareBtn">
                <svg class="mr-2" width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.45654 5.64062C8.30615 5.64062 8.17725 5.75585 8.16553 5.90624C8.03076 7.52929 6.66553 8.79491 5.00537 8.79491C3.25732 8.79491 1.8335 7.38085 1.8335 5.64257C1.8335 3.93163 3.2124 2.53319 4.92334 2.49022C4.96826 2.48827 5.00342 2.52343 5.00342 2.56835V3.55077C5.00342 3.79687 5.2749 3.9453 5.48389 3.81444L7.36279 2.49999C7.5581 2.37694 7.5581 2.09374 7.36279 1.97069L5.48584 0.673819C5.27685 0.542959 5.00537 0.691397 5.00537 0.937491V1.83007C5.00537 1.87304 4.97217 1.90819 4.9292 1.90819C2.89014 1.94921 1.24951 3.60546 1.24951 5.64257C1.24951 7.70507 2.93115 9.37499 5.00537 9.37499C6.97412 9.37499 8.5874 7.87694 8.74756 5.95507C8.76318 5.78515 8.62646 5.64062 8.45654 5.64062V5.64062Z" fill="#4F94F5"/>
                  </svg>
                $shareLabel</button>
            </div>
          </div>
        </div>
        <div class="modal-body p-0">
          <div class="visual-pattern-overlay">
            <div class="img" id="fragmentContent">
            </div>
            <img src="$noi_that_over_lay" alt="" class="w-100 visual-overlay" style="height: 405px;">
          </div>
          <footer class="footer-design m-0">
            <div class="footer-design-wrap position-static border-top">
              <div class="container-fluid p-0">
                <div class="row" style="float: right;">
                  <!-- <div class="col-12 col-sm-auto col-md-auto d-flex justify-content-start align-items-center border-right f-block-height">
                    <div class="px-3">
                      $infoLabel
                    </div>
                  </div> -->
                  <!-- <div class="col-12 col-sm col-md d-flex align-items-center justify-content-end px-4 px-lg-5 f-block-height f-block-height-2">
                    <table class="w-100 tbl-cart-info-detail">
                      <thead>
                        <tr>
                          <th class="pr-1">Type</th>
                          <th class="pr-1">Delivery time</th>
                          <th>$minimumOrderLabel</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="pr-1"><span class="black">Cement tile</span><br>$subText_1</td>
                          <td class="pr-1"><span class="black">$deliveryInfo</span><br>$subText_2</td>
                          <td><span class="black">{$minimumOrder}m<sup>2</sup></span><br>$subText_3</td>
                        </tr>
                      </tbody>
                    </table>
                  </div> -->
                  <div class="col-12 col-md-auto f-block-height" style="padding: 0 !important;">
                    <a href="javascript:;" class="link grey-style-next large btn-next-step w-100" data-dismiss="modal" aria-label="Close">$editDesignLabel</a>
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>
  </div>

  <!-- tool tip -->
  <div class="tooltip-info position-top" id="tooltip-price">
    <p>20qm more drop the price extra. You want this?</p>
    <div class="text-center">
      <button type="button" class="btn-no">$noLabel</button>
      <button type="button" class="btn-yes">$yesLabel</button>
    </div>
  </div>

  <div class="tooltip-info position-top" id="tooltip-color-box">
    <p class="mb-1">Any extra color per tile will be $<span id="colorTooltipPrice">1</span> extra. Do you want this?</p>
    <div class="text-center">
      <button type="button" class="btn-no" onclick="closeColorTooltip()">$noLabel</button>
      <button type="button" class="btn-yes" onclick="submitColorTooltip()">$yesLabel</button>
    </div>
  </div>
HTML;

echo $html;
