<?php

function validateProductDetail($productDetail)
{
  if (empty($productDetail)) {
    return404();
  }
}

function extractRequest($action = '', $request = [])
{
  if (empty($action) || empty($request)) {
    return404();
  }

  switch ($action) {
    case 'design':
      return [
        $request['product_id'],
        $request['shape'],
        '',//size
        // $request['size'], // debug
        '',
        '', 
        $request['manu_id'],
      ];
      break;

    case 'shareLink':
      $g_linkID = !empty($request['id']) ? $request['id'] : null;
      $linkResult = getTileToolShareLink($g_linkID, ['shapeSlug', 'sizeID', 'productID', 'customizedData', 'image_data', 'uid']);
      if (empty($linkResult)) {
        return404();
      }

      $shape = $linkResult->shapeSlug;
      $size = $linkResult->sizeID;
      $size = get_term_by('id', $size, 'pa_' . $shape . '-size');
      if ($size) {
        $size = $size->slug;
      }
      $productId = $linkResult->productID;
      $productConfigData = $linkResult->customizedData;
     /// var_dump($productConfigData);
      $uid = $linkResult->uid;
      return [
        $productId,
        $shape,
        $size,
        $productConfigData,
        $uid,
        '',
         844,//add by frank
      ];
      break;

    case 'editCart':
      if (!isset($_SESSION['customCheckout'][$request['id']]) || empty($_SESSION['customCheckout'][$request['id']])) {
        return404();
      }

      $item = $_SESSION['customCheckout'][$request['id']];
      $productDetail = $item['productDetail'];
      return [
        $productDetail['productID'],
        $productDetail['shapeSlug'],
        $productDetail['sizeSlug'],
        json_encode($item['layerCanvas']),
        '',
        '',
      ];
      break;

    default:
      return404();
      break;
  }
}

function buildLayersObject($layers = [], $confidData = [])
{
  // User logged in or not with already set up data
  if (!is_user_logged_in()) {
    $sID = session_id();
    $layersObj = !empty($_SESSION[$sID]) ? $_SESSION[$sID] : [];
  } else {
    $current_user = wp_get_current_user();
    $layersObj = !empty($_SESSION[$current_user->user_login]) ? $_SESSION[$current_user->user_login] : [];
  }

  if (!empty($layersObj)) {
    return $layersObj;
  }

  $maxMinLayerDatas = $mainLayerIds = $layersObj = [];
  // User logged in or not without already set up data
  foreach ($layers as $id => $src) {
    $layersObj[$id] = [];
  }
  if (!empty($confidData)) {
    $layersObj = $confidData;
  }
  foreach ($layersObj as $layerId => $layerData) {
    if ('background-color' == $layerId) {
      continue;
    }

    $_layerId = isset($layerData['id']) ? $layerData['id'] : $layerId;
    $existedLayer = get_post($_layerId);
    if (!empty($existedLayer) && 'publish' == $existedLayer->post_status) {
      $layersObj[$layerId]['imgSource'] = get_the_post_thumbnail_url($_layerId, 'full');
      $layersObj[$layerId]['maxMinData'] = [];
      if ($layerData['duplicateID']) {
        $mainLayerIds[$layerData['duplicateID']] = true;
      } else {
        $mainLayerIds[$_layerId] = true;
      }
    } else {
      unset($layersObj[$layerId]);
    }
  }


  foreach ($mainLayerIds as $mainLayerId => $val) {
    $maxMinLayerDatas[$mainLayerId] = [
      'min_limit_moving' => pods_field('layer', $mainLayerId, 'min_limit_moving', true),
      'max_limit_moving' => pods_field('layer', $mainLayerId, 'max_limit_moving', true),
      'min_limit_rotate' => pods_field('layer', $mainLayerId, 'min_limit_rotate', true),
      'max_limit_rotate' => pods_field('layer', $mainLayerId, 'max_limit_rotate', true),
      'min_limit_scaling' => pods_field('layer', $mainLayerId, 'min_limit_scaling', true),
      'max_limit_scaling' => pods_field('layer', $mainLayerId, 'max_limit_scaling', true),
      'min_limit_skewing' => pods_field('layer', $mainLayerId, 'min_limit_skewing', true),
      'max_limit_skewing' => pods_field('layer', $mainLayerId, 'max_limit_skewing', true)
    ];
  }

  foreach ($layersObj as $layerId => $layerData) {
    if ('background-color' == $layerId) {
      continue;
    }

    $_layerId = $layerData['id'];
    if ($layerData['duplicateID'] && isset($maxMinLayerDatas[$layerData['duplicateID']])) {
      $_layerId = $layerData['duplicateID'];
    }

    $layersObj[$layerId]['maxMinData'] = $maxMinLayerDatas[$_layerId];
  }

  return $layersObj;
}

function getTilesShopCheckout()
{
  if (!isset($_SESSION['customCheckout']) || empty($_SESSION['customCheckout'])) {
    return null;
  }

  $cartItems = $_SESSION['customCheckout'];
  foreach ($cartItems as $id => $item) {
    $imageData = $item['image'];
    $imageName = 'order_image_of_' .  $id . '_' . $id;
    if (empty($item['imageLink'])) {
      $image = uploadFeaturedImageForProduct(null,  $imageName, $imageData, false);
      $cartItems[$id]['imageLink'] =  $image[0] ?? $item['image'];
    }
  }
  $_SESSION['customCheckout'] = $cartItems;

  return $_SESSION['customCheckout'];
}

function buildTileShopToolCustomMenu($menu_name = 'tileshop-tool-menu')
{
  if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
    $menu = wp_get_nav_menu_object($locations[$menu_name]);

    $menu_items = wp_get_nav_menu_items($menu->term_id);

    $menu_list = '';
    foreach ((array) $menu_items as $key => $menu_item) {
      $title = $menu_item->title;
      $url = $menu_item->url;
      $menu_list .= '<li><a href="' . $url . '">' . $title . '</a></li>';
    }
  } else {
    $menu_list = '<li>Menu "' . $menu_name . '" not defined.</li>';
  }

  return $menu_list;
}

function renderCollapseMenu()
{

  $cartItem = isset($_SESSION['customCheckout']) ? count($_SESSION['customCheckout']) : 0;
  $cartItem = (int)$cartItem ? '(<span id="cartItem">' . $cartItem . '</span>)' : '';
  $cartLink = $cartItem ? '?action=cart' : '#';
  $shoppingCartLabel = esc_html__('YOUR CART', 'tile-tool');

  $logoImgUrl = get_option('logo-configuration') ?? esc_url(plugins_url('assets/new-assets/img/logo.png', __FILE__));
  $logoutUrl = wp_logout_url(home_url());

  $userHtml = <<<HTML
    <div class="mt-4">
        <div class="row justify-content-center">
            <div class="col-auto">
            <button class="link no-bg white border-white hover-bg-white" onclick="location.href='$logoutUrl'">SIGN OUT</button>
            </div>
        </div>
    </div>
HTML;
  if (!is_user_logged_in()) {
    $userHtml = <<<HTML
    <div class="mt-4">
        <p class="white">Sign in to see your order status or share your designs</p>
        <div class="row justify-content-center">
            <div class="col-auto">
            <button class="link no-bg white border-white hover-bg-white" data-toggle="modal" data-target="#loginModalCenter">SIGN
                IN</button>
            </div>
            <div class="col-auto">
            <button class="link grey" data-toggle="modal" data-target="#signupModalCenter">SIGN UP</button>
            </div>
        </div>
    </div>
HTML;
  }

  $menu_name = 'tileshop-tool-menu';
  $menu_list = buildTileShopToolCustomMenu($menu_name);

  $langList = get_terms('language');
  $langData = [];
  foreach ($langList as $langObj) {
    $langDesc = unserialize($langObj->description);
    $langData[] = [
      'name' => $langObj->name,
      'code' => strstr($langDesc['locale'], '_', true),
      'locale' => $langDesc['locale'],
      'flag' => esc_url(plugins_url('../polylang/flags/' . $langDesc['flag_code'] . '.png', __FILE__))
    ];
  }
  $selectedLang = [
    'locale' => pll_current_language('locale'),
    'slug' => pll_current_language()
  ];

  $langHtml = '';
  foreach ($langData as $lang) {
    $langText = strtoupper($lang['code']);
    if ($selectedLang['locale'] == $lang['locale']) {
      $langHtml .= '<span class="active">' . $langText . '</span> / ';
      continue;
    }

    $href = $lang['code'] == 'en' ? '/' : 'javascript:void(0);';
    $langHtml .= '<a href="' . $href . '" class="change-language" data-lang-code="' . $lang['code'] . '">' . $langText . '</a> / ';
  }
  $langHtml = rtrim($langHtml, ' / ');

  $fbShareUrl = get_option('facebook-share-url');
  $instaShareUrl = get_option('instagram-share-url');
  $pintsShareUrl = get_option('pinterest-share-url');

  $html = <<<HTML

<div class="collapse w-100" id="collapseMenu">
      <button class="btn btn-link btn-menu p-0 ml-3" type="button" data-toggle="collapse" data-target="#collapseMenu" aria-controls="collapseMenu" aria-expanded="false" aria-label="Toggle docs navigation">
        <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M13.4719 10.9975L20.9312 3.53879C21.6094 2.86073 21.6094 1.7452 20.9312 1.06714C20.2531 0.389078 19.1375 0.389078 18.4594 1.06714L11 8.52583L3.54063 1.06714C2.8625 0.389078 1.74687 0.389078 1.06875 1.06714C0.729687 1.40617 0.554688 1.85457 0.554688 2.30296C0.554688 2.75136 0.729687 3.19976 1.06875 3.53879L8.52812 10.9975L1.06875 18.4562C0.729687 18.7952 0.554688 19.2436 0.554688 19.692C0.554688 20.1404 0.729687 20.5888 1.06875 20.9278C1.74687 21.6059 2.8625 21.6059 3.54063 20.9278L11 13.4691L18.4594 20.9278C19.1375 21.6059 20.2531 21.6059 20.9312 20.9278C21.6094 20.2497 21.6094 19.1342 20.9312 18.4562L13.4719 10.9975Z" fill="white" />
        </svg>
      </button>
      <div class="h-mobile-wrap">
        <div>
          <div class="text-center">
            <a href="?" class="logo">
              <img src="$logoImgUrl" alt="">
            </a>
          </div>
          <ul class="h-menu h-mobile" id="menu-$menu_name">
            $menu_list
          </ul>
          <div class="text-center mt-3">
            <a href="$cartLink" class="link"><i class="fa fa-shopping-cart mr-3"></i> $cartItem $shoppingCartLabel</a>
          </div>
          <ul class="social text-center mt-3">
            <li>
              <a href="$fbShareUrl" target="_blank">
                <i class="fab fa-facebook"></i>
              </a>
            </li>
            <li>
              <a href="$instaShareUrl" target="_blank">
                <i class="fab fa-instagram"></i>
              </a>
            </li>
            <li>
              <a href="$pintsShareUrl" target="_blank">
                <i class="fab fa-pinterest"></i>
              </a>
            </li>
          </ul>

          $userHtml
        </div>
      </div>
      <div class="language-menu">
        $langHtml
      </div>
    </div>

HTML;

  return $html;
}


function renderLoginModal()
{
  $secutiry = wp_nonce_field('ajax-login-nonce', 'security');
  $html = <<<HTML
<div class="modal fade login-modal" id="loginModalCenter" tabindex="-1" role="dialog" aria-labelledby="loginModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header justify-content-center border-0">
          <h5 class="modal-title pt-3" id="loginModalCenterTitle">Sign in</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-wrap">
            <form action="" id="loginForm">
              $secutiry
              <input type="text" name="username" required placeholder="Your username" class="input3">
              <input type="password" name="password" required placeholder="Password" class="input3 has-eye">
              <p class="text-error hide info-error">Wrong username or password</p>
              <div class="text-right mb-3"><a href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#forgotpasswordModalCenter">Forgot
                  your password?</a></div>
              <div class="row no-gutters">
                <div class="col pr-1">
                  <button type="submit" class="link grey w-100">SIGN IN</button>
                </div>
                <div class="col pl-1">
                  <button class="link w-100" data-dismiss="modal" data-toggle="modal" data-target="#signupModalCenter">SIGN
                    UP</button>
                </div>
              </div>
            </form>
            <p class="text-center mt-4">Or login with your social account</p>
            <div class="row justify-content-center mb-3">
              <div class="col-auto">
                <a href="" class="social-login"><i class="fab fa-facebook"></i></a>
              </div>
              <div class="col-auto">
                <a href="" class="social-login"><i class="fab fa-google-plus-g"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
HTML;

  echo $html;
}

function renderSignupModal()
{
  $secutiry = wp_nonce_field('ajax-register-nonce', 'signonsecurity');
  $html = <<<HTML
<div class="modal fade login-modal" id="signupModalCenter" tabindex="-1" role="dialog" aria-labelledby="signupModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header justify-content-center border-0">
          <h5 class="modal-title pt-3" id="loginModalCenterTitle">Get started with tile design</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-wrap">
          <form action="" id="signupForm">
              $secutiry
              <input type="email" name="email" required placeholder="Your email" class="input3">
              <input type="text" name="username" required placeholder="Your username" class="input3">
              <input type="password" name="password" required placeholder="Password" class="input3 has-eye">
              <input type="password" name="repassword" required placeholder="Confirm password" class="input3">
              <p class="text-error hide info-error">Invalid field, please re-check again.</p>
              <div class="row no-gutters">
                <div class="col pr-1">
                  <button type="submit" class="link grey w-100">CREATE ACCOUNT</button>
                </div>
                <div class="col pl-1">
                  <button class="link w-100" data-dismiss="modal" data-toggle="modal" data-target="#loginModalCenter">SIGN
                    IN</button>
                </div>
              </div>
            </form>
            <p class="text-center mt-4">Or login with your social account</p>
            <div class="row justify-content-center mb-3">
              <div class="col-auto">
                <a href="" class="social-login"><i class="fab fa-facebook"></i></a>
              </div>
              <div class="col-auto">
                <a href="" class="social-login"><i class="fab fa-google-plus-g"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
HTML;

  echo $html;
}
