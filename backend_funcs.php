<?php

function viewSocialShareUrlsConfiguration()
{
    $fbShareUrl = get_option('facebook-share-url');
    $instaShareUrl = get_option('instagram-share-url');
    $pintsShareUrl = get_option('pinterest-share-url');

    $socialShareUrlsTitleLabel = __('Social Share Urls Configuration', 'tile-tool');
    $submitBtnLabel = __('Submit', 'tile-tool');

    $html = <<<HTML
<div class="wrap">
    <p><strong>$socialShareUrlsTitleLabel</strong></p>
    <label>Facebook</label><p><input type="text" id="fbShareUrl" value="$fbShareUrl" style="width:50%;" /></p>
    <label>Instagram</label><p><input type="text" id="instaShareUrl" value="$instaShareUrl" style="width:50%;" /></p>
    <label>Pinterest</label><p><input type="text" id="pintsShareUrl" value="$pintsShareUrl" style="width:50%;" /></p>
    <p><button type="button" id="socialShareUrlsConfigBtn">$submitBtnLabel</button></p>
</div>
HTML;

    return $html;
}

function viewLogoConfiguration()
{
    $logoConfiguration = get_option('logo-configuration');

    $logoTitleLabel = __('TilesShop Logo Configuration', 'tile-tool');
    $logoNoteLabel = __('Please insert another image link if you want to update it.', 'tile-tool');
    $submitBtnLabel = __('Submit', 'tile-tool');

    $html = <<<HTML
<div class="wrap">
    <p><strong>$logoTitleLabel</strong></p>
    <p><img alt="logo image" src="$logoConfiguration" width="405" height="405" /></p>
HTML;
    if (!empty($rulerConfiguration)) {
        $html .= '<p style="color:red;"><strong>'.$logoNoteLabel.'</strong></p>';
    }
    $html .= <<<HTML
    <p><input type="text" id="logoConfigValue" value="$logoConfiguration" style="width:50%;" /></p>
    <p><button type="button" id="logoConfigBtn">$submitBtnLabel</button></p>
</div>
HTML;

    return $html;
}

function viewBlankTileConfiguration()
{
    $blankTileConfiguration = get_option('blank-tile-configuration');

    $blankTileLabel = __('TilesShop Blank Tile Configuration', 'tile-tool');
    $blankTileNoteLabel = __('Please input another product id if you want to update it.', 'tile-tool');
    $submitBtnLabel = __('Submit', 'tile-tool');

    $html = <<<HTML
<div class="wrap">
    <p><strong>$blankTileLabel</strong></p>
HTML;
    if (!empty($blankTileConfiguration)) {
        $html .= '<p style="color:red;"><strong>'.$blankTileNoteLabel.'</strong></p>';
    }
    $html .= <<<HTML
    <p><input type="number" min="0" autocomplete="off" id="blankTileConfigValue" value="$blankTileConfiguration" /></p>
    <p><button type="button" id="blankTileConfigBtn">$submitBtnLabel</button></p>
</div>
HTML;

    return $html;
}

function viewRulerConfiguration()
{
    $rulerConfiguration = get_option('ruler-configuration');

    $rulerTitleLabel = __('TilesShop Ruler Configuration', 'tile-tool');
    $rulerNoteLabel = __('Please insert another image link if you want to update it.', 'tile-tool');
    $submitBtnLabel = __('Submit', 'tile-tool');

    $html = <<<HTML
<div class="wrap">
    <p><strong>$rulerTitleLabel</strong></p>
    <p><img alt="rule image" src="$rulerConfiguration" width="405" height="405" /></p>
HTML;
    if (!empty($rulerConfiguration)) {
        $html .= '<p style="color:red;"><strong>'.$rulerNoteLabel.'</strong></p>';
    }
    $html .= <<<HTML
    <p><input type="text" id="rulerConfigValue" value="$rulerConfiguration" style="width:50%;" /></p>
    <p><button type="button" id="rulerConfigBtn">$submitBtnLabel</button></p>
</div>
HTML;

    return $html;
}

function viewCustomizedPrice()
{
    $customizedPriceObject = get_option('customized-price-object');

    $priceStep = $priceAreaPairs = '';
    if (!empty($customizedPriceObject)) {
        $priceStep = $customizedPriceObject['priceStep'];
        $priceAreaPairs = $customizedPriceObject['priceAreaPair'];
    }

    $headerLabel = __('TilesShop Customized Price Option', 'tile-tool');
    $noteLabel = __('<i>*Note: The smallest area node in m<sup>2</sup> will be set as minimum order and the percent discount per tile corresponding to the node will be set as default price per tile</i>',
        'tile-tool');
    $addMoreLabel = __('Add one more pair', 'tile-tool');
    $pairLabel = __('Pair ', 'tile-tool');
    $equalLabel = __('equal ', 'tile-tool');
    $submitBtnLabel = __('Submit', 'tile-tool');


    $html = <<<HTML
<div class="wrap">
    <p>
        <strong>$headerLabel</strong>
    </p>
    <p>
        <strong> Step (m<sup>2</sup>) </strong>
        <input type="number" min="0" class="priceStep" value="$priceStep">
    </p>
    <hr />
    <p>
        <label style="color:#C72C1C;">$noteLabel</label>
    </p>
    <p>
        <label>
            <a href="javascript:void(0);" id="addPriceAreaPairBtn">$addMoreLabel</a>
        </label>
    </p>
    <table class="table" id="priceAreaBlock" style="font-weight: bold;">
HTML;
    if (!empty($customizedPriceObject)) :
        $count = 1;
        $removePair = (count($priceAreaPairs) > 1) ? '<a href="javascript:void(0);" class="removePriceAreaPairBtn">remove pair</a>' : '';
        foreach ($priceAreaPairs as $area => $percent) {
            $html .= '<tr class="pricePair">';
            $html .= '<td class="priceAreaPairTitle">'.$pairLabel.' '.$count.'</td>';
            $html .= '<td><input type="number" min="0" name="percent" value="'.$percent.'" /> (%)</td>';
            $html .= '<td>'.$equalLabel.'</td>';
            $html .= '<td><input type="number" min="0" name="area" value="'.$area.'"/> (m<sup>2</sup>)</td>';
            $html .= '<td class="priceAreaPairRemoveBtn">'.$removePair.'</td>';
            $html .= '</tr>';
            $count++;
        }
    else :
        $html .= <<<HTML
        <tr class="pricePair">
            <td class="priceAreaPairTitle">$pairLabel 1</td>
            <td><input type="number" min="0" name="percent" /> (%)</td>
            <td>$equalLabel</td>
            <td><input type="number" min="0" name="area" /> (m<sup>2</sup>)</td>
            <td class="priceAreaPairRemoveBtn"></td>
        </tr>
HTML;
    endif;
    $html .= <<<HTML
    </table>
    <p><button type="button" id="customizedPriceSubmitBtn">$submitBtnLabel</button></p>
</div>
HTML;

    return $html;
}

function viewColorHoverConfiguration()
{
    $colorHoverConfiguration = get_option('color-hover-configuration');
    $colorHoverConfiguration = !empty($colorHoverConfiguration) ? $colorHoverConfiguration : '#BDBDBD';

    $colorHoverTitleLabel = __('TilesShop Color Hover Configuration', 'tile-tool');
    $colorHoverNoteLabel = __('Please choose another color if you want to update it.', 'tile-tool');
    $submitBtnLabel = __('Submit', 'tile-tool');

    $html = <<<HTML
<div class="wrap">
    <p><strong>$colorHoverTitleLabel</strong></p>
    <p style="color:red;">*$colorHoverNoteLabel</p>
    <p><input type="color" value="$colorHoverConfiguration" id="colorHoverConfigInput"></p>
    <p><button type="button" id="colorHoverConfigBtn">$submitBtnLabel</button></p>
</div>
HTML;

    return $html;
}

function viewVisualizerBackgroundLayer()
{
    $visualizerBgLayer = get_option('customized-visualizer-bg-layer-object');

    $bgImageList = !empty($visualizerBgLayer) ? $visualizerBgLayer : '';

    $headerLabel = __('TilesShop Visualizer Background Layer Option', 'tile-tool');
    $addMoreLabel = __('Add one more background', 'tile-tool');
    $pairLabel = __('Layer Name ', 'tile-tool');
    $equalLabel = __('~ Image Link ', 'tile-tool');
    $submitBtnLabel = __('Submit', 'tile-tool');


    $html = <<<HTML
<div class="wrap">
    <p>
        <strong>$headerLabel</strong>
    </p>
    <hr />
    <p>
        <label>
            <a href="javascript:void(0);" id="addVisualizerBgLayerPairBtn">$addMoreLabel</a>
        </label>
    </p>
    <table class="table" id="visualizerBgLayerPair" style="font-weight: bold;">
HTML;
    if (!empty($visualizerBgLayer)) :
        $count = 1;
        $removePair = (count($bgImageList) > 1) ? '<a href="javascript:void(0);" class="removePriceAreaPairBtn">remove pair</a>' : '';
        foreach ($bgImageList as $title => $imageLink) {
            $html .= '<tr class="pricePair">';
            $html .= '<td class="priceAreaPairTitle">'.$pairLabel.' '.$count.'</td>';
            $html .= '<td><input type="text" name="bg-layer-name" value="'.$title.'" /> </td>';
            $html .= '<td>'.$equalLabel.'</td>';
            $html .= '<td><input type="text" name="bg-layer-image-link" value="'.$imageLink.'"/></td>';
            $html .= '<td><img width="120px" height="120px" src="' .$imageLink . '" /></td>';
            $html .= '<td class="priceAreaPairRemoveBtn">'.$removePair.'</td>';
            $html .= '</tr>';
            $count++;
        }
    else :
        $html .= <<<HTML
        <tr class="pricePair">
            <td class="priceAreaPairTitle">$pairLabel 1</td>
            <td><input type="text" name="bg-layer-name" /></td>
            <td>$equalLabel</td>
            <td><input type="text" name="bg-layer-image-link" /></td>
            <td class="priceAreaPairRemoveBtn"></td>
        </tr>
HTML;
    endif;
    $html .= <<<HTML
    </table>
    <p><button type="button" id="visualizerBackgroundLayerSubmitBtn">$submitBtnLabel</button></p>
</div>
HTML;

    return $html;
}