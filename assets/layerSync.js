/**
 * Created by soicoi on 9/10/17.
 */
var productLayers = jQuery('#productLayers').val();
productLayers = JSON.parse(productLayers);

function getDefs(key) {
    return new Promise(function (resolve, reject) {
        if(key == undefined || obj == undefined) return false;

        var obj = document.getElementById(key.replace("canvas", "object"));
        jQuery(obj).load(function() {
            var content = this.contentDocument || this.contentWindow;
            var svg = content.getElementsByTagName("svg");
            var defs = jQuery(svg).find(">:first-child")[0];
            resolve([obj, defs]);
        });
    });
}

async function applyColor(key) {
    if (key === "background-color" && document.getElementById('layerBackground') != undefined) {
        document.getElementById('layerBackground').style.backgroundColor = productLayers[key];
        return;
    }

    var def = await getDefs(key);
    if(!def) return;
    else def = def[1];

    jQuery(def).find("style")
        .text("*{fill:" + productLayers[key]['color'] +";}");
}

async function applyTransformation(key) {
    if (key === "background-color") return;

    var rotateDegree = productLayers[key]['rotateDegree'];
    var scale = productLayers[key]['scale'].split(",");
    var position = productLayers[key]['position'].split(",");

    var ratio = 60 / $('#' + key).width();

    var obj = await getDefs(key);
    if(!obj) return;
    else obj = obj[0];

    var transform = "rotate(" + rotateDegree + "deg) scaleX(" + scale[0]
        + ") scaleY(" + scale[1] + ") translate(" + parseFloat(position[0]) * ratio + "px," + parseFloat(position[1]) * ratio + "px);";

    var transformStyle = "-webkit-transform:" + transform
                       + "-moz-transform:" + transform
                       + "-ms-transform:" + transform
                       + "-o-transform:" + transform
                       + "transform:" + transform;

    obj.setAttribute("style", transformStyle);
}

function doSyncLayer(layerId) {
    if(typeof productLayers == 'undefined' || productLayers == null) return;

    if (layerId.length < 1) {
       // sync all layers
        $.each(productLayers, function (key, value) {
            // apply color
            applyColor(key);
            // apply transform
            applyTransformation(key);
        });
    } else {
        // apply color
        applyColor(layerId);
        // apply transform
        applyTransformation(layerId);
    }
}

jQuery(window).load(function () {
    // pass "" or "child-canvas-77"
    doSyncLayer("");
});
