function main() {
  adjustScreenSize();

  loadTileShadow("#defaultImageUrlOfOnTileShadowShape");
  loadImageOnTop("#defaultImageUrlOfOnTopShape");
  loadImageOnTopAddMore("#defaultImageUrlOfOnAddMoreShape");
  loadImageOnTopBackground("#defaultImageUrlOfOnBackgroundShape");

  loadTerazzoLayout("#terazoLink");
  initSlider();

  var productLayers = $("#productLayers").val() || "{}";
  productLayers = JSON.parse(productLayers);
  if (
    typeof productLayers == "undefined" ||
    jQuery.isEmptyObject(productLayers)
  )
    return false;

  var productImages = $("#productImages").val() || "{}";
  productImages = JSON.parse(productImages);
  if (
    typeof productImages == "undefined" ||
    jQuery.isEmptyObject(productImages)
  ) {
    return false;
  }

  $.each(productImages, function (key, value) {
    if (key == "background-color") {
      return;
    }

    var childCanvasId = key;
    if ($.isEmptyObject(productLayers[childCanvasId])) {
      return;
    }
    var chidCanvasData = {};
    /**
     * Declare config flag for each layer = false
     * If config of layer is existed
     * Then setting up config data for layer and config flag = true
     * Else setting up default data for layer
     */
    // Default data

    chidCanvasData.id = key;
    chidCanvasData.name = value.name;
    chidCanvasData.duplicateID = 0;
    chidCanvasData.duplicateCounter = 0;
    chidCanvasData.imgSource = value.imgSource;
    chidCanvasData.rotateDegree = 0;
    chidCanvasData.scale = "1,1";
    chidCanvasData.skew = "0,0";
    chidCanvasData.position = "0,0";
    chidCanvasData.color = "";
    chidCanvasData.order = order_count;
    /**
     * Put layer data into array layerCanvas
     */
    layerCanvas[childCanvasId] = !$.isEmptyObject(productLayers[childCanvasId])
      ? productLayers[childCanvasId]
      : chidCanvasData;
    if (productLayers[childCanvasId].order === undefined)
      layerCanvas[childCanvasId].order = order_count;
    if (productLayers[childCanvasId].name === undefined)
      layerCanvas[childCanvasId].name = value.name;
    if (productLayers[childCanvasId].duplicateID === undefined)
      layerCanvas[childCanvasId].duplicateID = 0;
    if (productLayers[childCanvasId].duplicateCounter === undefined)
      layerCanvas[childCanvasId].duplicateCounter = 0; // Draw layer
    if (productLayers[childCanvasId].skew === undefined)
      layerCanvas[childCanvasId].skew = chidCanvasData.skew; // Draw layer

    if (
      productLayers[childCanvasId].duplicateID !== undefined &&
      productLayers[childCanvasId].name === undefined
    ) {
      var tmpDuplicateID =
        "child-canvas-" + productLayers[childCanvasId].duplicateID;
      layerCanvas[childCanvasId].name =
        productLayers[tmpDuplicateID].name +
        "-" +
        productLayers[childCanvasId].duplicateCounter;
    }

    var tmpColor = productLayers[childCanvasId].color;
    drawLayer(childCanvasId);

    if (tmpColor !== "" && !colorArray.includes(tmpColor)) {
      colorArray.push(tmpColor);
    }

    if (order_count < productImages.length) order_count++;
  });
  layerCanvas["background-color"] = !jQuery.isEmptyObject(
    productLayers["background-color"]
  )
    ? productLayers["background-color"]
    : "";
  $("#layerCanvas-bg").css("background-color", layerCanvas["background-color"]);

  setOrderCount();

  resetCanvas = JSON.parse(JSON.stringify(layerCanvas));
  originalCanvas = JSON.parse(JSON.stringify(layerCanvas)); //     initSlider();
  //
  var productDesignType = wpParams.productDesignType;
  var priceObject = !jQuery.isEmptyObject(wpParams.priceObject)
    ? wpParams.priceObject
    : {};
  if (
    ["customized", "color-only"].includes(productDesignType) &&
    $("#priceObject").val() != undefined
  ) {
    priceObject = $("#priceObject").val();
    priceObject = JSON.parse(priceObject);
  }
  var priceStep = parseFloat(priceObject["priceStep"]);
  var colorCount = colorArray.length;
  colorTotalPrice = parseFloat($("#colorUnitPrice").val() * colorCount);
  reCalculateColorPrice();

  $("body").on("mouseenter", "#layerBackground", function () {
    if (!$(this).hasClass("active")) {
      loadImageOnTopBackground("#imageUrlOfOnBackgroundHoverShape") ||
        loadImageOnTopBackground("#defaultImageUrlOfOnBackgroundHoverShape");
    }

    $("#layerCanvas-bg").css("background-color", defaultHoverColor);
  });

  $("body").on("mouseleave", "#layerBackground", function () {
    if (!$(this).hasClass("active")) {
      loadImageOnTopBackground("#imageUrlOfOnBackgroundShape") ||
        loadImageOnTopBackground("#defaultImageUrlOfOnBackgroundShape");
    }

    var currentColor = $(this).find("img").css("background-color");
    $("#layerCanvas-bg").css("background-color", currentColor);
  });

  $("body").on("mouseenter", "#customizedTool .btn-add-pattern", function () {
    loadImageOnTopAddMore("#imageUrlOfOnAddMoreHoverShape") ||
      loadImageOnTopAddMore("#defaultImageUrlOfOnAddMoreHoverShape");
    $("#customizedTool .icon-add-plus-not-hover")
      .removeClass("show")
      .addClass("hide");
    $("#customizedTool .icon-add-plus-hover")
      .removeClass("hide")
      .addClass("show");
  });

  $("body").on("mouseleave", "#customizedTool .btn-add-pattern", function () {
    loadImageOnTopAddMore("#imageUrlOfOnAddMoreShape") ||
      loadImageOnTopAddMore("#defaultImageUrlOfOnAddMoreShape");
    $("#customizedTool .icon-add-plus-not-hover")
      .removeClass("hide")
      .addClass("show");
    $("#customizedTool .icon-add-plus-hover")
      .removeClass("show")
      .addClass("hide");
  });

  $("body").on("mouseenter", "#pattern-list li.btn-pattern-item", function () {
    // Event handler for hover in small object layer

    var par = $(this);

    isHover = true;
    hoverLayerId = $(par).find("a").data("layer-id");
    var hoverColor = $(
      'div[data-main-color="' + layerCanvas[hoverLayerId]["color"] + '"]'
    ).data("hover-color");
    if (
      "undefined" == hoverColor ||
      !hoverColor ||
      typeof hoverColor == "undefined"
    ) {
      hoverColor = $("#colorHover").val();
    }
    if ($(par).hasClass("active")) {
      $("#color-tab > span.tab-color-layer").css(
        "background-color",
        layerCanvas[hoverLayerId]["color"]
      );
    }
    tmpHoverColor = layerCanvas[hoverLayerId]["color"];
    // tmpOriginColor = layerCanvas[hoverLayerId]["color"];
    fillColor(hoverColor, hoverLayerId);
    // Do show small preview hover image
    if (!$(par).hasClass("active")) {
      var coverSrc =
        $("#imageUrlOfOnSmallPreviewHoverShape").val() ||
        $("#defaultImageUrlOfOnSmallPreviewHoverShape").val();
      $("#small-preview-layer-cover-" + hoverLayerId).attr("src", coverSrc);
    }
  });

  $("body").on("mouseleave", "#pattern-list li.btn-pattern-item", function () {
    // Event handler for hover out small object layer
    var par = $(this);
    hoverLayerId = $(par).find("a").data("layer-id");

    fillColor(tmpHoverColor, hoverLayerId);
    if ($(par).hasClass("active")) {
      $("#color-tab > span.tab-color-layer").css(
        "background-color",
        layerCanvas[hoverLayerId]["color"]
      );
    }
    isHover = false;
    // Do hide small preview hover image
    if (!$(par).hasClass("active")) {
      var coverSrc =
        $("#imageUrlOfOnSmallPreviewShape").val() ||
        $("#defaultImageUrlOfOnSmallPreviewShape").val();
      $("#small-preview-layer-cover-" + hoverLayerId).attr("src", coverSrc);
    }
  });

  // console.log(IsMobileBrowser);
  var colorEvent = IsMobileBrowser ? "touchstart" : "click";
  // Change color event
  var colorTooltipContent = document.getElementById("tooltip-color-box");
  var colorBox = document.getElementById("color-box");
  colorBox.addEventListener(colorEvent, function (e) {
    if (layerId == "") {
      alert("Please choose layer first.");
      e.preventDefault();
      return false;
    }

    if (e.target && e.target.matches("div.color-item")) {
      currentSelectedColor = $(e.target).data("main-color");
      $("#colorTooltipPrice").html($("#colorUnitPrice").val().toString());
      // if (colorArray.includes(currentSelectedColor)) {
      //   submitColorTooltip();
      // } else {
      //   $(e.target).after(colorTooltipContent);
      //   $(e.target).next("#tooltip-color-box").fadeIn("fast");
      // }
      submitColorTooltip();
      $(e.target)
        .parents(".lbl-item")
        .children('input[name="color"]')
        .removeClass("colorUnchecked")
        .addClass("colorChecked");
    }
  });

  var colorBgBox = document.getElementById("background-color-box");
  colorBgBox.addEventListener(colorEvent, function (e) {
    if (layerId == "" || layerId != "background-color") {
      alert("Please choose layer first.");
      e.preventDefault();
      return false;
    }

    if (e.target && e.target.matches("div.color-item")) {
      currentSelectedColor = $(e.target).data("main-color");
      // if (colorArray.includes(currentSelectedColor)) {
      //   submitColorTooltip();
      // } else {
      //   $(e.target).after(colorTooltipContent);
      //   $(e.target).next("#tooltip-color-box").fadeIn("fast");
      // }
      submitColorTooltip();
      $(e.target)
        .parents(".lbl-item")
        .children('input[name="color"]')
        .removeClass("colorUnchecked")
        .addClass("colorChecked");
    }
  });

  $("#toolVisualizarModalCenter").on("show.bs.modal", function () {
    $(this)
      .find("#fragmentContent")
      .css("background-color", layerCanvas["background-color"]);
    $(this)
      .find("#fragmentContent")
      .css("border-color", layerCanvas["background-color"]);
    // $(this).css("padding-left", "0px !important");
    // $(this).css("overflow-y", "hidden !important");
    onVisualizeDesignModalOpen(this);
  });

  $("#toolVisualizarModalCenter").on("hide.bs.modal", function () {
    $("#testCanvas").remove();
    stopLoading();
  });

  // Load more layer modal
  $("#toolAddElementModalCenter").on("show.bs.modal", function () {
    onAddElementModalOpen(this);
  });

  $("#toolAddElementModalCenter").on("hide.bs.modal", function () {
    $('input[name="lm-tag"]').each(function () {
      $(this).prop("checked", false);
    });

    $("#btnAddMoreLayer").prop("disabled", true);
    $("#btnAddMoreLayer").removeClass("blue-style").addClass("link-hover");
  });

  // Share design modal
  $("#toolShareModalCenter").on("show.bs.modal", function () {
    onShareDesign(this);
  });

  $("#toolShareModalCenter").on("hide.bs.modal", function () {
    $(this).find("#input-share-link").val("");
    $(".btn-copy-link").html("Copy link");
  });

  $(document).on("click", "#visualizerShareBtn", function () {
    onShareDesign(this);
  });

  $(".tool-layer .nav-link").click(function (e) {
    e.preventDefault();

    if (layerId == "") {
      alert("Please choose layer first.");
      return false;
    }

    $(".tool-layer .nav-link, .tool-layer .nav-item").removeClass("active");

    $(this).addClass("active");
    $(this).parents(".nav-item").addClass("active");

    $("#myTabContent .tab-pane").removeClass("show").removeClass("active");
    var tab = $(this).data("tab");
    $("#" + tab)
      .addClass("show")
      .addClass("active");
    doSyncPreviewLayer(layerId);
  });

  $("body").on("click", ".btn-move-step", function () {
    var { type, step, slider } = $(this).data();
    if (type && step) {
      switch (type) {
        case "up":
          up(step, slider);
          break;
        case "down":
          down(step, slider);
          break;
        case "left":
          left(step, slider);
          break;
        case "right":
          right(step, slider);
          break;
        case "rotate-left":
          rotate_left(step, slider);
          break;
        case "rotate-right":
          rotate_right(step, slider);
          break;
      }
    }
  });

  $("body").on("click", "button.btn-reset-color", function () {
    if (layerId == "") {
      alert("Please choose layer first.");
      return false;
    }

    layerCanvas[layerId] = JSON.parse(JSON.stringify(resetCanvas[layerId]));
    $("li.btn-pattern-item.active").trigger("click");
    if (layerId != "background-color") {
      drawLayer(layerId);
      $(".tab-color-layer").css(
        "background-color",
        layerCanvas[layerId]["color"]
      );
    } else {
      $("#layerCanvas-bg, #layerCanvas").css(
        "background-color",
        layerCanvas["background-color"]
      );
      $(".tab-color-background, #layerBackground > img").css(
        "background-color",
        layerCanvas["background-color"]
      );

      doSyncPreviewLayer("background-color");
      $.each(layerCanvas, function (index, value) {
        if (index !== "background-color") {
          $("#smallPreviewImage_" + index).css(
            "background-color",
            layerCanvas["background-color"]
          );
        }
      });
    }
    reCalculateColorPrice();
    if (layerId == "background-color") {
      onSelectPatternItem("#layerBackground", layerCanvas["background-color"]);
    } else {
      onSelectPatternItem('a[data-layer-id="' + layerId + '"]', layerCanvas[layerId]['color']);
      // $('a[data-layer-id="' + layerId + '"]').trigger("click");
    }
  });

  $("body").on("change", "input.input-move-step", function () {
    var isOutOfRange = false;
    var val = parseInt($(this).val());
    var max = $(this).prop("max");
    var min = $(this).prop("min");
    var slider = $(this).attr("id").split("-");
    slider.pop();
    slider = slider.join("-");
    switch (slider) {
      case "move-up-down":
        if (val > max || val < min) {
          isOutOfRange = true;
        } else {
          var moveDirection = val < 0 ? "down" : "up";
          move(moveDirection, layerId, val);
          $(this).val(parseFloat(val) + "px");
        }
        break;
      case "move-left-right":
        if (val > max || val < min) {
          isOutOfRange = true;
        } else {
          var moveDirection = val < 0 ? "left" : "right";
          move(moveDirection, layerId, val);
          $(this).val(parseFloat(val) + "px");
        }
        break;
      case "tool-rotate":
        if (val > max || val < min) {
          isOutOfRange = true;
        } else {
          rotate(layerId, val);
          $(this).val(parseFloat(val).toFixed(0) + "\xB0");
        }
        break;
      case "scale-up-down":
        val = val / 100;
        if (val > max || val < min) {
          isOutOfRange = true;
          min = min * 100;
          max = max * 100;
        } else {
          scale("y", val, layerId);
          $(this).val(val.toFixed(2) * 100 + "%");
        }
        break;
      case "scale-left-right":
        val = val / 100;
        if (val > max || val < min) {
          isOutOfRange = true;
          min = min * 100;
          max = max * 100;
        } else {
          scale("x", val, layerId);
          $(this).val(val.toFixed(2) * 100 + "%");
        }
        break;
      case "skew-up-down":
        val = val / 100;
        if (val > max || val < min) {
          isOutOfRange = true;
          min = min * 100;
          max = max * 100;
        } else {
          skew("y", val, layerId);
          $(this).val(val.toFixed(2) * 100 + "%");
        }
        break;
      case "skew-left-right":
        val = val / 100;
        if (val > max || val < min) {
          isOutOfRange = true;
          min = min * 100;
          max = max * 100;
        } else {
          skew("x", val, layerId);
          $(this).val(val.toFixed(2) * 100 + "%");
        }
        break;
    }

    if (isOutOfRange) {
      alert("Invalid input. Must between: " + min + " and " + max);
      return false;
    }
    $("#" + slider).slider("setValue", val);
  });

  // $("body").on("change", "#priceRangeControl", function () {
  //   var area = parseInt($(this).val());
  //   var areaMax = parseInt($(this).prop("max"));
  //   var areaMin = parseInt($(this).prop("min"));

  //   if (area < areaMin || isNaN(area)) {
  //     area = areaMin;
  //   }
  //   if (area > areaMax) {
  //     area = areaMax;
  //   }
  //   $(this).slider("setValue", area);
  //   calPricePerArea(area, true);
  // });

  onSelectPatternItem("#layerBackground", layerCanvas["background-color"]);
  // $("#pattern-list .lower-canvas").css(
  //   "background-color",
  //   layerCanvas["background-color"]
  // );
  $.each(layerCanvas, function (index, value) {
    if (index !== "background-color") {
      $("#smallPreviewImage_" + index).css(
        "background-color",
        layerCanvas["background-color"]
      );
    }
  });
}

function getDefSelectedAddMoreLayerColor(key) {
  // return new Promise(function (resolve, reject) {
  //   var obj = document.getElementById(key.replace("canvas", "object"));
  //   if (key == undefined || obj == undefined) return false;
  //   var content = obj.contentDocument || obj.contentWindow;
  //   var svg = content.getElementsByTagName("svg");
  //   var defs = $(svg).find(">:first-child")[0];
  //   resolve([obj, defs]);
  // });
  var obj = document.getElementById(key.replace("canvas", "object"));
  if (key == undefined || obj == undefined) return false;
  var content = obj.contentDocument || obj.contentWindow;
  var svg = content.getElementsByTagName("svg");
  var defs = $(svg).find(">:first-child")[0];
  return [obj, defs];
}

function changeSelectedAddMoreLayerColor(key, color) {
  var def = getDefSelectedAddMoreLayerColor(key);
  if (!def) return false;
  else def = def[1];
  $(def)
    .find("style")
    .text("*{fill:" + color + ";}");
}

function checkShowAddAsNewToCartBtn() {
  if ($("#addAsNewItemBtn").length > 0) {
    $("#addAsNewItemBtn")
      .parents(".button-group")
      .removeClass("show")
      .addClass("hide");

    if (
      (!$.isEmptyObject(resetCanvas) &&
        JSON.stringify(layerCanvas) != JSON.stringify(originalCanvas)) ||
      $("#productDesignType").val() == "fixed"
    ) {
      $("#addAsNewItemBtn")
        .parents(".button-group")
        .removeClass("hide")
        .addClass("show");
    }
  }
}

function loadTileShadow(el) {
  if ($(el).val() == undefined) return false;
  var imgSrc = $(el).val();

  if (!checkIsImageExist(imgSrc)) {
    $("#mainCanvasTileShadowLayout").hide();
    return;
  }

  fabric.Image.fromURL(imgSrc, function (oImg) {
    oImg
      .set({
        width: tmpSize,
        height: tmpSize,
        left: 0,
        top: 0,
        hasControls: false,
        hasBorders: false,
        selectable: false,
      })
      .scale(screenRatio, false);

    hiddenCanvas.setWidth(400).setHeight(400).clear().add(oImg);

    $("#mainCanvasTileShadowLayout")
      .attr("src", hiddenCanvas.toDataURL())
      .show();
    adjustScreenSize();
  });
}

function loadTerazzoLayout(el) {
  if ($(el).val() == undefined) return false;
  var imgSrc = $(el).val();

  if (!checkIsImageExist(imgSrc)) {
    $("#mainCanvasTerazzoLayout").hide();
    return;
  }

  fabric.Image.fromURL(imgSrc, function (oImg) {
    oImg
      .set({
        width: tmpSize,
        height: tmpSize,
        left: 0,
        top: 0,
        hasControls: false,
        hasBorders: false,
        selectable: false,
      })
      .scale(screenRatio, false);

    hiddenCanvas.setWidth(400).setHeight(400).clear().add(oImg);

    $("#mainCanvasTerazzoLayout")
      .attr(
        "src",
        hiddenCanvas.toDataURL({
          width: 400,
          height: 400,
        })
      )
      .show();
    adjustScreenSize();
  });

  if ($("#terazoOpt").prop("checked")) {
    $("#mainCanvasTerazzoLayout").addClass("show").removeClass("hide");
  } else {
    $("#mainCanvasTerazzoLayout").addClass("hide").removeClass("show");
  }
}

function loadImageOnTop(el) {
  if ($(el).val() == undefined) return false;
  var imgSrc = $(el).val();

  if (!checkIsImageExist(imgSrc)) {
    $("#mainCanvasImageOnTop").hide();
    return;
  }

  fabric.Image.fromURL(imgSrc, function (oImg) {
    oImg
      .set({
        width: tmpSize,
        height: tmpSize,
        left: 0,
        top: 0,
        hasControls: false,
        hasBorders: false,
        selectable: false,
      })
      .scale(screenRatio, false);

    hiddenCanvas.clear().setWidth(400).setHeight(400).add(oImg);

    $("#mainCanvasImageOnTop")
      .attr(
        "src",
        hiddenCanvas.toDataURL({
          width: 400,
          height: 400,
        })
      )
      .show();
    adjustScreenSize();
  });
}

function loadImageOnTopAddMore(el) {
  if ($(el).val() == undefined) return false;
  var imgSrc = $(el).val();

  $("#addLayerCover").attr("src", imgSrc);
}

function loadImageOnTopAddMoreHover(el) {
  if ($(el).val() == undefined) return false;
  var imgSrc = $(el).val();

  $("#addLayerHoverCover").attr("src", imgSrc);
}

function loadImageOnTopBackground(el) {
  if ($(el).val() == undefined) return false;
  var imgSrc = $(el).val();

  $("#layerBackground img").attr("src", imgSrc);
}

function reCalculateColorPrice() {
  colorArray = [];
  $.each(layerCanvas, function (i, v) {
    if (
      i !== "background-color" &&
      v["color"] != "" &&
      v["color"] != layerCanvas["background-color"] &&
      !colorArray.includes(v["color"])
    ) {
      colorArray.push(v["color"]);
    } else if (i == "background-color" && v != "" && !colorArray.includes(v)) {
      colorArray.push(v);
    }
  });
  var colorCount = colorArray.length;
  if (colorCount == 1 && colorArray[0] == layerCanvas["background-color"]) {
    colorTotalPrice = 0;
    // colorArray = [];
  } else {
    colorTotalPrice = parseFloat($("#colorUnitPrice").val() * colorCount);
  }

  // calPricePerArea($("#priceRangeControl").val(), true);
}

// function calPricePerArea(area, isUpdate) {
//   var priceObject = $("#priceObject").val();
//   if (priceObject.length < 0) {
//     priceObject = {};
//   } else {
//     priceObject = JSON.parse(priceObject);
//   }
//   var piecePerM2 = parseFloat($("#ppa").val());
//   var piecePerBox = parseFloat($("#ppb").val());
//   var boxPerPallete = parseFloat($("#bpp").val());

//   var numberOfCloneToolTip =
//     $("#priceRangeSlider .tooltip-main.clone").length - 1;
//   $("#priceRangeSlider .tooltip-main.clone").each(function (i, v) {
//     var val = nodeList[i];
//     var txt = "$" + parseFloat(priceObject["priceAreaPair"][val]).toFixed(2);
//     $(this)
//       .find(".tooltip-inner")
//       .html(txt + "/m<sup>2</sup>");

//     if (!isUpdate) {
//       var previousLeft = parseFloat(
//         $(this).prev().css("left").replace("px", "")
//       );
//       var currentLeft = parseFloat($(this).css("left").replace("px", ""));
//       var nextLeft = parseFloat($(this).next().css("left").replace("px", ""));
//       var left = 0;
//       if (i == 0) {
//         left = nextLeft / 2;
//       } else if (i > 0 && i < numberOfCloneToolTip) {
//         left = (nextLeft - currentLeft) / 2 + currentLeft;
//       } else {
//         $(this).addClass("hide");
//       }

//       $(this).css("left", left.toString() + "px");
//     }
//   });
//   var tmpVal = parseFloat(area);
//   var pricePerTile = 0;
//   var text = 0;

//   $("#priceRangeSlider")
//     .find(".tooltip-main:not(.clone)")
//     .removeClass("show")
//     .addClass("hide");
//   $("#priceRangeSlider").find(".slider-tick").css("background", "#edeeef");
//   if ($.inArray(tmpVal, nodeList) < 0) {
//     $("#priceRangeSlider")
//       .find(".tooltip-main.clone .tooltip-inner")
//       .css("font-weight", "normal");
//     for (var i = 0; i < nodeList.length; i++) {
//       if (area > nodeList[i]) {
//         continue;
//       }

//       pricePerTile = priceObject["priceAreaPair"][nodeList[i - 1]];

//       $("#priceRangeSlider")
//         .find(".tooltip-main.clone:eq(" + (i - 1) + ") > .tooltip-inner")
//         .css("font-weight", "bolder");
//       $("#priceRangeSlider")
//         .find(".slider-tick:eq(" + (i - 1) + ")")
//         .css("background", "#656E7A");

//       var previousNodeWidth = $("#priceRangeSlider")
//         .find(".slider-tick:eq(" + (i - 1) + ")")
//         .css("left");
//       previousNodeWidth = previousNodeWidth.replace("px", "");
//       previousNodeWidth = parseFloat(previousNodeWidth);

//       var nextNodeWidth = $("#priceRangeSlider")
//         .find(".slider-tick:eq(" + i + ")")
//         .css("left");
//       nextNodeWidth = nextNodeWidth.replace("px", "");
//       nextNodeWidth = parseFloat(nextNodeWidth);

//       var firstNodeWidth = $("#priceRangeSlider")
//         .find(".slider-tick:eq(0)")
//         .css("left");
//       firstNodeWidth = firstNodeWidth.replace("px", "");
//       firstNodeWidth = parseFloat(firstNodeWidth);

//       var sliderSelectionWidth =
//         (nextNodeWidth - previousNodeWidth).toString() + "px";
//       var sliderSelectionLeft =
//         (previousNodeWidth - firstNodeWidth).toString() + "px";

//       if (i <= 1) {
//         sliderSelectionLeft = "0px";
//       }

//       $("#priceRangeSlider .tick-slider-selection").css(
//         "width",
//         sliderSelectionWidth
//       );
//       $("#priceRangeSlider .tick-slider-selection").css(
//         "left",
//         sliderSelectionLeft
//       );
//       break;
//     }
//   } else {
//     pricePerTile = priceObject["priceAreaPair"][area];
//     text = 1;
//   }
//   // Plus color price
//   pricePerTile = parseFloat(pricePerTile) + colorTotalPrice;
//   // Plus terazo price
//   var terazoPrice = parseFloat($("#terazoPrice").val());
//   if ($("#terazoOpt").is(":checked")) {
//     pricePerTile += terazoPrice;
//   }
//   text = text === 1 ? "$" + pricePerTile : area + " m<sup>2</sup>";
//   var pricePerM2 = parseFloat(pricePerTile * piecePerM2);

//   var calculatedPrice = parseFloat(pricePerM2 * parseFloat(area)).toFixed(2);
//   $("#priceRangeSlider")
//     .find(".tooltip-main:not(.clone) .tooltip-inner")
//     .html(text + "/m<sup>2</sup>");
//   $("#priceValue").val(calculatedPrice);
//   $(".priceVal").html(
//     '<span id="calculatedArea">' +
//       area +
//       "</span>" +
//       " m<sup>2</sup> - $" +
//       '<span class="calculatedPrice" id="calculatedPrice">' +
//       calculatedPrice +
//       "</span>"
//   );
//   $("#pricePerTile").html(parseFloat(pricePerTile).toFixed(2));
//   $("#pricePerM2Default").val(parseFloat(pricePerM2).toFixed(2));

//   $("#pricingDetailTooltip").attr(
//     "data-original-title",
//     '<span class="grey">Price per m2</span>' +
//       '<b id="pricePerTile">$' +
//       parseFloat(pricePerM2).toFixed(2) +
//       "</b>" +
//       "<br>" +
//       '<span class="grey">Tiles per m2</span>' +
//       "<b>" +
//       piecePerM2.toFixed(2) +
//       "</b>" +
//       "<br>" +
//       '<span class="grey">Tiles per box</span>' +
//       "<b>" +
//       parseFloat($("#ppb").val()).toFixed(2) +
//       "</b>"
//   );

//   var totalPieces = area * piecePerM2;
//   var totalBoxes = totalPieces / piecePerBox;
//   var palletes = totalBoxes / boxPerPallete;

//   $("#calculatedPrice, .calculatedPrice").html(
//     "$" + calculatedPrice.toString()
//   );

//   $("#piecesPerM2").val(Math.ceil(parseFloat(piecePerM2).toFixed(2)));
//   $("#pricePerM2").val(parseFloat(pricePerM2).toFixed(2));
//   $("#tilesPerBox").val(Math.ceil(parseFloat($("#ppb").val()).toFixed(2)));
//   $("#totalBoxes").val(Math.ceil(parseFloat(totalBoxes).toFixed(2)));
//   $("#totalPieces").val(Math.ceil(parseFloat(totalPieces).toFixed(2)));
//   $("#totalPalletes").val(Math.ceil(parseFloat(palletes).toFixed(2)));
//   $("#totalBoxesLabel").html(Math.ceil(parseFloat(totalBoxes).toFixed(2)));
//   $("#totalTilesLabel").html(Math.ceil(parseFloat(totalPieces).toFixed(2)));

//   if ($("#productDesignType").val() == "fixed") {
//     $("#fixedTileCurrentBoxes").html(
//       Math.ceil(parseFloat(totalBoxes).toFixed(2))
//     );
//     var totalPiecesFirstNode =
//       parseFloat($("#minimumOrder").val()) * piecePerM2;
//     $("#fixedTileMinOrderTiles").html(totalPiecesFirstNode);

//     var totalPiecesInStock =
//       parseFloat($("#fixedTileCurrentOrderedTiles").html()) / piecePerM2;
//     $("#fixedTileCurrentOrderedSquare").html(totalPiecesInStock.toFixed(2));
//   }

//   $("#priceRangeSlider .tooltip-main.clone").each(function (i, v) {
//     var newCloneHtml = $(this).find(".tooltip-inner").html();
//     newCloneHtml = newCloneHtml.replace("$", "");
//     newCloneHtml = parseFloat(newCloneHtml);
//     newCloneHtml += colorTotalPrice;
//     if ($("#terazoOpt").is(":checked")) {
//       newCloneHtml += terazoPrice;
//     }

//     var pricePerM2 = parseFloat(newCloneHtml).toFixed(2) * piecePerM2;
//     $(this)
//       .find(".tooltip-inner")
//       .html("$" + parseFloat(pricePerM2).toFixed(2) + "/m<sup>2</sup>");
//   });
// }

// function loadPricingSlider() {
//   var priceObject = $("#priceObject").val();
//   if (priceObject.length < 0) {
//     priceObject = {};
//   } else {
//     priceObject = JSON.parse(priceObject);
//   }

//   var priceStep = parseFloat(priceObject["priceStep"]);
//   var colorCount = colorArray.length;
//   colorTotalPrice = parseFloat($("#colorUnitPrice").val() * colorCount);
//   reCalculateColorPrice();
//   if (priceObject["priceAreaPair"] == undefined) {
//     alert("Something wrong with price. Please contact admin.");
//     return false;
//   }

//   $.each(priceObject["priceAreaPair"], function (index, value) {
//     nodeList.push(parseFloat(index));
//     nodeListTxt.push(index + "m\u00B2");
//   });

//   $("#fixedTileMinOrderSquare").html(nodeListTxt[0]);

//   var selectedArea =
//     undefined != $("#selectedArea").val()
//       ? parseInt($("#selectedArea").val())
//       : nodeList[0];
//   $("#priceRangeControl")
//     .slider({
//       tooltip: "always",
//       ticks_labels: nodeListTxt,
//       ticks: nodeList,
//       step: priceStep,
//       value: selectedArea,
//       modify_label_layout: true,
//       formatter: function (value) {
//         if (priceObject["priceAreaPair"][value] !== undefined) {
//           return (
//             "$" + parseFloat(priceObject["priceAreaPair"][value]).toFixed(2)
//           );
//         }
//         return value + " m2";
//       },
//     })
//     .on("slideStart", function () {
//       if (!IsMobileBrowser) {
//         calPricePerArea(this.value, true);
//       }
//     })
//     .on("slide", function () {
//       if (!IsMobileBrowser) {
//         calPricePerArea(this.value, true);
//       }
//     })
//     .on("slideStop", function () {
//       calPricePerArea(this.value, true);
//     });

//   $("#priceRangeSlider")
//     .find(".slider-tick")
//     .each(function (idx, val) {
//       var left = $(this).attr("style").replace("left: ", "").replace(";", "");
//       var tooltipFake = $(
//         "#priceRangeSlider .tooltip-main:not(.clone)"
//       ).clone();
//       $(tooltipFake).addClass("clone");
//       $(tooltipFake).css("left", left);
//       $(tooltipFake).insertBefore("#priceRangeSlider .tooltip-min");

//       $(".slider-tick-label:eq(" + idx + ")").css("left", left);
//     });

//   calPricePerArea($("#priceRangeControl").val(), false);
//   $("#priceRangeControl").prop("max", Math.max.apply(null, nodeList));
//   $("#priceRangeControl").prop("min", nodeList[0]);
// }

function startLoading() {
  // add the overlay with loading image to the page
  // var over =
  //   '<div id="overlay">' +
  //   '<img id="loading" src="' +
  //   $("#loadingImg").val() +
  //   '">' +
  //   "</div>";
  var over =
  '<div id="overlay"><div id="loading" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>';
  $(over).appendTo("body");
  $("body").css("overflow", "hidden");
}

function stopLoading() {
  $("#overlay").remove();
  $("body").css("overflow", "");
}

function adjustScreenSize() {
  $("#layerCanvasBorder").css("width", tmpSize + "px");
  // $("#layerCanvas").css("width", tmpSize + "px");
  // $("#layerCanvas").css("height", tmpSize + "px");
  $("#layerCanvas img").attr("width", tmpSize.toString());
  $("#layerCanvas img").attr("height", tmpSize.toString());
  $("#layerCanvas img").css("position", "absolute");
  $("#typo3Image").hide();
}

function setOrderCount() {
  var orderArr = [];
  $.each(layerCanvas, function (key, value) {
    if (key !== "background-color") {
      orderArr.push(parseInt(value.order));
    }
  });
  var maxOrder = Math.max.apply(null, orderArr);
  if (order_count < maxOrder) {
    order_count = maxOrder;
  }
}

function setLayerOrder(_layerId, orderValue) {
  $("#" + _layerId).css("z-index", orderValue);
}

function getLayerIdByAttr(attr, val) {
  var tmp = "";
  $.each(layerCanvas, function (key, value) {
    if (key != "background-color" && value[attr] == val) {
      return (tmp = key);
    }
  });

  return tmp;
}

function getURLParameter(sParam) {
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split("&");
  for (var i = 0; i < sURLVariables.length; i++) {
    var sParameterName = sURLVariables[i].split("=");
    if (sParameterName[0] == sParam) return sParameterName[1];
  }
}

function renderPreviewTool(layerId) {
  $(".preview-object").each(function (key, value) {
    $(value).prop("data", layerCanvas[layerId]["imgSource"]);
    $(value).load(function () {
      var content = this.contentDocument || this.contentWindow;
      var svg = content.getElementsByTagName("svg");
      var def = $(svg).find(">:first-child")[0];
      if (!def) {
        return false;
      }
      // Color
      $(def)
        .find("style")
        .text("*{fill:" + layerCanvas[layerId]["color"] + ";}");
      // Transformation
      var rotateDegree = layerCanvas[layerId]["rotateDegree"];
      var scale = layerCanvas[layerId]["scale"].split(",");
      var position = layerCanvas[layerId]["position"].split(",");
      var ratio = 60 / $("#" + layerId).width();

      var transform =
        "rotate(" +
        rotateDegree +
        "deg) scaleX(" +
        scale[0] +
        ") scaleY(" +
        scale[1] +
        ") translate(" +
        parseFloat(position[0]) * ratio +
        "px," +
        parseFloat(position[1]) * ratio +
        "px);";
      var transformStyle =
        "-webkit-transform:" +
        transform +
        "-moz-transform:" +
        transform +
        "-ms-transform:" +
        transform +
        "-o-transform:" +
        transform +
        "transform:" +
        transform;
      this.setAttribute("style", transformStyle);
    });
  });
}
function makeTooltipForColorsInUsed() {
  $(".color-item.color-item-list").each(function () {
    var mainColor = $(this).data("main-color");
    var label = $(this).data("label");
    $(
      '.color-item:not(.color-item-list)[data-main-color="' + mainColor + '"]'
    ).each(function () {
      $(this).attr("data-original-title", label);
    });
  });
}
function renderColorsInUsed() {
  $(".tooltip.show").remove();
  var colorsInUsed = [];
  $.each(layerCanvas, function (key, data) {
    var colorInUsed = "";
    if (key == "background-color" || data.color == "") {
      return;
    }

    if (hoverLayerId == key && isHover) {
      colorInUsed = tmpHoverColor;
    } else {
      colorInUsed = data.color;
    }

    if (colorsInUsed.indexOf(colorInUsed) == -1) {
      colorsInUsed[parseInt(data.order)] = colorInUsed;
    }
  });

  var bgColorIdx = colorsInUsed.indexOf(layerCanvas["background-color"]);
  if (bgColorIdx > -1) {
    colorsInUsed.splice(bgColorIdx, 1);
  }

  colorsInUsed.push(layerCanvas["background-color"]);

  var obj = "#recent-color-list";
  if (layerId == "background-color") {
    obj = "#recent-bg-color-list";
  }

  // ["#recent-color-list", "#recent-bg-color-list"].forEach(function (v, k) {
  //   var obj = v;
  var btn = $(obj + " > .col-auto").get(0).outerHTML;
  var html = "";
  for (var i = colorsInUsed.length - 1; i >= 0; i--) {
    if (colorsInUsed[i] == "" || colorsInUsed[i] == null) {
      continue;
    }

    var _tmpHoverColor = $(
      '.color-item[data-main-color="' + colorsInUsed[i] + '"]'
    ).data("hover-color");

    var _tmpColorLabel = $(
      '.color-item[data-main-color="' + colorsInUsed[i] + '"]'
    ).data("label");

    html +=
      '<div class="col">' +
      '<label class="lbl-item"><input autocomplete="off" type="radio" name="color" class="d-none">' +
      '<div class="color-item" style="background: ' +
      colorsInUsed[i] +
      '" data-main-color="' +
      colorsInUsed[i] +
      '"' +
      ' data-hover-color="' +
      _tmpHoverColor +
      '" data-toggle="tooltip" data-placement="top" title="' +
      _tmpColorLabel +
      '">' +
      '<i class="icon-check"></i></div>' +
      "</label>" +
      "</div>";
  }
  if (colorsInUsed.length < RECENT_COLOR_MAXIMUM) {
    for (var i = 0; i < RECENT_COLOR_MAXIMUM - colorsInUsed.length; i++) {
      html += '<div class="col"></div>';
    }
  }

  $(obj).html(btn + html);
  makeTooltipForColorsInUsed();
  $('[data-toggle="tooltip"]').tooltip();
  // });
}

function fillColor(color, layerId) {
  isReload = true;
  layerCanvas[layerId]["color"] = color;
  drawLayer(layerId);
}

function rotate(layerId, degree) {
  isReload = true;

  layerCanvas[layerId]["rotateDegree"] = degree.toString();
  drawLayer(layerId);
}

function move(direction, layerId, value) {
  isReload = true;

  value = parseFloat(value);
  var position = layerCanvas[layerId]["position"];
  position = position.split(",");
  var horizontal = parseFloat(position[0]);
  var vertical = parseFloat(position[1]);

  switch (direction) {
    case "left":
    case "right":
      horizontal = value;
      break;

    case "up":
    case "down":
      vertical = 0 - value;
      break;
  }

  position[0] = parseFloat(horizontal);
  position[1] = parseFloat(vertical);
  layerCanvas[layerId]["position"] = position.join(",");
  drawLayer(layerId);
}
function skew(direction, value, layerId) {
  isReload = true;

  var skew = layerCanvas[layerId]["skew"];
  skew = skew.split(",");
  var skewX = parseFloat(skew[0]);
  var skewY = parseFloat(skew[1]);

  switch (direction) {
    case "x":
      skewX = value;
      break;

    case "y":
      skewY = value;
      break;
  }

  skew[0] = parseFloat(skewX).toFixed(2);
  skew[1] = parseFloat(skewY).toFixed(2);
  layerCanvas[layerId]["skew"] = skew.join(",");
  drawLayer(layerId);
}

function scale(direction, value, layerId) {
  isReload = true;

  var scale = layerCanvas[layerId]["scale"];
  scale = scale.split(",");
  var scaleX = parseFloat(scale[0]);
  var scaleY = parseFloat(scale[1]);

  switch (direction) {
    case "x":
      scaleX = value;
      break;

    case "y":
      scaleY = value;
      break;
  }

  scale[0] = parseFloat(scaleX).toFixed(2);
  scale[1] = parseFloat(scaleY).toFixed(2);
  layerCanvas[layerId]["scale"] = scale.join(",");
  drawLayer(layerId);
}

function modifyingLayer(layerData) {
  // try to detect a fresh new layer added and drawn into canvas, then scale it to 0.7 - 0.92
  if (layerData.scale == "1,1") {
    layerData.scale = "0.92,0.92";
  }

  return layerData;
}

function drawLayer(layerId) {
  layerCanvas[layerId] = modifyingLayer(layerCanvas[layerId], layerId);

  var imgSrc = layerCanvas[layerId]["imgSource"];

  var color = layerCanvas[layerId]["color"];

  var rotateDegree = parseInt(layerCanvas[layerId]["rotateDegree"]);

  var position = layerCanvas[layerId]["position"];
  position = position.split(",");
  var left = parseFloat(position[0]);
  var top = parseFloat(position[1]);

  var scale = layerCanvas[layerId]["scale"];
  scale = scale.split(",");
  var scaleX = parseFloat(scale[0]);
  var scaleY = parseFloat(scale[1]);

  var skew = layerCanvas[layerId]["skew"];
  skew = skew.split(",");
  var skewX = parseFloat(skew[0]);
  var skewY = parseFloat(skew[1]);

  if (!checkIsImageExist(imgSrc)) {
    return;
  }

  fabric.loadSVGFromURL(imgSrc, function (objects, options) {
    var obj = fabric.util.groupSVGElements(objects, options);
    obj
      .set({
        // Default value
        width: tmpSize,
        height: tmpSize,
        selectable: false,
        centeredScaling: true,
        centeredRotation: true,
        transformMatrix: [scaleX, skewY, skewX, scaleY, 0, 0],
        left: left,
        top: top,
        hasControls: false,
        hasBorders: false,
        fill: color || defaultColor,
      })
      .scale(screenRatio)
      .rotate(rotateDegree);

    hiddenCanvas.setWidth(400).setHeight(400).clear().add(obj);
    var canvasCurrentMainObject = hiddenCanvas.getObjects();
    if (typeof canvasCurrentMainObject[0]._objects != "undefined") {
      var canvasCurrentObjects = canvasCurrentMainObject[0]._objects;
      for (
        var objCount = 0;
        objCount < canvasCurrentObjects.length;
        objCount++
      ) {
        canvasCurrentObjects[objCount].fill = color || defaultColor;
      }
    }

    $("#" + layerId).attr("src", hiddenCanvas.toDataURL());
    adjustScreenSize();
  });
  setLayerOrder(layerId, layerCanvas[layerId]["order"]);

  if (!isHover) {
    doSyncLayer(layerId);
    doSyncPreviewLayer(layerId);
  }
}

function initSlider() {
  $(".slider-range").each(function () {
    $(this).slider({
      tooltip: "hide",
    });
  });
}

function destroySlider() {
  $(".slider-range").each(function () {
    $(this).slider("destroy");
  });
}

// function disableSlider(disable) {
//     $("#rotateControl, #scaleXControl, #scaleYControl, #moveXControl, #moveYControl").slider(disable ? "disable" : "enable");
// }

function loadSkewSlider(layerId, min, max) {
  var skewPercent = layerCanvas[layerId].skew;

  skewPercent = skewPercent.split(",");
  var skewX = parseFloat(skewPercent[0]);
  var skewY = parseFloat(skewPercent[1]);

  var min_limit_skewing = parseFloat(min);
  var max_limit_skewing = parseFloat(max);

  min_limit_skewing =
    min_limit_skewing !== "" && !isNaN(min_limit_skewing)
      ? min_limit_skewing
      : "";
  max_limit_skewing =
    max_limit_skewing !== "" && !isNaN(max_limit_skewing)
      ? max_limit_skewing
      : "";

  var skewMin =
    min_limit_skewing !== "" && min_limit_skewing > 0 ? min_limit_skewing : 0;
  var skewMax =
    max_limit_skewing !== "" && max_limit_skewing > 0
      ? max_limit_skewing
      : 1 + Math.max.apply(null, SKEW_NODE_LIST) / 100;

  $("#skew-up-down")
    .slider({
      tooltip: "hide",
      id: "skew-up-down-control",
      step: SKEW_STEP,
      min: skewMin,
      max: skewMax,
      value: skewY,
      orientation: "vertical",
      reversed: true,
      formatter: function (value) {
        var tmpText = parseFloat(value) * 100;
        return parseFloat(tmpText).toFixed(0) + "%";
      },
    })
    .on("slide", function () {
      if (!IsMobileBrowser) {
        skew("y", this.value, layerId);
        var tmpText = parseFloat(this.value) * 100;
        $("#skew-up-down-value").val(parseFloat(tmpText).toFixed(0) + "%");
      }
    })
    .on("slideStop", function () {
      skew("y", this.value, layerId);
      var tmpText = parseFloat(this.value) * 100;
      $("#skew-up-down-value").val(parseFloat(tmpText).toFixed(0) + "%");
    });
  $("#skew-up-down-value").prop("min", skewMin).prop("max", skewMax);
  $("#skew-up-down-value")
    .prop("step", SCALE_STEP)
    .val(skewY * 100 + "%");

  $("#skew-left-right")
    .slider({
      tooltip: "hide",
      id: "skew-left-right-control",
      step: SKEW_STEP,
      min: skewMin,
      max: skewMax,
      value: skewX,
      formatter: function (value) {
        var tmpText = parseFloat(value) * 100;
        return parseFloat(tmpText).toFixed(0) + "%";
      },
    })
    .on("slide", function () {
      if (!IsMobileBrowser) {
        skew("x", this.value, layerId);
        var tmpText = parseFloat(this.value) * 100;
        $("#skew-left-right-value").val(parseFloat(tmpText).toFixed(0) + "%");
      }
    })
    .on("slideStop", function () {
      skew("x", this.value, layerId);
      var tmpText = parseFloat(this.value) * 100;
      $("#skew-left-right-value").val(parseFloat(tmpText).toFixed(0) + "%");
    });
  $("#skew-left-right-value").prop("min", skewMin).prop("max", skewMax);
  $("#skew-left-right-value")
    .prop("step", SKEW_STEP)
    .val(skewX * 100 + "%");
}

function loadScaleSlider(layerId, min, max) {
  var scalePercent = layerCanvas[layerId].scale;

  scalePercent = scalePercent.split(",");
  var scaleX = parseFloat(scalePercent[0]);
  var scaleY = parseFloat(scalePercent[1]);

  var min_limit_scaling = parseFloat(min);
  var max_limit_scaling = parseFloat(max);

  min_limit_scaling =
    min_limit_scaling !== "" &&
    !isNaN(min_limit_scaling) &&
    min_limit_scaling != 0
      ? min_limit_scaling
      : "";
  max_limit_scaling =
    max_limit_scaling !== "" &&
    !isNaN(max_limit_scaling) &&
    max_limit_scaling != 0
      ? max_limit_scaling
      : "";

  var scaleMin =
    min_limit_scaling !== "" && min_limit_scaling > 0 ? min_limit_scaling : 0;
  var scaleMax =
    max_limit_scaling !== "" && max_limit_scaling > 0
      ? max_limit_scaling
      : 1 + Math.max.apply(null, SCALE_NODE_LIST) / 100;

  $("#scale-up-down")
    .slider({
      tooltip: "hide",
      id: "scale-up-down-control",
      step: SCALE_STEP,
      min: scaleMin,
      max: scaleMax,
      value: scaleY,
      orientation: "vertical",
      reversed: true,
      formatter: function (value) {
        var tmpText = parseFloat(value) * 100;
        return parseFloat(tmpText).toFixed(0) + "%";
      },
    })
    .on("slide", function () {
      if (!IsMobileBrowser) {
        scale("y", this.value, layerId);
        var tmpText = parseFloat(this.value) * 100;
        $("#scale-up-down-value").val(parseFloat(tmpText).toFixed(0) + "%");
      }
    })
    .on("slideStop", function () {
      scale("y", this.value, layerId);
      var tmpText = parseFloat(this.value) * 100;
      $("#scale-up-down-value").val(parseFloat(tmpText).toFixed(0) + "%");
    });
  $("#scale-up-down-value").prop("min", scaleMin).prop("max", scaleMax);
  $("#scale-up-down-value")
    .prop("step", SCALE_STEP)
    .val(scaleY * 100 + "%");

  $("#scale-left-right")
    .slider({
      tooltip: "hide",
      id: "scale-left-right-control",
      step: SCALE_STEP,
      min: scaleMin,
      max: scaleMax,
      value: scaleX,
      formatter: function (value) {
        var tmpText = parseFloat(value) * 100;
        return parseFloat(tmpText).toFixed(0) + "%";
      },
    })
    .on("slide", function () {
      if (!IsMobileBrowser) {
        scale("x", this.value, layerId);
        var tmpText = parseFloat(this.value) * 100;
        $("#scale-left-right-value").val(parseFloat(tmpText).toFixed(0) + "%");
      }
    })
    .on("slideStop", function () {
      scale("x", this.value, layerId);
      var tmpText = parseFloat(this.value) * 100;
      $("#scale-left-right-value").val(parseFloat(tmpText).toFixed(0) + "%");
    });
  $("#scale-left-right-value").prop("min", scaleMin).prop("max", scaleMax);
  $("#scale-left-right-value")
    .prop("step", SCALE_STEP)
    .val(scaleX * 100 + "%");
}

function loadRotateSlider(layerId) {
  var rotateDegree = parseInt(layerCanvas[layerId].rotateDegree);

  rotateDegree = rotateDegree > ROTATION_MIN ? rotateDegree : ROTATION_MIN;
  rotateDegree = rotateDegree < ROTATION_MAX ? rotateDegree : ROTATION_MAX;

  $("#tool-rotate")
    .slider({
      id: "tool-rotate-control",
      tooltip: "hide",
      step: ROTATION_STEP,
      min: ROTATION_MIN,
      max: ROTATION_MAX,
      value: rotateDegree,
      formatter: function (value) {
        return value + "\xB0";
      },
    })
    .on("slide", function () {
      if (!IsMobileBrowser) {
        rotate(layerId, this.value);
        $("#tool-rotate-value").val(this.value + "\xB0");
      }
    })
    .on("slideStop", function () {
      rotate(layerId, this.value);
      $("#tool-rotate-value").val(this.value + "\xB0");
    });
  $("#tool-rotate-value").prop("min", ROTATION_MIN).prop("max", ROTATION_MAX);
  $("#tool-rotate-value")
    .prop("step", ROTATION_STEP)
    .val(rotateDegree + "\xB0");
}

function loadMoveSlider(layerId, min, max) {
  var position = layerCanvas[layerId].position;
  position = position.split(",");
  var left = parseFloat(position[0]);
  var top = parseFloat(position[1]);
  top = 0 - top;

  var min_limit_moving = parseFloat(min);
  var max_limit_moving = parseFloat(max);
  min_limit_moving =
    min_limit_moving !== "" && !isNaN(min_limit_moving) && min_limit_moving != 0
      ? min_limit_moving
      : "";
  max_limit_moving =
    max_limit_moving !== "" && !isNaN(max_limit_moving) && max_limit_moving != 0
      ? max_limit_moving
      : "";

  var moveMin =
    min_limit_moving !== ""
      ? min_limit_moving
      : Math.max.apply(null, MOVE_NODE_LIST);
  var moveMax =
    max_limit_moving !== ""
      ? max_limit_moving
      : Math.max.apply(null, MOVE_NODE_LIST);

  moveMin = 0 - moveMin;

  $("#move-up-down")
    .slider({
      id: "move-up-down-control",
      tooltip: "hide",
      step: MOVE_STEP,
      min: moveMin,
      max: moveMax,
      value: parseInt(top),
      orientation: "vertical",
      reversed: true,
      formatter: function (value) {
        return value + "px";
      },
    })
    .on("slide", function () {
      if (!IsMobileBrowser) {
        var moveDirection = this.value < 0 ? "down" : "up";
        // var moveValue = (parseInt(this.value) * moveMax) / 100;
        move(moveDirection, layerId, parseInt(this.value));
        $("#move-up-down-value").val(this.value + "px");
      }
    })
    .on("slideStop", function () {
      var moveDirection = this.value < 0 ? "down" : "up";
      // var moveValue = (parseInt(this.value) * moveMax) / 100;
      move(moveDirection, layerId, parseInt(this.value));
      $("#move-up-down-value").val(this.value + "px");
    });
  $("#move-up-down-value")
    .prop("step", MOVE_STEP)
    .val(parseInt(top) + "px");
  $("#move-up-down-value").prop("min", moveMin).prop("max", moveMax);

  // var leftInPercent = (left * 100) / moveMax;
  // leftInPercent = parseInt(leftInPercent);
  $("#move-left-right")
    .slider({
      id: "move-left-right-control",
      tooltip: "hide",
      step: MOVE_STEP,
      min: moveMin,
      max: moveMax,
      value: parseInt(left),
      formatter: function (value) {
        return value + "px";
      },
    })
    .on("slide", function () {
      if (!IsMobileBrowser) {
        var moveDirection = this.value < 0 ? "left" : "right";
        // var moveValue = (parseInt(this.value) * moveMax) / 100;
        move(moveDirection, layerId, parseInt(this.value));
        $("#move-left-right-value").val(this.value + "px");
      }
    })
    .on("slideStop", function () {
      var moveDirection = this.value < 0 ? "left" : "right";
      // var moveValue = (parseInt(this.value) * moveMax) / 100;
      move(moveDirection, layerId, parseInt(this.value));
      $("#move-left-right-value").val(this.value + "px");
    });
  $("#move-left-right-value")
    .prop("step", MOVE_STEP)
    .val(parseInt(left) + "px");
  $("#move-left-right-value").prop("min", moveMin).prop("max", moveMax);
}

// set range value
function up(step, slider) {
  var $sliderValue = $("#" + slider + "-value");

  switch (slider) {
    case "move-up-down":
      var max = $sliderValue.prop("max");
      var position = layerCanvas[layerId]["position"];
      var vertical = 0 - parseInt(position.split(",")[1]);
      vertical += step;
      vertical = vertical > max ? max : vertical;
      vertical = parseFloat(vertical);
      $("#" + slider).slider("setValue", vertical);
      $sliderValue.val(vertical + "px");
      move("up", layerId, vertical);
      break;
    case "scale-up-down":
      var layerScale = layerCanvas[layerId]["scale"];
      var scaleY = parseFloat(layerScale.split(",")[1]);
      var max = parseFloat($sliderValue.prop("max"));
      scaleY += step / 100;
      scaleY = scaleY > max ? max : scaleY;
      scaleY = parseFloat(scaleY);
      $("#" + slider).slider("setValue", scaleY);
      $sliderValue.val(scaleY.toFixed(2) * 100 + "%");
      scale("y", scaleY, layerId);
      break;
    case "skew-up-down":
      var layerSkew = layerCanvas[layerId]["skew"];
      var skewY = parseFloat(layerSkew.split(",")[1]);
      var max = parseFloat($sliderValue.prop("max"));
      skewY += step / 100;
      skewY = skewY > max ? max : skewY;
      skewY = parseFloat(skewY);
      $("#" + slider).slider("setValue", skewY);
      $sliderValue.val(skewY.toFixed(2) * 100 + "%");
      skew("y", skewY, layerId);
      break;
  }
}

function down(step, slider) {
  var $sliderValue = $("#" + slider + "-value");

  switch (slider) {
    case "move-up-down":
      var min = $sliderValue.prop("min");
      var position = layerCanvas[layerId]["position"];
      var vertical = 0 - parseInt(position.split(",")[1]);
      vertical -= step;
      vertical = vertical < min ? min : vertical;
      vertical = parseFloat(vertical);
      $("#" + slider).slider("setValue", vertical);
      $sliderValue.val(vertical + "px");
      move("up", layerId, vertical);
      break;
    case "scale-up-down":
      var layerScale = layerCanvas[layerId]["scale"];
      var scaleY = parseFloat(layerScale.split(",")[1]);
      var min = parseFloat($sliderValue.prop("min"));
      scaleY -= step / 100;
      scaleY = scaleY < min ? min : scaleY;
      scaleY = parseFloat(scaleY);
      $("#" + slider).slider("setValue", scaleY);
      $sliderValue.val(scaleY.toFixed(2) * 100 + "%");
      scale("y", scaleY, layerId);
      break;
    case "skew-up-down":
      var layerSkew = layerCanvas[layerId]["skew"];
      var skewY = parseFloat(layerSkew.split(",")[1]);
      var min = parseFloat($sliderValue.prop("min"));
      skewY -= step / 100;
      skewY = skewY < min ? min : skewY;
      skewY = parseFloat(skewY);
      $("#" + slider).slider("setValue", skewY);
      $sliderValue.val(skewY.toFixed(2) * 100 + "%");
      skew("y", skewY, layerId);
      break;
  }
}

function right(step, slider) {
  var $sliderValue = $("#" + slider + "-value");

  switch (slider) {
    case "move-left-right":
      var max = $sliderValue.prop("max");
      var position = layerCanvas[layerId]["position"];
      var horizontal = parseFloat(position.split(",")[0]);
      horizontal += step;
      horizontal = horizontal > max ? max : horizontal;
      horizontal = parseFloat(horizontal);
      $("#" + slider).slider("setValue", horizontal);
      $sliderValue.val(horizontal + "px");
      move("right", layerId, horizontal);
      break;
    case "scale-left-right":
      var layerScale = layerCanvas[layerId]["scale"];
      var scaleX = parseFloat(layerScale.split(",")[0]);
      var max = parseFloat($sliderValue.prop("max"));
      scaleX += step / 100;
      scaleX = scaleX > max ? max : scaleX;
      scaleX = parseFloat(scaleX);
      $("#" + slider).slider("setValue", scaleX);
      $sliderValue.val(scaleX.toFixed(2) * 100 + "%");
      scale("x", scaleX, layerId);
      break;
    case "skew-left-right":
      var layerSkew = layerCanvas[layerId]["skew"];
      var skewX = parseFloat(layerSkew.split(",")[0]);
      var max = parseFloat($sliderValue.prop("max"));
      skewX += step / 100;
      skewX = skewX > max ? max : skewX;
      skewX = parseFloat(skewX);
      $("#" + slider).slider("setValue", skewX);
      $sliderValue.val(skewX.toFixed(2) * 100 + "%");
      skew("x", skewX, layerId);
      break;
  }
}

function left(step, slider) {
  var $sliderValue = $("#" + slider + "-value");

  switch (slider) {
    case "move-left-right":
      var min = $sliderValue.prop("min");
      var position = layerCanvas[layerId]["position"];
      var horizontal = parseFloat(position.split(",")[0]);
      horizontal -= step;
      horizontal = horizontal < min ? min : horizontal;
      horizontal = parseFloat(horizontal);
      $("#" + slider).slider("setValue", horizontal);
      $sliderValue.val(horizontal + "px");
      move("left", layerId, horizontal);
      break;
    case "scale-left-right":
      var layerScale = layerCanvas[layerId]["scale"];
      var scaleX = parseFloat(layerScale.split(",")[0]);
      var min = parseFloat($sliderValue.prop("min"));
      scaleX -= step / 100;
      scaleX = scaleX < min ? min : scaleX;
      scaleX = parseFloat(scaleX);
      $("#" + slider).slider("setValue", scaleX);
      $sliderValue.val(scaleX.toFixed(2) * 100 + "%");
      scale("x", scaleX, layerId);
      break;
    case "skew-left-right":
      var layerSkew = layerCanvas[layerId]["skew"];
      var skewX = parseFloat(layerSkew.split(",")[0]);
      var min = parseFloat($sliderValue.prop("min"));
      skewX -= step / 100;
      skewX = skewX < min ? min : skewX;
      skewX = parseFloat(skewX);
      $("#" + slider).slider("setValue", skewX);
      $sliderValue.val(skewX.toFixed(2) * 100 + "%");
      skew("x", skewX, layerId);
      break;
  }
}

// end move slider

// rotate slider

function rotate_right(step, slider) {
  var $sliderValue = $("#" + slider + "-value");

  var tmpVal = parseFloat($sliderValue.val());
  var val = parseFloat(layerCanvas[layerId].rotateDegree);
  var min = parseFloat($sliderValue.prop("min"));
  var max = parseFloat($sliderValue.prop("max"));
  if (val <= min || val >= max) val = tmpVal;
  val += step;
  val = val > max ? max : val;
  val = parseFloat(val);
  $("#" + slider).slider("setValue", val);
  $sliderValue.val(val.toFixed(0) + "\xB0");
  rotate(layerId, val);
}

function rotate_left(step, slider) {
  var $sliderValue = $("#" + slider + "-value");
  var tmpVal = parseFloat($sliderValue.val());
  var val = parseFloat(layerCanvas[layerId].rotateDegree);
  var min = parseFloat($sliderValue.prop("min"));
  var max = parseFloat($sliderValue.prop("max"));
  if (val <= min || val >= max) val = tmpVal;
  val -= step;
  val = val < min ? min : val;
  val = parseFloat(val);
  $("#" + slider).slider("setValue", val);
  $sliderValue.val(val.toFixed(0) + "\xB0");
  rotate(layerId, val);
}
// end rotate tool

function getMaxMinLayerData(layerId, isLoadSliderValue) {
  if (layerId == "background-color") {
    return false;
  }

  startLoading();
  $.ajax({
    url: wpParams.ajax_url + "?action=getLayerMinMaxData",
    type: "GET",
    data: {
      layerId: layerCanvas[layerId].id.toString().split("-")[0],
    },
  }).done(function (resp) {
    resp = JSON.parse(resp);
    if (!resp.success) {
      alert(resp.message);
      return false;
    }

    $("#maxMinData_" + layerId).val(JSON.stringify(resp.message));
    if (isLoadSliderValue) {
      loadSliderValues(layerId);
    }
    stopLoading();
  });
}

function loadSliderValues(layerId) {
  if (layerId == "background-color") {
    return false;
  }

  var maxMinData = $("#maxMinData_" + layerId).val();
  if (!maxMinData.length) {
    getMaxMinLayerData(layerId, true);
    return false;
  }

  maxMinData = JSON.parse(maxMinData);

  destroySlider();

  loadMoveSlider(
    layerId,
    maxMinData.min_limit_moving,
    maxMinData.max_limit_moving
  );
  loadRotateSlider(layerId);
  loadScaleSlider(
    layerId,
    maxMinData.min_limit_scaling,
    maxMinData.max_limit_scaling
  );
  loadSkewSlider(
    layerId,
    maxMinData.min_limit_skewing,
    maxMinData.max_limit_skewing
  );
}

function upsertProduct(el) {
  var type = $(el).val();
  if (["create", "update"].indexOf(type) < 0) {
    alert("No action as request: " + type);
    return false;
  }

  var productID = $("#productID").val() || getURLParameter("product_id");
  if (productID.length < 1) {
    alert("Please select product");
    return false;
  }
  // Get cat list
  var categories = [];
  if ($("input[name='categories[]']:checked").length < 1) {
    alert("Please select at least one category.");
    return false;
  } else {
    $("input[name='categories[]']:checked").each(function () {
      categories.push($(this).val());
    });
  }
  // Get thickness list
  var thicknesses = [];
  // Get shape list
  var shapeAndSize = {};
  if (
    !$('input[name="shapes"]:checked').val() ||
    !$('input[name="sizes"]:checked').val()
  ) {
    alert("Please select shape and its size.");
    return false;
  }
  shapeAndSize[$('input[name="shapes"]:checked').val()] = {
    name: $('input[name="shapes"]:checked').val(),
  };
  if ($('input[name="product_design_type"]:checked').val() !== "fixed") {
    var _size = [];
    $('input[name="sizes"]').each(function () {
      if (this.checked) {
        _size.push($(this).val());
      }
    });
    if (_size.length < 1) {
      alert("Please select size.");
      return false;
    }
    shapeAndSize[$('input[name="shapes"]:checked').val()]["size"] = _size;
  } else {
    shapeAndSize[$('input[name="shapes"]:checked').val()]["size"] = $(
      'input[name="sizes"]:checked'
    ).val();
    // if (!$('#fixedThicknessManagerSel').val()) {
    //     alert('Please select thickness.');
    //     return false;
    // }
    // thicknesses.push($('#fixedThicknessManagerSel').val());
  }
  // Get priceObject
  var priceObject = {};
  var productDesignType = $('input[name="product_design_type"]:checked').val();
  if (["fixed", "color-only"].includes(productDesignType)) {
    /*
     * Get and make priceObject
     */
    // Validate pricePerTile
    var pricePerTile = parseFloat($("#defaultPriceBlock .pricePerTile").val());
    if (pricePerTile === "" || pricePerTile < 1) {
      alert("Please enter valid price per tile");
      return false;
    }
    priceObject["pricePerTile"] = pricePerTile;

    // Validate pricePerM2
    var pricePerM2 = parseFloat($("#defaultPriceBlock .pricePerM2").val());
    if (pricePerM2 === "" || pricePerM2 < 1) {
      alert("Please enter valid price per m2");
      return false;
    }
    priceObject["pricePerM2"] = pricePerM2;

    // Validate priceStep
    var priceStep = parseFloat($("#defaultPriceBlock .priceStep").val());
    if (priceStep === "" || priceStep < 1) {
      alert("Please enter valid price step");
      return false;
    }
    priceObject["priceStep"] = priceStep;

    priceObject["priceAreaPair"] = {};
    var areaList = [];
    var isFulFillPriceAreaPair = 1;
    // Validate priceAreaPair
    $("#priceAreaBlock .priceAreaPair").each(function (index, value) {
      var price = $(value).find("input[name=price]").val();
      var area = $(value).find("input[name=area]").val();

      if (
        price === "" ||
        area === "" ||
        parseFloat(area) < 1 ||
        parseFloat(price) < 0
      ) {
        isFulFillPriceAreaPair *= 0;
      }

      priceObject["priceAreaPair"][area] = price;
      areaList.push(area);
    });
    if (!isFulFillPriceAreaPair) {
      alert(
        "Please fulfill valid pair of price and area. (Price or area must be greater than 0)"
      );
      return false;
    }
  }

  var minOrder = Math.min.apply(null, areaList);
  // Validate delivery
  var delivery = $("#delivery").val();
  var packaging = $("#packaging").val();
  var customDesign = $("#customDesignInput").val() || "";
  var subText1 = $("#subtext-1").val() || "";
  var subText2 = $("#subtext-2").val() || "";
  var subText3 = $("#subtext-3").val() || "";
  var weight = parseFloat($("#weightInput").val()) || 0.0;
  var productTitle = $("#productTitle").val();
  var productDesignType = $('input[name="product_design_type"]:checked').val();
  var productManufacturer = $('input[name="manufacturer"]:checked').val();
  var productTerazzo = $("#terazoOpt").prop("checked") ? 1 : 0;
  // crossOrigin="Anonymous"
  startLoading();
  // if(checkIsSafariBrowser()) {
  window.scrollTo(0, 0);
  html2canvas($("#layerCanvas"), { useCORS: true })
    .then((canvas) => {
      $(canvas).prop("id", "testCanvas").addClass("hide");
      document.body.appendChild(canvas);
      var _canvas = document.getElementById("testCanvas");
      $.ajax({
        url: wpParams.ajax_url + "?action=upsertProduct&type=" + type,
        type: "POST",
        data: {
          layerCanvas: layerCanvas,
          productID: productID,
          categories: categories,
          shapeAndSize: shapeAndSize,
          productTitle: productTitle,
          featuredImage:
            _canvas.toDataURL() || $("#productFeaturedImage").val(),
          priceObject: priceObject,
          minOrder: minOrder,
          thicknesses: thicknesses,
          delivery: delivery,
          packaging: packaging,
          customDesign: customDesign,
          weight: weight,
          terazzo: productTerazzo,
          subText1: subText1,
          subText2: subText2,
          subText3: subText3,
          productDesignType: productDesignType,
          productManufacturer: productManufacturer,
        },
      }).done(function (resp) {
        $("#testCanvas").remove();
        resp = JSON.parse(resp);
        if (!resp.success) {
          alert(resp.message);
          stopLoading();
          return false;
        }
        window.location.href = resp.message;
        stopLoading();
      });
    })
    .catch(function (error) {
      alert("Error when upsert design: " + error.toString());
      $("#testCanvas").remove();
    })
    .finally(function () {
      window.scrollTo(0, document.body.scrollHeight);
      stopLoading();
    });
  // } else {
  //   var nodeLayer = document.getElementById("layerCanvas");
  //   domtoimage
  //     .toPng(nodeLayer)
  //     .then(function(dataUrl) {
  //       $.ajax({
  //         url: wpParams.ajax_url + "?action=upsertProduct&type=" + type,
  //         type: "POST",
  //         data: {
  //           layerCanvas: layerCanvas,
  //           productID: productID,
  //           categories: categories,
  //           shapeAndSize: shapeAndSize,
  //           productTitle: productTitle,
  //           featuredImage: dataUrl || $("#productFeaturedImage").val(),
  //           priceObject: priceObject,
  //           minOrder: minOrder,
  //           thicknesses: thicknesses,
  //           delivery: delivery,
  //           packaging: packaging,
  //           customDesign: customDesign,
  //           weight: weight,
  //           terazzo: productTerazzo,
  //           subText1: subText1,
  //           subText2: subText2,
  //           subText3: subText3,
  //           productDesignType: productDesignType,
  //           productManufacturer: productManufacturer
  //         }
  //       }).done(function(resp) {
  //         resp = JSON.parse(resp);
  //         if (!resp.success) {
  //           alert(resp.message);
  //           stopLoading();
  //           return false;
  //         }
  //         window.location.href = resp.message;
  //         stopLoading();
  //       });
  //     })
  //     .catch(function(error) {
  //       alert("Error when upsert design: " + error.toString());
  //     }).finally(function() {
  //       stopLoading();
  //     });
  // }
}

function showTabBg() {
  $("#tool-layer").removeClass("active");
  $("#tool-bg-layer").addClass("active");
  $("#pane-bg-color").addClass("active");
}

function hideTabBg() {
  $("#tool-layer").addClass("active");
  $("#tool-bg-layer").removeClass("active");
  $("#pane-bg-color").removeClass("active");
}

function unActiveAllPatternItem() {
  $("li.btn-pattern-item").removeClass("active");
  var coverSrc =
    $("#imageUrlOfOnSmallPreviewShape").val() ||
    $("#defaultImageUrlOfOnSmallPreviewShape").val();
  $(".small-preview-layer-cover").each(function () {
    $(this).attr("src", coverSrc);
  });
  loadImageOnTopBackground("#imageUrlOfOnBackgroundShape") ||
    loadImageOnTopBackground("#defaultImageUrlOfOnBackgroundShape");
}

// color tooltip
function closeColorTooltip() {
  $("#tooltip-color-box").remove();
}

function submitColorTooltip() {
  var el = $('.color-item[data-main-color="' + currentSelectedColor + '"]')[0];
  if (layerId == "background-color") {
    layerCanvas["background-color"] = currentSelectedColor;
    $("#layerCanvas-bg").css("background-color", currentSelectedColor);
    $(".tab-color-background").css("background-color", currentSelectedColor);
    doSyncLayer(layerId);
    // $("#pattern-list .lower-canvas").css(
    //   "background-color",
    //   currentSelectedColor
    // );
    $.each(layerCanvas, function (index, value) {
      if (index !== "background-color") {
        $("#smallPreviewImage_" + index).css(
          "background-color",
          layerCanvas["background-color"]
        );
      }
    });
  } else {
    $(".btn-layer[data-layer-id=" + layerId + "]").attr(
      "data-hover-color",
      $(el).data("hover-color")
    );
    $(".tab-color-layer").css("background-color", currentSelectedColor);
    fillColor(currentSelectedColor, layerId);
  }

  if (layerId != "background-color") {
    var coverSrc =
      $("#imageUrlOfOnSmallPreviewHoverShape").val() ||
      $("#defaultImageUrlOfOnSmallPreviewHoverShape").val();
    $("#small-preview-layer-cover-" + layerId).attr("src", coverSrc);
  } else {
    loadImageOnTopBackground("#imageUrlOfOnBackgroundHoverShape") ||
      loadImageOnTopBackground("#defaultImageUrlOfOnBackgroundHoverShape");
  }

  if (recentColorList.length <= RECENT_COLOR_MAXIMUM) {
    var isExistedRecentColor = false;
    $.each(recentColorList, function (k, v) {
      if (v.color == currentSelectedColor) {
        isExistedRecentColor = true;
        return false;
      }
    });

    if (!isExistedRecentColor) {
      if (recentColorList.length == RECENT_COLOR_MAXIMUM) {
        recentColorList.pop();
      }

      recentColorList.unshift({
        hover: $(el).data("hover-color"),
        color: currentSelectedColor,
      });
    }
  }

  reCalculateColorPrice();
  // closeColorTooltip();
  // renderColorsInUsed(layerId);
  $('input[name="color"]')
    .removeClass("colorChecked")
    .addClass("colorUnchecked");
  // $('#color-tab span').css('background-color', currentSelectedColor);
  if (layerId != "background-color") {
    doSyncLayer(layerId);
    doSyncPreviewLayer(layerId);
  }

  currentSelectedColor = "";
  $.each(layerCanvas, function (index, value) {
    if (index !== "background-color") {
      $("#smallPreviewImage_" + index).css(
        "background-color",
        layerCanvas["background-color"]
      );
    }
  });

  if (layerId != "background-color") {
    onSelectPatternItem("a[data-layer-id='" + layerId + "']", layerCanvas[layerId]['color']);
  } else {
    onSelectPatternItem("#layerBackground", layerCanvas['background-color']);
  }

  // $(el)
  //   .parents(".lbl-item")
  //   .children('input[name="color"]')
  //   .removeClass("colorUnchecked")
  //   .addClass("colorChecked");
}

function getGallerias(templateConfigurationDataVal) {
  $.ajax({
    url: wpParams.ajax_url + "?action=getGalleries",
    type: "GET",
    data: {
      templateConfigurationData: templateConfigurationDataVal,
    },
  }).done(function (resp) {
    resp = JSON.parse(resp);
    if (!resp.success) {
      alert("FALSE");
      return;
    }

    var baseCustomStyle =
      "background-position: center; background-size: contain; background-repeat: no-repeat;";
    var html = "";
    $.each(resp.message, function (i, v) {
      var customStyle = baseCustomStyle + "background-image: url(" + v + ");";
      html +=
        '<option value="' +
        i +
        '" data-custom-style="' +
        customStyle +
        '"></option>';
    });

    $("#dropdownPattern-list-popup").html(html);
    $("#dropdownPattern-list-popup").niceSelect("update");
  });
}

function onChangeSizeVisualizer(el) {
  var sizeWithRatioVisualizer = $("#sizeWithRatioVisualizer").val();
  sizeWithRatioVisualizer = JSON.parse(sizeWithRatioVisualizer);

  var sizeSlug = $(el).val();
  var sizeVisualizerRatio = sizeWithRatioVisualizer[sizeSlug];
  $("#fragmentPreview > #wrap > .zoom-img-visualizer").css(
    "transform",
    "scale(" + sizeVisualizerRatio + ")"
  );
}

function _doCaptureWithHtml2CanvasForVisualizer(fragmentPodItemTitle) {
  startLoading();
  window.scrollTo(0, 0);
  // virtual cropping: infact dcrease the size of the canvas layers div to eliminate the lines
  //var innerOuterDistance = 19 + CROP_PIXEL;
  //var canvasLayersSize = FRAGMENT_IMAGE_SIZE * 4 - innerOuterDistance;
  //$("#layerCanvas-bg").width(canvasLayersSize);
  //$("#layerCanvas-bg").height(canvasLayersSize);
  //console.log(canvasLayersSize);

  html2canvas($("#layerCanvas"), { useCORS: true })
    .then((canvas) => {
      $(canvas).prop("id", "testCanvas").addClass("hide");
      document.body.appendChild(canvas);
      var _canvas = document.getElementById("testCanvas");
      var _imgSrc = _canvas.toDataURL();
      var img = document.getElementById("hiddenImage");
      img.src = _imgSrc;
      img.onload = function () {
        var canvas = document.getElementById("hidden-canvas");
        var context = canvas.getContext("2d");
        canvas.width = FRAGMENT_IMAGE_SIZE * 4;
        canvas.height = FRAGMENT_IMAGE_SIZE * 4;

        var imageData = {};
        context.drawImage(
          img,
          0,
          0,
          canvas.width,
          canvas.height,
          0,
          0,
          canvas.width,
          canvas.height
        );

        imageData = context.getImageData(0, 0, canvas.width, canvas.height);

        for (var i = 0; i < imageData.data.length; i += 4) {
          var r = imageData.data[i];
          var g = imageData.data[i + 1];
          var b = imageData.data[i + 2];

          //if it is pure white, change its alpha to 0
          if (
            (b == 255 && g == 255 && r == 255) ||
            (b == 170 && g == 172 && r == 171)
          ) {
            // if (b != 191 && g != 183 && r != 130)  {
            imageData.data[i + 3] = 0;
          }
        }
        context.putImageData(imageData, 0, 0);

        $("#visualizerImageDemo").attr("src", canvas.toDataURL());
        $.ajax({
          url: wpParams.ajax_url + "?action=getHtmlFragment",
          type: "POST",
          data: {
            templateConfigurationData: $("#templateConfigurationData").val(),
            fragmentPodItemTitle: fragmentPodItemTitle,
            imgSrc: canvas.toDataURL(),
          },
        }).done(function (resp) {
          $("#testCanvas").remove();
          resp = JSON.parse(resp);
          if (!resp.success) {
            stopLoading();
            alert("FALSE");
            return;
          }

          var respMess = resp.message;
          var imgSrc = respMess.imgSrc;

          if (respMess.htmlFragment == "") {
            $("#fragmentContent").html(
              '<p class="text-primary">There is no fragment html template for selected thumbs. Please contact admin for more detail.</p>'
            );
          } else {
            // $("#fragmentContent").html(respMess.fragmentContent);
            $("#fragmentContent").load(respMess.htmlFragment, function () {
              $("#fragmentPreview img").each(function (i, v) {
                v.src = imgSrc;

                if (checkIsSafariBrowser()) {
                  v.width = FRAGMENT_IMAGE_SIZE;
                  v.height = FRAGMENT_IMAGE_SIZE;
                } else {
                  v.width =
                    v.width != "" && v.width != undefined
                      ? v.width
                      : FRAGMENT_IMAGE_SIZE;
                  v.height =
                    v.height != "" && v.height != undefined
                      ? v.height
                      : FRAGMENT_IMAGE_SIZE;
                }
              });
              $("#sizeVisualizerSelect").trigger("change");
            });
          }

          $("#grid-layout").removeClass("hide").addClass("show");
          $("#mainCanvasTileShadowLayout").show();
          stopLoading();
        });
      };
    })
    .catch(function (error) {
      $("#grid-layout").removeClass("hide").addClass("show");
      console.error("Error when loading image for visualizer ", error);
      stopLoading();
      $("#testCanvas").remove();
    })
    .finally(function () {
      window.scrollTo(0, document.body.scrollHeight);
      // $("#layerCanvas").width(FRAGMENT_IMAGE_SIZE * 4 + innerOuterDistance);
      // $("#layerCanvas").height(FRAGMENT_IMAGE_SIZE * 4 + innerOuterDistance);
      stopLoading();
    });
}

function _doCaptureWithDomToImage(fragmentPodItemTitle) {
  startLoading();
  var nodeLayer = document.getElementById("layerCanvas");
  domtoimage
    .toPng(nodeLayer)
    .then(function (dataUrl) {
      var img = document.getElementById("hiddenImage");
      img.src = dataUrl;
      img.onload = function () {
        var canvas = document.getElementById("hidden-canvas");
        var context = canvas.getContext("2d");
        canvas.width = FRAGMENT_IMAGE_SIZE * 4;
        canvas.height = FRAGMENT_IMAGE_SIZE * 4;
        context.drawImage(img, 0, 0, canvas.width, canvas.height);
        var imageData = context.getImageData(0, 0, img.width, img.height);
        for (var i = 0; i < imageData.data.length; i += 4) {
          var r = imageData.data[i];
          var g = imageData.data[i + 1];
          var b = imageData.data[i + 2];

          //if it is pure white, change its alpha to 0
          if (
            (b == 255 && g == 255 && r == 255) ||
            (b == 170 && g == 172 && r == 171)
          ) {
            // if (b != 191 && g != 183 && r != 130)  {
            imageData.data[i + 3] = 0;
          }
        }
        context.putImageData(imageData, 0, 0);

        $("#visualizerImageDemo").attr("src", canvas.toDataURL());

        $.ajax({
          url: wpParams.ajax_url + "?action=getHtmlFragment",
          type: "POST",
          data: {
            templateConfigurationData: $("#templateConfigurationData").val(),
            fragmentPodItemTitle: fragmentPodItemTitle,
            imgSrc: canvas.toDataURL(),
          },
        }).done(function (resp) {
          resp = JSON.parse(resp);
          if (!resp.success) {
            alert("FALSE");
            stopLoading();
            return;
          }

          var respMess = resp.message;
          var imgSrc = respMess.imgSrc;

          if (respMess.htmlFragment == "") {
            $("#fragmentContent").html(
              '<p class="text-primary">There is no fragment html template for selected thumbs. Please contact admin for more detail.</p>'
            );
          } else {
            $("#fragmentContent").load(respMess.htmlFragment, function () {
              $("#fragmentPreview img").each(function (i, v) {
                v.src = imgSrc;

                if (checkIsSafariBrowser()) {
                  v.width = FRAGMENT_IMAGE_SIZE;
                  v.height = FRAGMENT_IMAGE_SIZE;
                } else {
                  v.width =
                    v.width != "" && v.width != undefined
                      ? v.width
                      : FRAGMENT_IMAGE_SIZE;
                  v.height =
                    v.height != "" && v.height != undefined
                      ? v.height
                      : FRAGMENT_IMAGE_SIZE;
                }
              });
            });
          }

          $("#grid-layout").removeClass("hide").addClass("show");
          $("#sizeVisualizerSelect").trigger("change");
          $("#tileShadowLayout")
            .parents(".canvas-container")
            .addClass("show")
            .removeClass("hide");
          stopLoading();
        });
      };
    })
    .catch(function (error) {
      $("#grid-layout").removeClass("hide").addClass("show");
      console.error("Error when loading image for visualizer ", error);
      stopLoading();
    })
    .finally(function () {
      stopLoading();
    });
}

function changeFragmentHtml(fragmentPodItemTitle) {
  $("#dropdownPattern-list-popup").next(".nice-select").find("span").remove();
  $("#dropdownPattern-list-popup")
    .next(".nice-select")
    .prepend('<span class="current">Pattern</span>');

  $("#grid-layout").removeClass("show").addClass("hide");

  var $selectedOpt = $(
    '#dropdownPattern-list-popup > option[value="' + fragmentPodItemTitle + '"]'
  );
  var imgUrl = $selectedOpt.data("img-url");
  $("#visualizerFragmentIconSelection").attr("src", imgUrl);

  $("#mainCanvasTileShadowLayout").hide();

  _doCaptureWithHtml2CanvasForVisualizer(fragmentPodItemTitle);
}

function onSelectPatternItem(el, color) {
  unActiveAllPatternItem();
  $('input[name="color"]')
    // .prop("checked", false)
    .removeClass("colorChecked")
    .addClass("colorUnchecked");
  var selectedColor = "";
  var selectedColorTab = "";
  var selectedRecentColorTab = "";
  if ($(el).attr("id") == "layerBackground") {
    layerId = "background-color";
    selectedColor = layerCanvas[layerId];
    selectedColorTab = "#background-color-box";
    selectedRecentColorTab = "#recent-bg-color-list";
    showTabBg();
  } else {
    layerId = $(el).data("layer-id");
    // tmpHoverColor = layerCanvas[layerId]["color"];
    // // if (IsMobileBrowser) {
    // //   tmpHoverColor = layerCanvas[layerId]["color"];
    // // }
    console.log(color, layerCanvas[layerId]["color"], tmpHoverColor);
    selectedColor = color || tmpHoverColor;
    selectedColorTab = "#color-box";
    selectedRecentColorTab = "#recent-color-list";
    hideTabBg();
    $("#color-tab").trigger("click");
  }

  $(".btn-pattern-item").removeClass("active");
  if (layerId != "background-color") {
    $('li.btn-pattern-item[data-layer-id="' + layerId + '"]').addClass(
      "active"
    );
    var coverSrc =
      $("#imageUrlOfOnSmallPreviewHoverShape").val() ||
      $("#defaultImageUrlOfOnSmallPreviewHoverShape").val();
    $("#small-preview-layer-cover-" + layerId).attr("src", coverSrc);
  } else {
    $(el).addClass("active");
    loadImageOnTopBackground("#imageUrlOfOnBackgroundHoverShape") ||
      loadImageOnTopBackground("#defaultImageUrlOfOnBackgroundHoverShape");
  }

  if (typeof layerCanvas[layerId] == "undefined") {
    return false;
  }

  if (layerId != "background-color") {
    doSyncLayer(layerId);
    doSyncPreviewLayer(layerId);
  } else {
    $(".tab-color-background").css(
      "background-color",
      layerCanvas["background-color"]
    );
  }

  renderColorsInUsed(layerId);

  var productDesignType = $("#productDesignType").val();
  // if (productDesignType == 'customized' || isManager || layerId != 'background-color') loadSliderValues(layerId);
  if (
    ["customized", "color-only"].includes(productDesignType) ||
    layerId != "background-color"
  ) {
    loadSliderValues(layerId);
  }

  var selectedColorItemEle = $(
    selectedColorTab +
      " .row:not(" +
      selectedRecentColorTab +
      ') .color-item[data-main-color="' +
      selectedColor +
      '"]'
  )[0];
  $(selectedColorItemEle)
    .parents(".lbl-item")
    .children('input[name="color"]')
    // .prop("checked", true)
    .removeClass("colorUnchecked")
    .addClass("colorChecked");

  makeTooltipForColorsInUsed();
}

function onRemovePatternBtn(el) {
  if (layerId == "" || layerId == "background-color") {
    alert("Please choose layer first.");
    return false;
  }

  delete layerCanvas[layerId];
  delete resetCanvas[layerId];
  checkShowAddAsNewToCartBtn();

  $(
    "#pattern-list li.btn-pattern-item[data-layer-id=" + layerId + "]"
  ).remove();
  $("#layerCanvas")
    .find("img#" + layerId + "")
    .remove();
  $(".customize-product").removeClass("show").addClass("hide");
  $("#color-tab").trigger("click");

  var resetOrderCount = 1;
  var layerCount = Object.keys(layerCanvas).length - 1;
  $.each(layerCanvas, function (index, value) {
    if (index !== "background-color") {
      layerCanvas[index]["order"] = resetOrderCount;
      setLayerOrder(index, resetOrderCount);
      if (resetOrderCount < layerCount) resetOrderCount++;
    }
  });
  order_count = layerCount === 0 ? layerCount : resetOrderCount;
  layerId = "";
  reCalculateColorPrice();
  onSelectPatternItem("#layerBackground", layerCanvas["background-color"]);
}

function onCopyPatternBtn(el) {
  if (layerId == "" || layerId == "background-color") {
    alert("Please choose layer first.");
    return false;
  }

  var currentSelectedColorObj =
    $('input[name="color"].colorChecked') || $('input[name="color"]:checked');
  var parSelectedColorObj = $(currentSelectedColorObj).parents(".lbl-item");
  var selectedColor = $(parSelectedColorObj)
    .find(".color-item")
    .data("main-color");
  var tmpDuplicateLayer = JSON.parse(JSON.stringify(layerCanvas[layerId]));
  tmpDuplicateLayer.color = selectedColor || tmpHoverColor;
  var tmpID = tmpDuplicateLayer.id.toString();
  var _layerID = tmpID.split("-")[0];
  var maxDuplicateId = 0;
  $.each(layerCanvas, function (key, value) {
    if (value.id != undefined) {
      tmpID = value.id.toString();
      if (key !== "background-color" && tmpID.split("-")[0] == _layerID) {
        if (value.duplicateCounter > maxDuplicateId) {
          maxDuplicateId = value.duplicateCounter;
        }
      }
    }
  });

  var cloneDuplicateLayer = JSON.parse(
    JSON.stringify(
      layerCanvas[
        "child-canvas-" +
          _layerID +
          (maxDuplicateId > 0 ? "-" + maxDuplicateId.toString() : "")
      ]
    )
  );
  cloneDuplicateLayer.id = _layerID + "-" + (maxDuplicateId + 1).toString();
  cloneDuplicateLayer.duplicateID = parseInt(_layerID);
  cloneDuplicateLayer.order = ++order_count;
  cloneDuplicateLayer.duplicateCounter = maxDuplicateId + 1;
  var cloneTmpName = cloneDuplicateLayer.name.toString();
  cloneDuplicateLayer.name =
    cloneTmpName.split("-")[0] + "-" + (maxDuplicateId + 1).toString();

  var duplicateLayer = JSON.parse(JSON.stringify(cloneDuplicateLayer));
  duplicateLayer.position = tmpDuplicateLayer.position;
  duplicateLayer.rotate = tmpDuplicateLayer.rotate;
  duplicateLayer.scale = tmpDuplicateLayer.scale;
  duplicateLayer.skew = tmpDuplicateLayer.skew;
  duplicateLayer.color = tmpDuplicateLayer.color;

  var tmpLayerKey = "child-canvas-" + duplicateLayer.id;
  var tmpSmallPreviewLayerCoverHtml = $(
    "#small-preview-layer-cover-" + layerId
  ).clone();
  $(tmpSmallPreviewLayerCoverHtml).attr(
    "id",
    "small-preview-layer-cover-child-canvas-" + duplicateLayer.id
  );

  var html =
    '<li class="btn-pattern-item" data-layer-id="child-canvas-' +
    duplicateLayer.id +
    '">';
  html +=
    "<input type='hidden' id='maxMinData_child-canvas-" +
    duplicateLayer.id +
    "' value='" +
    $("#maxMinData_" + layerId).val() +
    "' />";
  html +=
    '<a title="' +
    duplicateLayer.name +
    '" href="javascript:void(0);" onclick="onSelectPatternItem(this)" data-layer-id="child-canvas-' +
    duplicateLayer.id +
    '">';
  html +=
    '<img src="' +
    $("#smallPreviewImage_" + layerId).attr("src") +
    '" id="smallPreviewImage_child-canvas-' +
    duplicateLayer.id +
    '" style="' +
    $("#smallPreviewImage_" + layerId).attr("style") +
    '" />';
  html += '<div class="icon-showmenu"></div>';
  html += '<div class="pattern-action-menu">';
  html +=
    '<button class="action-menu-up-down" onclick="onMoveUpPatternBtn(this)"><i class="ti-angle-up"></i></button>';
  html += '<button class="btn-copy" onclick="onCopyPatternBtn(this)"></button>';
  html +=
    '<button class="btn-trash" onclick="onRemovePatternBtn(this)"></button>';
  html +=
    '<button class="action-menu-up-down" onclick="onMoveDownPatternBtn(this)"><i class="ti-angle-down"></i></button>';
  html += "</div>";
  html += $(tmpSmallPreviewLayerCoverHtml).get(0).outerHTML;
  html += "</a>";
  html += "</li>";
  $("#pattern-list").prepend(html);
  $("#layerCanvas").append(
    '<img id="child-canvas-' +
      duplicateLayer.id +
      '" width="' +
      tmpSize +
      '" height="' +
      tmpSize +
      '" />'
  );

  layerCanvas[tmpLayerKey] = duplicateLayer;
  resetCanvas[tmpLayerKey] = JSON.parse(
    JSON.stringify(layerCanvas[tmpLayerKey])
  );
  drawLayer(tmpLayerKey);
  doSyncLayer(tmpLayerKey);
  reCalculateColorPrice();
}

function onMoveUpPatternBtn(el) {
  var currentSelectedColorObj =
    $('input[name="color"].colorChecked') || $('input[name="color"]:checked');
  var parSelectedColorObj = $(currentSelectedColorObj).parents(".lbl-item");
  var selectedColor = $(parSelectedColorObj)
    .find(".color-item")
    .data("main-color");

  layerCanvas[layerId]["color"] = tmpHoverColor || selectedColor;
  var current_order = layerCanvas[layerId]["order"];
  current_order = parseInt(current_order);
  var tmp_order = current_order;

  var orderArr = [];
  $.each(layerCanvas, function (key, value) {
    if (key !== "background-color") {
      orderArr.push(parseInt(value.order));
    }
  });
  var maxOrder = Math.max.apply(null, orderArr);

  if (current_order + 1 > maxOrder) {
    alert("Selected layer is reach the top level. Cannot move to top no more.");
    return false;
  }
  tmp_order += 1;

  var tmp_layerID = getLayerIdByAttr("order", tmp_order);
  layerCanvas[tmp_layerID]["order"] = current_order;
  layerCanvas[layerId]["order"] = tmp_order;
  setLayerOrder(tmp_layerID, current_order);
  setLayerOrder(layerId, tmp_order);

  var currentLayer = $(el).parents("li.btn-pattern-item.active");
  var previousLayer = $(currentLayer).prev();

  var tmplayer = $(previousLayer).clone();
  $(previousLayer).remove();
  $(currentLayer).after(tmplayer);
  doSyncLayer("");
}

function onMoveDownPatternBtn(el) {
  var currentSelectedColorObj =
    $('input[name="color"].colorChecked') || $('input[name="color"]:checked');
  var parSelectedColorObj = $(currentSelectedColorObj).parents(".lbl-item");
  var selectedColor = $(parSelectedColorObj)
    .find(".color-item")
    .data("main-color");

  layerCanvas[layerId]["color"] = tmpHoverColor || selectedColor;
  var current_order = layerCanvas[layerId]["order"];
  current_order = parseInt(current_order);
  var tmp_order = current_order;

  var orderArr = [];
  $.each(layerCanvas, function (key, value) {
    if (key !== "background-color") {
      orderArr.push(parseInt(value.order));
    }
  });
  var minOrder = Math.min.apply(null, orderArr);

  if (current_order - 1 < minOrder) {
    alert(
      "Selected layer is reach the bottom level. Cannot move to bottom no more."
    );
    return false;
  }
  tmp_order -= 1;

  var tmp_layerID = getLayerIdByAttr("order", tmp_order);
  layerCanvas[tmp_layerID]["order"] = current_order;
  layerCanvas[layerId]["order"] = tmp_order;
  setLayerOrder(tmp_layerID, current_order);
  setLayerOrder(layerId, tmp_order);

  var currentLayer = $(el).parents("li.btn-pattern-item.active");
  var nextLayer = $(currentLayer).next();

  var tmplayer = $(currentLayer).clone();
  $(currentLayer).remove();
  $(nextLayer).after(tmplayer);
  doSyncLayer("");
}

function onSelectProductDesignTypeManagerMode(el) {
  $('input[name="manufacturer"]').attr("checked", false);
  $("#shapeList").html("");
  $("#sizeList").html("");
  // if ($(el).val() == "customized") {
  //   $("#managePrice").addClass("hide");
  // } else {
  //   $("#managePrice").removeClass("hide");
  // }
  if ($(el).val() !== "fixed") {
    $("#managePrice").addClass("hide");
  } else {
    $("#managePrice").removeClass("hide");
  }

  $('input[name="manufacturer"]:checked').trigger("click");
}

function onSelectSizeManagerMode(el) {
  var designType = $('input[name="product_design_type"]:checked').val();
  if (designType !== "fixed") {
    return false;
  }

  startLoading();
  $.ajax({
    url:
      wpParams.ajax_url +
      "?action=getImageOnTopAndTerazzoByManuAndShapeAndSize",
    type: "GET",
    data: {
      manu: $('input[name="manufacturer"]:checked').val(),
      shape: $('input[name="shapes"]:checked').val(),
      size: $(el).val(),
    },
  }).done(function (resp) {
    resp = JSON.parse(resp);
    if (!resp.success) {
      alert(resp.message);
      return false;
    }
    var message = resp.message;

    if (!message.imageOnTopUrl) {
      alert("Do not have any image on top.");
    } else {
      $("#imageUrlOfOnTopShape").val(message.imageOnTopUrl);
      loadImageOnTop("#imageUrlOfOnTopShape");

      $("#imageUrlOfOnTileShadowShape").val(message.imageOnTopTileShadowUrl);
      loadTileShadow("#imageUrlOfOnTileShadowShape");

      $("#imageUrlOfOnAddMoreShape").val(message.imageOnTopAddMoreUrl);
      loadImageOnTopAddMore("#imageUrlOfOnAddMoreShape");
      $("#imageUrlOfOnAddMoreHoverShape").val(
        message.imageOnTopAddMoreHoverUrl
      );

      $("#imageUrlOfOnBackgroundShape").val(message.imageOnTopBackgroundUrl);
      loadImageOnTopBackground("#imageUrlOfOnBackgroundShape");
      $("#imageUrlOfOnBackgroundHoverShape").val(
        message.imageOnTopBackgroundHoverUrl
      );

      $("#imageUrlOfOnSmallPreviewShape").val(
        message.imageOnTopSmallPreviewUrl
      );
      $(".small-preview-layer-cover").attr(
        "src",
        message.imageOnTopSmallPreviewUrl
      );
      $("#imageUrlOfOnSmallPreviewHoverShape").val(
        message.imageOnTopSmallPreviewHoverUrl
      );

      $("#imageUrlOfOnMainPreviewShape").val(message.imageOnTopMainPreviewUrl);
    }

    if (!message.terazzoUrl) {
      alert("Do not have any terazzo.");
    } else {
      $("#terazzoUrl").val(message.terazzoUrl);
      loadTerazzoLayout("#terazzoUrl");
    }

    stopLoading();
  });
}

function onSelectShapeManagerMode(el) {
  var designType = $('input[name="product_design_type"]:checked').val();
  if (typeof designType == "undefined") {
    alert("Please select product design type first.");
    return false;
  }

  // var sizeList = JSON.parse($('#sizeList').val());
  // console.log(sizeList);

  var type = designType == "fixed" ? "radio" : "checkbox";
  var sizeListHtml = "";
  var sizeList = m_shapeSize[$(el).val()];
  for (var key in sizeList) {
    sizeListHtml +=
      '<input autocomplete="off" type="' +
      type +
      '" name="sizes" onclick="onSelectSizeManagerMode(this)" value="' +
      key +
      '">' +
      sizeList[key] +
      "</input>";
  }
  $("#sizeList").html(sizeListHtml);

  if (designType == "fixed") {
    var selectedSizeSlug = $("#selectedSizeSlug").val();
    $('input[name="sizes"]').each(function () {
      if ($(this).val() == selectedSizeSlug) {
        $(this).prop("checked", true);
        $(this).trigger("click");
        return false;
      }
    });
  } else {
    var selectedSizeSlugs = $("#initialSizeList").val();
    selectedSizeSlugs = JSON.parse(selectedSizeSlugs);
    for (var key in selectedSizeSlugs) {
      $('input[name="sizes"][value="' + selectedSizeSlugs[key] + '"]').prop(
        "checked",
        true
      );
    }
  }
}

function onSelectManufacturerManagerMode(el) {
  startLoading();
  $.ajax({
    url: wpParams.ajax_url + "?action=getShapeSizeByManufacturer",
    type: "GET",
    data: {
      manuID: $(el).val(),
    },
  }).done(function (resp) {
    resp = JSON.parse(resp);
    if (!resp.success) {
      alert("FALSE SHAPE");
      return false;
    }
    var responseData = resp.message;

    var shapeListHtml = "";
    var shapeList = responseData.shape;
    m_shapeSize = responseData.shapeSize;
    for (var key in shapeList) {
      shapeListHtml +=
        '<input autocomplete="off" type="radio" name="shapes" onclick="onSelectShapeManagerMode(this)" value="' +
        key +
        '">' +
        shapeList[key] +
        "</input>";
    }
    $("#shapeList").html(shapeListHtml);

    var shapeSlug = $("#selectedShapeSlug").val();
    $('input[name="shapes"][value="' + shapeSlug + '"]')
      .prop("checked", true)
      .trigger("click");
    stopLoading();
  });
}

function onClickAddPriceAreaPairManagerMode(el) {
  var count = $("#priceAreaBlock .priceAreaPair").length;
  if (count === 8) {
    alert("You reach the limit of nodes (8 nodes).");
    return false;
  }

  var removeBtn =
    '<a href="javascript:void(0);" class="removePriceAreaPairBtn" onclick="onClickRemovePriceAreaPairManagerMode(this)"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>';
  $("#priceAreaBlock .priceAreaPair .priceAreaPairTitle").each(function () {
    if (!$(this).has("a.removePriceAreaPairBtn").length) {
      $(this).append(removeBtn);
    }
  });

  var html = '<div class="row priceAreaPair">';
  html +=
    '<div class="col-sm-2 priceAreaPairTitle" style="text-align:center;">';
  html += '<strong class="pricePairTitle"> Pair ' + ++count + " </strong>";
  html += removeBtn;
  html += "</div>";
  html += '<div class="col-sm-4">';
  html += "<strong> price (usd) </strong>";
  html += '<input type="number" min="1" name="price" />';
  html += "</div>";
  html += '<div class="col-sm-2" style="text-align:center;"> equal </div>';
  html += '<div class="col-sm-4">';
  html += "<strong> area (m<sup>2</sup>) </strong>";
  html += '<input type="number" min="1" name="area" />';
  html += "</div>";
  html += "</div>";

  $("#priceAreaBlock").append(html);
}

function onClickRemovePriceAreaPairManagerMode(el) {
  $(el).closest(".priceAreaPair").remove();

  var count = $(".priceAreaPair").length;
  if (count === 1) {
    $("#priceAreaBlock .priceAreaPair .priceAreaPairTitle")
      .find(".removePriceAreaPairBtn")
      .remove();
  }

  $("#priceAreaBlock .priceAreaPair .priceAreaPairTitle .pricePairTitle").each(
    function (index) {
      $(this).html("Pair " + ++index + " ");
    }
  );
}

function loadFixedTileInfo() {
  $("#sizeBlock").addClass("hide");
  $("#fixedTileSelectedSize").html($("#selectedSizeSlug").val());
  $("#fixedTileSelectedThickness").html($("#selectedThickness").val());
  $("#fixedTileShape").html($("#selectedShapeName").val());
}

function loadToolBasedOnProductType() {
  if ($("#productDesignType").val() == "fixed") {
    $("#fixedTool").removeClass("hide").addClass("show");
    // $("#customizedTool, #terazzoBlock").removeClass("show").addClass("hide");
    $("#customizedTool").removeClass("show").addClass("hide");

    loadFixedTileInfo();
  } else if ($("#productDesignType").val() == "customized") {
    // $("#customizedTool, #terazzoBlock").removeClass("hide").addClass("show");
    $("#customizedTool").removeClass("hide").addClass("show");
    $("#fixedTool").removeClass("show").addClass("hide");
  } else {
    // $("#customizedTool, #terazzoBlock").removeClass("hide").addClass("show");
    $("#customizedTool").removeClass("hide").addClass("show");
    $("#move-tab, #rotate-tab, #scale-tab, #skew-tab")
      .parents(".nav-item")
      .addClass("hide");
    $("#fixedTool").removeClass("show").addClass("hide");
  }
}

function onToggleManagerMode(el) {
  $("#managerSection").toggleClass("hide");
  $("#terazzoBlock").toggleClass("hide");
  $("html, body").animate(
    {
      scrollTop: $("#managerSection").offset().top,
    },
    500
  );

  if ($("#managerSection").hasClass("hide")) {
    loadImageOnTop("#defaultImageUrlOfOnTopShape");
    loadTileShadow("#defaultImageUrlOfOnTileShadowShape");
    loadImageOnTopAddMore("#defaultImageUrlOfOnAddMoreShape");
    loadImageOnTopAddMoreHover("#defaultImageUrlOfOnAddMoreHoverShape");
    loadImageOnTopBackground("#defaultImageUrlOfOnBackgroundShape");
    loadTerazzoLayout("#terazoLink");
    loadToolBasedOnProductType();
  } else {
    loadImageOnTop("#imageUrlOfOnTopShape");
    loadTileShadow("#imageUrlOfOnTileShadowShape");
    loadImageOnTopAddMore("#imageUrlOfOnAddMoreShape");
    loadImageOnTopAddMoreHover("#imageUrlOfOnAddMoreHoverShape");
    loadImageOnTopBackground("#imageUrlOfOnBackgroundShape");
    loadTerazzoLayout("#terazzoUrl");

    // $("#customizedTool, #terazzoBlock").removeClass("hide").addClass("show");
    $("#customizedTool").removeClass("hide").addClass("show");
    $("#move-tab, #rotate-tab, #scale-tab, #skew-tab")
      .parents(".nav-item")
      .removeClass("hide");
    $("#fixedTool").removeClass("show").addClass("hide");
  }
}

function syncAddMoreLayerModalSliderArrowColor() {
  if (!$("#swiper-button-next-element").hasClass("swiper-button-disabled")) {
    $("#swiper-button-next-element").css("background-color", "#0CA8FF");
    $("#swiper-button-next-element").find("path").css("fill", "#fff");
  } else {
    $("#swiper-button-next-element").css("background-color", "#fff");
    $("#swiper-button-next-element").find("path").css("fill", "#A1AAB6");
  }
}

function onLoadMoreLayerNextSlide(el) {
  startLoading();
  var total = $("#totalLayers").val();
  var page = $(el).attr("data-next-page");
  var next_page = parseInt(page) + 1;

  var addElementSlider = document.getElementById("swiper-add-element").swiper;
  if (
    !addElementSlider.isEnd ||
    addElementSlider.slides.length == parseInt(total)
  ) {
    stopLoading();
    return false;
  }

  var tags = [];
  $('input[name="lm-tag"]:checked').each(function () {
    tags.push(parseInt($(this).val()));
  });

  $.ajax({
    url: wpParams.ajax_url + "?action=loadMoreLayer",
    type: "POST",
    data: {
      page: next_page,
      tags: tags,
    },
  }).done(function (resp) {
    resp = JSON.parse(resp);
    if (!resp.success) {
      alert(resp.message);
      return false;
    }
    var html = "";
    html += '<div class="swiper-slide text-center">';
    $.each(resp.message, function (key, value) {
      html +=
        '<div class="element-tile d-inline-block" data-layer-title="' +
        value.name +
        '" class="btn-load-layer" data-layer-id="' +
        key +
        '">';
      // '" onclick="onClickLayerAddMore(this)">';
      html +=
        '<object class="canvas-object" id="child-object-' +
        key +
        '" type="image/svg+xml" data="' +
        value.src +
        '"> </object>';
      html += "</div>";
    });
    html += "</div>";

    if (html != '<div class="swiper-slide text-center"></div>') {
      addElementSlider.appendSlide(html);

      addElementSlider.update();
      addElementSlider.slideTo(addElementSlider.slides.length);
      disableHoverEvents();
      disableTouchEventOnSafariIosONLY();
    }

    if (!resp.end) {
      $(el).attr("data-next-page", next_page);
      $(el).attr("data-end-list", 0);
      $(el).removeClass("swiper-button-disabled");
    } else {
      $(el).attr("data-end-list", 1);
    }
    $("#totalLayers").val(resp.total);

    syncAddMoreLayerModalSliderArrowColor();
    stopLoading();
  });
}

function onLoadMoreLayerWithTags() {
  startLoading();

  var tags = [];
  $('input[name="lm-tag"]:checked').each(function () {
    tags.push(parseInt($(this).val()));
  });

  var addElementSlider = document.getElementById("swiper-add-element").swiper;
  addElementSlider.removeAllSlides();

  $.ajax({
    url: wpParams.ajax_url + "?action=loadMoreLayer",
    type: "POST",
    data: {
      page: 1,
      tags: tags,
    },
  }).done(function (resp) {
    resp = JSON.parse(resp);
    if (!resp.success) {
      alert(resp.message);
      return false;
    }
    var html = "";
    html += '<div class="swiper-slide text-center">';
    $.each(resp.message, function (key, value) {
      html +=
        '<div class="element-tile d-inline-block" data-layer-title="' +
        value.name +
        '" class="btn-load-layer" data-layer-id="' +
        key +
        '">';
      // '" onclick="onClickLayerAddMore(this)">';
      html +=
        '<object class="canvas-object" id="child-object-' +
        key +
        '" type="image/svg+xml" data="' +
        value.src +
        '"> </object>';
      html += "</div>";
    });
    html += "</div>";

    addElementSlider.appendSlide(html);

    addElementSlider.update();
    addElementSlider.slideTo(0);
    disableHoverEvents();
    disableTouchEventOnSafariIosONLY();

    if (!resp.end) {
      $("#swiper-button-next-element").attr("data-next-page", 1);
      $("#swiper-button-next-element").attr("data-end-list", 0);
      $("#swiper-button-next-element").removeClass("swiper-button-disabled");
    } else {
      $("#swiper-button-next-element").attr("data-end-list", 1);
    }
    $("#totalLayers").val(resp.total);
    stopLoading();
  });
}

// $('body').on('hidden.bs.modal', '.modal', function () {
//     $(this).removeData('bs.modal');
// });

$("body").on("click touchstart touchend", ".element-tile", function () {
  // $("body").on("click", ".element-tile", function() {
  // onClickLayerAddMore(this);
  var currentAddMoreLayerId = $(this).data("layer-id");
  if ($(this).hasClass("element-selected")) {
    $(this).removeClass("element-selected");
    changeSelectedAddMoreLayerColor(
      "child-canvas-" + currentAddMoreLayerId.toString(),
      "#636363"
    );
  } else {
    $(this).addClass("element-selected");
    changeSelectedAddMoreLayerColor(
      "child-canvas-" + currentAddMoreLayerId.toString(),
      "#4F94F5"
    );
  }

  $("#btnAddMoreLayer").prop("disabled", true);
  $("#btnAddMoreLayer").removeClass("blue-style").addClass("link-hover");

  $(".element-tile").each(function () {
    if ($(this).hasClass("element-selected")) {
      $("#btnAddMoreLayer").prop("disabled", false);
      $("#btnAddMoreLayer").addClass("blue-style").removeClass("link-hover");
    }
  });
});

// function onClickLayerAddMore(el) {
//   // $('#swiper-add-element .element-tile').removeClass('active');
//   $(el).toggleClass("element-selected");
// }
// $("body").on("click touchend", "#btnAddMoreLayer", function() {
$("body").on("click", "#btnAddMoreLayer", function () {
  $(".element-tile.element-selected").each(function () {
    var newLayer = $(this).clone();
    var childCanvasId = newLayer.data("layer-id");
    var dataLayerId = "child-canvas-" + childCanvasId;
    var duplicateArr = [];
    var duplicateNamesArr = [];
    var orderArr = [];
    $.each(layerCanvas, function (key, value) {
      if (key !== "background-color") {
        var tmpID = value.id.toString();
        var _layerID = tmpID.split("-")[0];
        if (parseInt(_layerID) == parseInt(childCanvasId)) {
          var tmpCount = tmpID.split("-")[1];
          duplicateArr.push(parseInt(tmpCount ? parseInt(tmpCount) : 0));
          duplicateNamesArr.push(value.name);
        }
        orderArr.push(parseInt(value.order));
      }
    });

    // trigger close click on modal (bootstrap's modal issues, conflict)
    if (IsMobileBrowser) {
      $(".clicktoclose").click();
      $(".clicktoclose").click();
    } else {
      $(".clicktoclose").click();
    }
    $(".modal-backdrop").addClass("hidden");

    var duplicateArrLength = duplicateArr.length;
    var maxDuplicateId = Math.max.apply(null, duplicateArr);
    var duplicateMax = duplicateArrLength > 0 ? maxDuplicateId + 1 : 0;
    duplicateMax =
      duplicateMax > duplicateArrLength ? duplicateMax : duplicateArrLength;
    dataLayerId =
      duplicateMax > 0 ? dataLayerId + "-" + duplicateMax : dataLayerId;

    var tmpMaxMinData = "";
    if (duplicateMax > 1) {
      tmpMaxMinData = $(
        "#maxMinData_child-canvas-" + childCanvasId + "-" + maxDuplicateId
      ).val();
    }

    var newLayerHtml =
      '<li class="btn-pattern-item" data-layer-id="' + dataLayerId + '">';
    newLayerHtml +=
      "<input type='hidden' id='maxMinData_" +
      dataLayerId +
      "' value='" +
      tmpMaxMinData +
      "' />";
    newLayerHtml +=
      '<a href="javascript:void(0);" onclick="onSelectPatternItem(this)" data-layer-id="' +
      dataLayerId +
      '">' +
      '<img src="" id="smallPreviewImage_' +
      dataLayerId +
      '" />';
    ("</a>");
    newLayerHtml +=
      '<div class="icon-showmenu"></div>' +
      '<div class="pattern-action-menu">' +
      '<button class="action-menu-up-down" onclick="onMoveUpPatternBtn(this)">' +
      '<i class="ti-angle-up"></i>' +
      "</button>" +
      '<button class="btn-copy" onclick="onCopyPatternBtn(this)"></button>' +
      '<button class="btn-trash" onclick="onRemovePatternBtn(this)"></button>' +
      '<button class="action-menu-up-down" onclick="onMoveDownPatternBtn(this)">' +
      '<i class="ti-angle-down"></i>' +
      "</button>" +
      "</div></li>";

    var newLayerTitle = newLayer.data("layer-title");
    newLayerTitle =
      duplicateMax > 0 ? newLayerTitle + "-" + duplicateMax : newLayerTitle;

    $("#pattern-list").prepend(newLayerHtml);
    $("#layerCanvas").append(
      '<img id="' +
        dataLayerId +
        '" width="' +
        tmpSize +
        '" height="' +
        tmpSize +
        '" />'
    );

    var childCanvasData = {};

    /**
     * Declare config flag for each layer = false
     * If config of layer is existed
     * Then setting up config data for layer and config flag = true
     * Else setting up default data for layer
     */
    // Default data
    childCanvasData.id =
      duplicateMax > 0 ? childCanvasId + "-" + duplicateMax : childCanvasId;
    childCanvasData.duplicateID = duplicateMax > 0 ? childCanvasId : 0;
    childCanvasData.duplicateCounter = duplicateMax;
    childCanvasData.imgSource = newLayer.find("object").attr("data");
    childCanvasData.rotateDegree = 0;
    childCanvasData.scale = "1,1";
    childCanvasData.skew = "0,0";
    childCanvasData.position = "0,0";
    childCanvasData.color = "";
    childCanvasData.order =
      orderArr.length > 0 ? Math.max.apply(null, orderArr) + 1 : ++order_count;
    order_count =
      orderArr.length > 0 ? Math.max.apply(null, orderArr) + 1 : order_count;

    if (childCanvasData.duplicateID !== 0) {
      var duplicateName = duplicateNamesArr[0];
      duplicateName = duplicateName.split("-");
      duplicateName = duplicateName[0];
      if (layerCanvas["child-canvas-" + childCanvasData.id]) {
        duplicateName =
          layerCanvas["child-canvas-" + childCanvasData.id]["name"];
      }

      childCanvasData.name = "child-canvas-" + childCanvasData.id;
    } else {
      childCanvasData.name = newLayerTitle;
    }

    layerCanvas[dataLayerId] = childCanvasData;
    resetCanvas = $.extend(true, {}, layerCanvas);
    isHover = false;
    drawLayer(dataLayerId);
    reCalculateColorPrice();

    $('a[data-layer-id="' + dataLayerId + '"]').trigger("click");
    $('input[name="color"]')
      // .prop("checked", false)
      .removeClass("colorChecked")
      .addClass("colorUnchecked");
  });
  //$("#toolAddElementModalCenter").modal('hide');
});

function onChangeSizeSelection(el) {
  startLoading();

  var sizeSlug = $(el).val();
  var selectedShapeSlug =
    $("#selectedShapeSlug").val() || getURLParameter("shape");
  var selectedManuID = $("#selectedManuID").val();
  var productID = $("#productID").val() || getURLParameter("product_id");

  $.ajax({
    url: wpParams.ajax_url + "?action=getRelatedDataByFilters",
    type: "GET",
    data: {
      sizeSlug: sizeSlug,
      shapeSlug: selectedShapeSlug,
      manufacturerId: selectedManuID,
      productId: productID,
    },
  }).done(function (resp) {
    resp = JSON.parse(resp);
    if (!resp.success) {
      alert(resp.message);
      $(el).val($(el).find("option:first").val());
      onChangeSizeSelection(el);
    }

    var message = resp.message;
    $("#priceObject").val(JSON.stringify(message.priceObject));
    $("#colorUnitPrice").val(message.colorUnitPrice);
    $("#basePrice").val(message.basePrice);
    $("#bpp").val(message.bpp);
    $("#ppa").val(message.ppa);
    $("#ppb").val(message.ppb);
    $("#ppp").val(message.ppp);
    $("#wpt").val(message.wpt);
    $("#wpb").val(message.wpb);
    $("#visualizerSizeRatio").val(message.visualizerSizeRatio);
    $("#rulerLink").val(message.gridLink);
    $("#templateConfigurationData").val(message.fragmentId);
    $("#terazoLink, #terazzoUrl").val(message.terazzoUrl);

    $("#imageUrlOfOnTopShape").val(message.imageOnTopUrl);
    loadImageOnTop("#imageUrlOfOnTopShape");

    $("#imageUrlOfOnTileShadowShape").val(message.imageOnTopTileShadowUrl);
    loadTileShadow("#imageUrlOfOnTileShadowShape");

    $("#imageUrlOfOnSmallPreviewShape").val(message.imageOnTopSmallPreviewUrl);
    $(".small-preview-layer-cover").attr(
      "src",
      message.imageOnTopSmallPreviewUrl
    );
    $("#imageUrlOfOnSmallPreviewHoverShape").val(
      message.imageOnTopSmallPreviewHoverUrl
    );

    $("#imageUrlOfOnAddMoreShape").val(message.imageOnTopAddMoreUrl);
    loadImageOnTopAddMore("#imageUrlOfOnAddMoreShape");
    $("#imageUrlOfOnAddMoreHoverShape").val(message.imageOnTopAddMoreHoverUrl);

    $("#imageUrlOfOnBackgroundShape").val(message.imageOnTopBackgroundUrl);
    loadImageOnTopBackground("#imageUrlOfOnBackgroundShape");
    $("#imageUrlOfOnBackgroundHoverShape").val(
      message.imageOnTopBackgroundHoverUrl
    );

    $("#imageUrlOfOnMainPreviewShape").val(message.imageOnTopMainPreviewUrl);

    loadTerazzoLayout("#terazoLink");
    toggleGrid(document.getElementById("checkbox-grid"));
    reCalculateColorPrice();
    getGallerias(message.fragmentId);

    stopLoading();
  });
}

function onSaveDesign(el) {
  startLoading();

  var productDetail = {};
  productDetail["productID"] = $("#productID").val();
  productDetail["shapeSlug"] = $("#selectedShapeSlug").val();
  productDetail["sizeID"] = $("#size-selection")
    .find("option:selected")
    .data("size-id");
  productDetail["terazo"] = $("#terazoOpt").prop("checked") ? 1 : 0;

  // if(checkIsSafariBrowser()) {
  window.scrollTo(0, 0);
  html2canvas($("#layerCanvas"), { useCORS: true })
    .then((canvas) => {
      $(canvas).prop("id", "testCanvas").addClass("hide");
      document.body.appendChild(canvas);
      var _canvas = document.getElementById("testCanvas");
      productDetail["image"] = _canvas.toDataURL();
      $.ajax({
        url: wpParams.ajax_url + "?action=saveDesign",
        type: "POST",
        data: {
          layerCanvas: layerCanvas,
          productDetail: productDetail,
        },
      }).done(function (resp) {
        resp = JSON.parse(resp);
        if (!resp.success) {
          alert(resp.message);
          return false;
        }

        var html = $("#saveBtn").html().replace("SAVE", "SAVED");
        $("#saveBtn").html(html);
      });
    })
    .catch(function (error) {
      alert("Error when save design", error);
    })
    .finally(function () {
      window.scrollTo(0, document.body.scrollHeight);
      stopLoading();
      $("#testCanvas").remove();
    });
  // } else {
  //   var nodeLayer = document.getElementById("layerCanvas");
  //   domtoimage
  //     .toPng(nodeLayer)
  //     .then(function(dataUrl) {
  //       productDetail["image"] = dataUrl;

  //       $.ajax({
  //         url: wpParams.ajax_url + "?action=saveDesign",
  //         type: "POST",
  //         data: {
  //           layerCanvas: layerCanvas,
  //           productDetail: productDetail
  //         }
  //       }).done(function(resp) {
  //         resp = JSON.parse(resp);
  //         if (!resp.success) {
  //           alert(resp.message);
  //           return false;
  //         }

  //         var html = $("#saveBtn")
  //           .html()
  //           .replace("SAVE", "SAVED");
  //         $("#saveBtn").html(html);
  //         stopLoading();
  //       });
  //     }).catch(function(error) {
  //       alert("Error when save design", error);
  //     }).finally(function() {
  //       stopLoading();
  //     });
  // }
}
// function typo3Action(imageSrc, shareID, img) {
//   return new Promise(function (resolve, reject) {
//     // $("#layerCanvas img").hide();
//     $("#typo3Image").attr("src", imageSrc).show();
//     console.log("BEFORE 2nd CAPTURE");
//     console.log(imageSrc);
//     $("#mainCanvasTileShadowLayout").show();
//     html2canvas($("#layerCanvas"), { useCORS: true })
//       .then((canvas) => {
//         console.log("2nd CAPTURE");
//         console.log(canvas.toDataURL());
//         var img = document.getElementById("shareImage");
//         img.src = canvas.toDataURL();
//         img.onload = function () {
//           var cvs = document.getElementById("share-canvas-2");
//           var ctx = cvs.getContext("2d");
//           cvs.width = FRAGMENT_IMAGE_SIZE * 8;
//           cvs.height = FRAGMENT_IMAGE_SIZE * 8;
//           ctx.drawImage(img, 0, 0, cvs.width, cvs.height);

//           var paramString = decodeURI(window.location.search);
//           var urlParams = new URLSearchParams(paramString);

//           var action = "";
//           if (urlParams.has("action")) {
//             action = urlParams.get("action");
//           }

//           var typo3Type = "";
//           if (action == "design") {
//             typo3Type = "POST";
//           } else if (action == "shareLink") {
//             typo3Type = "PATCH";
//           }

//           var today = new Date();
//           $.ajax({
//             url: wpParams.ajax_url + "?action=typo3Action",
//             type: "POST",
//             data: {
//               typo3Type: typo3Type,
//               typo3Data: {
//                 title: $("#productTitle").val(),
//                 sku: shareID,
//                 productType: TYPO3_PRODUCT_TYPE,
//                 colors: colorArray.length,
//                 type: TYPO3_TYPE,
//                 appImage: cvs.toDataURL(),
//                 shapeSlug: $("#selectedShapeSlug").val(),
//                 datetime:
//                   today.getHours() +
//                   ":" +
//                   today.getMinutes() +
//                   " " +
//                   today.getDate() +
//                   "-" +
//                   today.getMonth() +
//                   "-" +
//                   today.getFullYear(),
//                 internalurl: TYPO3_INTERNAL_URL,
//                 uid: $("#typo3-uid").val() || "",
//               },
//             },
//             complete: function (jqXHR, textStatus) {
//               if (jqXHR.status == 200) {
//                 var respText = jqXHR.responseText;
//                 var respJson = JSON.parse(respText);
//                 // THEN RELOAD MAIN SITE
//                 // window.top.location.reload();

//                 // console.log(respJson);
//                 if (!respJson.success) {
//                   alert(respJson.message);
//                 } else {
//                   // t3Return = t3Return.replace("TID", respJson.message);
//                   // console.log(decodeURI(respJson.message));
//                   parent.window.location = decodeURI(respJson.message);
//                 }
//               } else {
//                 alert("Something wrong, please check with admin.");
//               }
//               // $("#layerCanvas img").show();
//               $("#typo3Image").hide();
//             },
//           });
//         };
//       })
//       .catch(function (error) {
//         alert("Error when prepared image for typo3 action", error);
//       })
//       .finally(function () {
//         window.scrollTo(0, document.body.scrollHeight);
//         stopLoading();
//         $("#testCanvas").remove();
//         // $("#layerCanvas img").show();
//         // $("#typo3Image").hide();
//       });
//   });
// }
// function imageOnLoadFunc(img, shareID) {
//   var cvs = document.getElementById("share-canvas");
//   var ctx = cvs.getContext("2d");
//   cvs.width = FRAGMENT_IMAGE_SIZE * 4;
//   cvs.height = FRAGMENT_IMAGE_SIZE * 4;
//   ctx.drawImage(img, 0, 0, cvs.width, cvs.height);
//   var imageData = ctx.getImageData(0, 0, cvs.width, cvs.height);
//   for (var i = 0; i < imageData.data.length; i += 4) {
//     var r = imageData.data[i];
//     var g = imageData.data[i + 1];
//     var b = imageData.data[i + 2];

//     //if it is pure white, change its alpha to 0
//     if (
//       (b == 255 && g == 255 && r == 255) ||
//       (b == 170 && g == 172 && r == 171)
//     ) {
//       // if (b != 191 && g != 183 && r != 130)  {
//       imageData.data[i] = CHANGE_DESIGN_CAPTURE_COLOR_RED;
//       imageData.data[i + 1] = CHANGE_DESIGN_CAPTURE_COLOR_GREEN;
//       imageData.data[i + 2] = CHANGE_DESIGN_CAPTURE_COLOR_BLUE;
//       imageData.data[i + 3] = CHANGE_DESIGN_CAPTURE_COLOR_OPACITY;
//       // imageData.data[i + 3] = 0;
//     }
//   }
//   ctx.putImageData(imageData, 0, 0);
//   // var _imgSrc = cvs.toDataURL();

//   await typo3Action(cvs.toDataURL(), shareID, img);
//   stopLoading();
// }
// function promiseShareDesign(productDetail) {
//   $.ajax({
//     url: wpParams.ajax_url + "?action=shareDesign",
//     type: "POST",
//     data: {
//       layerCanvas: layerCanvas,
//       productDetail: productDetail,
//       fromProductDetail: 1,
//       link:
//         window.location.protocol +
//         "//" +
//         window.location.hostname +
//         window.location.pathname,
//     },
//   }).done(function (resp) {
//     resp = JSON.parse(resp);
//     if (!resp.success) {
//       stopLoading();
//       alert(resp.message);
//       return;
//     }
//     // console.log(resp.message.share_link);
//     // console.log(resp.message.share_id);
//     var img = document.getElementById("shareImage");
//     var _imgSrc = productDetail["image"];
//     img.src = _imgSrc;
//     // img.onload = imageOnLoadFunc(img, resp.message.share_id);
//     img.onload = function () {
//       var cvs = document.getElementById("share-canvas");
//       var ctx = cvs.getContext("2d");
//       cvs.width = FRAGMENT_IMAGE_SIZE * 4;
//       cvs.height = FRAGMENT_IMAGE_SIZE * 4;
//       ctx.drawImage(img, 0, 0, cvs.width, cvs.height);
//       var imageData = ctx.getImageData(0, 0, cvs.width, cvs.height);
//       for (var i = 0; i < imageData.data.length; i += 4) {
//         var r = imageData.data[i];
//         var g = imageData.data[i + 1];
//         var b = imageData.data[i + 2];

//         //if it is pure white, change its alpha to 0
//         if (
//           (b == 255 && g == 255 && r == 255) ||
//           (b == 170 && g == 172 && r == 171)
//         ) {
//           // if (b != 191 && g != 183 && r != 130)  {
//           imageData.data[i] = CHANGE_DESIGN_CAPTURE_COLOR_RED;
//           imageData.data[i + 1] = CHANGE_DESIGN_CAPTURE_COLOR_GREEN;
//           imageData.data[i + 2] = CHANGE_DESIGN_CAPTURE_COLOR_BLUE;
//           imageData.data[i + 3] = CHANGE_DESIGN_CAPTURE_COLOR_OPACITY;
//           // imageData.data[i + 3] = 0;
//         }
//       }
//       ctx.putImageData(imageData, 0, 0);
//       // var _imgSrc = cvs.toDataURL();

//       await typo3Action(cvs.toDataURL(), resp.message.share_id, img);
//       stopLoading();
//     };
//   });
// }
function onSaveDesignV2(el) {
  startLoading();
  $("#mainCanvasTileShadowLayout").hide();
  var productDetail = {};
  productDetail["productID"] = $("#productID").val();
  productDetail["shapeSlug"] = $("#selectedShapeSlug").val();
  productDetail["sizeID"] = $("#size-selection")
    .find("option:selected")
    .data("size-id");

  productDetail["image"] = "";
  productDetail["terazo"] = $("#terazoOpt").prop("checked") ? 1 : 0;

  var nodeEleId = "layerCanvas";
  window.scrollTo(0, 0);

  // virtual cropping: infact dcrease the size of the canvas layers div to eliminate the lines
  // var innerOuterDistance = 20 + CROP_PIXEL;
  // var canvasLayersSize = FRAGMENT_IMAGE_SIZE * 4 - innerOuterDistance;
  // $("#layerCanvas").width(canvasLayersSize);
  // $("#layerCanvas").height(canvasLayersSize);
  // console.log(canvasLayersSize);

  html2canvas($("#" + nodeEleId), { useCORS: true })
    .then((canvas) => {
      $(canvas).prop("id", "testCanvas").addClass("hide");
      document.body.appendChild(canvas);
      var _canvas = document.getElementById("testCanvas");
      productDetail["image"] = _canvas.toDataURL();
      // $("#mainCanvasTileShadowLayout").show();
      $.ajax({
        url: wpParams.ajax_url + "?action=shareDesign",
        type: "POST",
        data: {
          layerCanvas: layerCanvas,
          productDetail: productDetail,
          fromProductDetail: 1,
          link:
            window.location.protocol +
            "//" +
            window.location.hostname +
            window.location.pathname,
        },
      }).done(function (resp) {
        resp = JSON.parse(resp);
        if (!resp.success) {
          stopLoading();
          alert(resp.message);
          return;
        }
        var shareID = resp.message.share_id;
        // console.log(resp.message.share_link);
        // console.log(resp.message.share_id);
        var img = document.getElementById("shareImage");
        var _imgSrc = productDetail["image"];
        img.src = _imgSrc;
        img.onload = function () {
          var shareCVS = document.getElementById("share-canvas");
          var shareCTX = shareCVS.getContext("2d");
          shareCVS.width = FRAGMENT_IMAGE_SIZE * 8;
          shareCVS.height = FRAGMENT_IMAGE_SIZE * 8;
          shareCTX.drawImage(img, 0, 0, shareCVS.width, shareCVS.height);
          var imageData = shareCTX.getImageData(
            0,
            0,
            shareCVS.width,
            shareCVS.height
          );
          for (var i = 0; i < imageData.data.length; i += 4) {
            var r = imageData.data[i];
            var g = imageData.data[i + 1];
            var b = imageData.data[i + 2];

            //if it is pure white, change its alpha to 0
            if (
              (b == 255 && g == 255 && r == 255) ||
              (b == 170 && g == 172 && r == 171)
            ) {
              // if (b != 191 && g != 183 && r != 130)  {
              imageData.data[i] = CHANGE_DESIGN_CAPTURE_COLOR_RED;
              imageData.data[i + 1] = CHANGE_DESIGN_CAPTURE_COLOR_GREEN;
              imageData.data[i + 2] = CHANGE_DESIGN_CAPTURE_COLOR_BLUE;
              imageData.data[i + 3] = CHANGE_DESIGN_CAPTURE_COLOR_OPACITY;
              // imageData.data[i + 3] = 0;
            }
          }
          shareCTX.putImageData(imageData, 0, 0);
          // var _imgSrc = cvs.toDataURL();

          // await typo3Action(cvs.toDataURL(), shareID, img);
          // $("#layerCanvas img").hide();
          $("#typo3Image").attr("src", shareCVS.toDataURL()).show();
          // console.log("BEFORE 2nd CAPTURE");
          // console.log(shareCVS.toDataURL());
          $("#mainCanvasTileShadowLayout").show();
          html2canvas($("#layerCanvas"), { useCORS: true })
            .then((canvas) => {
              console.log("2nd CAPTURE");
              console.log(canvas.toDataURL());
              var img = document.getElementById("shareImage");
              img.src = canvas.toDataURL();
              img.onload = function () {
                var shareCVS2 = document.getElementById("share-canvas-2");
                var shareCTX2 = shareCVS2.getContext("2d");
                shareCVS2.width = FRAGMENT_IMAGE_SIZE * 8;
                shareCVS2.height = FRAGMENT_IMAGE_SIZE * 8;
                shareCTX2.drawImage(
                  img,
                  0,
                  0,
                  shareCVS2.width,
                  shareCVS2.height
                );

                var paramString = decodeURI(window.location.search);
                var urlParams = new URLSearchParams(paramString);

                var action = "";
                if (urlParams.has("action")) {
                  action = urlParams.get("action");
                }

                var typo3Type = "";
                if (action == "design") {
                  typo3Type = "POST";
                } else if (action == "shareLink") {
                  typo3Type = "PATCH";
                }

                var today = new Date();
                $.ajax({
                  url: wpParams.ajax_url + "?action=typo3Action",
                  type: "POST",
                  data: {
                    typo3Type: typo3Type,
                    typo3Data: {
                      title: $("#productTitle").val(),
                      sku: shareID,
                      product: $("#product-uid").val(),
                      shape: $("#shape-uid").val(),
                      pathSegment:
                        Math.random().toString(36).substring(2, 15) +
                        Math.random().toString(36).substring(2, 15),
                      productType: $("#typo3ProductType").val() || TYPO3_PRODUCT_TYPE,
                      colors: colorArray.length,
                      type: TYPO3_TYPE,
                      appImage: shareCVS2.toDataURL(),
                      shapeSlug: $("#selectedShapeSlug").val(),
                      datetime:
                        today.getHours() +
                        ":" +
                        today.getMinutes() +
                        " " +
                        today.getDate() +
                        "-" +
                        today.getMonth() +
                        "-" +
                        today.getFullYear(),
                      internalurl: TYPO3_INTERNAL_URL,
                      uid: $("#typo3-uid").val() || "",
                    },
                  },
                  complete: function (jqXHR, textStatus) {
                    if (jqXHR.status != 200) {
                      alert("Something wrong, please check with admin.");
                    } else {
                      var respText = jqXHR.responseText;
                      var respJson = JSON.parse(respText);
                      // console.log(respJson);
                      if (!respJson.success) {
                        alert(respJson.message);
                        stopLoading();
                      } else {
                        // console.log(decodeURI(respJson.message));
                        parent.window.location = decodeURI(respJson.message);
                      }
                    }
                    $("#typo3Image").hide();
                  },
                });
              };
            })
            .catch(function (error) {
              alert("Error when prepared image for typo3 action", error);
            })
            .finally(function () {
              window.scrollTo(0, document.body.scrollHeight);
              $("#testCanvas").remove();
            });
        };
      });
    })
    .catch(function (error) {
      alert("Error when sharing design", error);
      stopLoading();
    })
    .finally(function () {
      window.scrollTo(0, document.body.scrollHeight);
      // $("#mainCanvasTileShadowLayout").show();
      $("#testCanvas").remove();
    });
}
function onShareDesign(el) {
  startLoading();
  var productDetail = {};
  productDetail["productID"] = $("#productID").val();
  productDetail["shapeSlug"] = $("#selectedShapeSlug").val();
  productDetail["sizeID"] = $("#size-selection")
    .find("option:selected")
    .data("size-id");

  productDetail["image"] = "";
  productDetail["terazo"] = $("#terazoOpt").prop("checked") ? 1 : 0;

  var nodeEleId = "layerCanvas";
  window.scrollTo(0, 0);
  // if(checkIsSafariBrowser()) {
  html2canvas($("#" + nodeEleId), { useCORS: true })
    .then((canvas) => {
      $(canvas).prop("id", "testCanvas").addClass("hide");
      document.body.appendChild(canvas);
      var _canvas = document.getElementById("testCanvas");
      productDetail["image"] = _canvas.toDataURL();
      $.ajax({
        url: wpParams.ajax_url + "?action=shareDesign",
        type: "POST",
        data: {
          layerCanvas: layerCanvas,
          productDetail: productDetail,
          fromProductDetail: 1,
          link:
            window.location.protocol +
            "//" +
            window.location.hostname +
            window.location.pathname,
        },
      }).done(function (resp) {
        resp = JSON.parse(resp);
        if (!resp.success) {
          stopLoading();
          alert(resp.message);
          return;
        }
        $(el).find("#input-share-link").val(resp.message);
        $(el).find("#input-share-link").focus();
        stopLoading();
      });
    })
    .catch(function (error) {
      alert("Error when sharing design", error);
    })
    .finally(function () {
      window.scrollTo(0, document.body.scrollHeight);
      stopLoading();
      $("#testCanvas").remove();
    });
  // } else {
  //   var nodeLayer = document.getElementById(nodeEleId);
  //   domtoimage
  //     .toPng(nodeLayer)
  //     .then(function(dataUrl) {
  //       productDetail["image"] = dataUrl;
  //       $.ajax({
  //         url: wpParams.ajax_url + "?action=shareDesign",
  //         type: "POST",
  //         data: {
  //           layerCanvas: layerCanvas,
  //           productDetail: productDetail,
  //           fromProductDetail: 1,
  //           link:
  //             window.location.protocol +
  //             "//" +
  //             window.location.hostname +
  //             window.location.pathname
  //         }
  //       }).done(function(resp) {
  //         resp = JSON.parse(resp);
  //         if (!resp.success) {
  //           stopLoading();
  //           alert(resp.message);
  //           return;
  //         }

  //         $(el)
  //           .find("#input-share-link")
  //           .val(resp.message);
  //         $(el)
  //           .find("#input-share-link")
  //           .focus();
  //         stopLoading();
  //       });
  //     })
  //     .catch(function(error) {
  //       alert("Error when sharing design", error);
  //     }).finally(function() {
  //       stopLoading();
  //     });
  // }
}

function toggleGrid(item) {
  var gridLayer = $("#rulerLink").val();
  if (gridLayer.length) {
    $("#grid-layout")
      .css("background", "url(" + gridLayer + ") top left")
      .css("background-size", "100% auto");
  }

  if (item.checked) {
    $("#grid-layout").addClass("show");
  } else {
    $("#grid-layout").removeClass("show");
  }
}

// copy link
function copyLink() {
  var el = document.getElementById("input-share-link");
  el.select();
  document.execCommand("copy");

  $(".btn-copy-link").html("Copied");
}

function onAddElementModalOpen(el) {
  startLoading();

  var addElementSlider = document.getElementById("swiper-add-element").swiper;
  addElementSlider.removeAllSlides();

  $.ajax({
    url: wpParams.ajax_url + "?action=loadMoreLayer",
    type: "POST",
    data: { page: 1 },
  }).done(function (resp) {
    resp = JSON.parse(resp);
    if (!resp.success) {
      alert(resp.message);
      return;
    }
    var html = "";
    html += '<div class="swiper-slide text-center">';
    $.each(resp.message, function (key, value) {
      html +=
        '<div class="element-tile d-inline-block" data-layer-title="' +
        value.name +
        '" class="btn-load-layer" data-layer-id="' +
        key +
        '">';
      // '" onclick="onClickLayerAddMore(this)">';
      html +=
        '<object class="canvas-object" id="child-object-' +
        key +
        '" type="image/svg+xml" data="' +
        value.src +
        '"> </object>';
      html += "</div>";
    });
    html += "</div>";

    addElementSlider.appendSlide(html);
    addElementSlider.update();
    disableHoverEvents();
    disableTouchEventOnSafariIosONLY();

    if (!resp.end) {
      $("#swiper-button-next-element").attr("data-next-page", 1);
      $("#swiper-button-next-element").attr("data-end-list", 0);
      $("#swiper-button-next-element").removeClass("swiper-button-disabled");
    }

    $("#totalLayers").val(resp.total);
    syncAddMoreLayerModalSliderArrowColor();
    stopLoading();
  });
}

function onVisualizeDesignModalOpen(el) {
  $("#dropdownPattern-list-popup")
    .parents(".choose-pattern-dropdown-popup")
    .removeClass("hide-arrow");
  var sizeSelect = $("#size-selection").val();
  var total = $("#dropdownPattern-list-popup > option").length;
  if (total == 1) {
    $("#dropdownPattern-list-popup")
      .parents(".choose-pattern-dropdown-popup")
      .addClass("hide-arrow");
    var patternDropDownNiceSel = $("#dropdownPattern-list-popup");
    patternDropDownNiceSel.css("background-imgage", "none");
  }
  $("#sizeVisualizerSelect").val(sizeSelect);
  $("#sizeVisualizerSelect").niceSelect("update");
  onChangeSizeVisualizer("#sizeVisualizerSelect");

  onChangeBgLayerVisualizer("#bgLayerVisualizerSelect");

  var patternListNiceSelect = $(el)
    .find("#dropdownPattern-list-popup")
    .next(".nice-select");
  $(patternListNiceSelect)
    .find("ul.list li")
    .each(function (idx, val) {
      var customStyle = $(el)
        .find("#dropdownPattern-list-popup option:eq(" + idx + ")")
        .data("custom-style");
      $(this).attr("style", customStyle);
    });

  var fragmentPodItemId = $(el).find("#dropdownPattern-list-popup").val();
  changeFragmentHtml(fragmentPodItemId);
}

function onChangeFragmentVisualizer(el) {
  changeFragmentHtml($(el).val());
}

function onChangeBgLayerVisualizer(el) {
  $("img.visual-overlay").attr("src", $(el).val());
}

function addOrEditCartItem(type) {
  var productDetail = {};
  productDetail["productID"] = $("#productID").val();
  productDetail["productName"] = $("#productOriginalTitle").val();
  productDetail["shapeSlug"] = $("#selectedShapeSlug").val();
  productDetail["shapeName"] = $("#selectedShapeName").val();
  // productDetail["sizeID"] = $(".headerSize").val();
  productDetail["sizeSlug"] = $("#size-selection option:selected").val();
  productDetail["sizeId"] = $("#size-selection option:selected").data(
    "size-id"
  );
  // productDetail["catID"] = $("#categoryBlock li.active").data("cat-id");
  productDetail["thickness"] = $("#thickness").val();
  productDetail["selectedThickness"] = $("#selectedThickness").val();

  productDetail["pricing"] = $("#priceValue").val();
  productDetail["area"] = $("#priceRangeControl").val();
  productDetail["terazo"] = $("#terazoOpt").is(":checked") ? 1 : 0;
  productDetail["manuID"] = $("#selectedManuID").val();
  productDetail["palletes"] = parseFloat($("#totalPalletes").val());

  productDetail["piecesPerM2"] = $("#piecesPerM2").val();
  productDetail["pricePerM2"] = $("#pricePerM2").val();
  productDetail["tilesPerBox"] = $("#tilesPerBox").val();
  productDetail["totalBoxes"] = $("#totalBoxes").val();
  productDetail["totalPieces"] = $("#totalPieces").val();
  productDetail["productType"] = $("#productDesignType").val();

  productDetail["wpb"] = $("#wpb").val();
  productDetail["wpt"] = $("#wpt").val();

  var cartItemId = "";
  if (type == "editCartItem") {
    cartItemId = $("#cartItemId").val();
  }

  startLoading();
  window.scrollTo(0, 0);
  // if(checkIsSafariBrowser()) {
  html2canvas($("#layerCanvas"), { useCORS: true })
    .then((canvas) => {
      $(canvas).prop("id", "testCanvas").addClass("hide");
      document.body.appendChild(canvas);
      var _canvas = document.getElementById("testCanvas");
      $.ajax({
        url: wpParams.ajax_url + "?action=" + type,
        type: "POST",
        data: {
          layerCanvas: layerCanvas,
          productDetail: productDetail,
          image: _canvas.toDataURL() || $("#productFeaturedImage").val(),
          cartItemId: cartItemId,
        },
      }).done(function (resp) {
        resp = JSON.parse(resp);
        if (!resp.success) {
          alert("FALSE");
          return;
        }

        if (type == "editCartItem") {
          location.href = resp.redirectLink;
        }

        $("#cartItem").html(resp.cartItem);

        location.href = "?action=cart";
        stopLoading();
      });
    })
    .catch(function (error) {
      console.error("Error in add or edit cart item", error);
    })
    .finally(function () {
      window.scrollTo(0, document.body.scrollHeight);
      stopLoading();
      $("#testCanvas").remove();
    });
  // } else {
  //   var nodeLayer = document.getElementById("layerCanvas");
  //   domtoimage
  //     .toPng(nodeLayer)
  //     .then(function(dataUrl) {
  //       $.ajax({
  //         url: wpParams.ajax_url + "?action=" + type,
  //         type: "POST",
  //         data: {
  //           layerCanvas: layerCanvas,
  //           productDetail: productDetail,
  //           image: dataUrl || $("#productFeaturedImage").val(),
  //           cartItemId: cartItemId
  //         }
  //       }).done(function(resp) {
  //         resp = JSON.parse(resp);
  //         if (!resp.success) {
  //           alert("FALSE");
  //           return;
  //         }

  //         if (type == "editCartItem") {
  //           location.href = resp.redirectLink;
  //         }

  //         $("#cartItem").html(resp.cartItem);

  //         location.href = "?action=cart";
  //         stopLoading();
  //       });
  //     })
  //     .catch(function(error) {
  //       console.error("Error in add or edit cart item", error);
  //     }).finally(function() {
  //       stopLoading();
  //     });
  // }
}

function onClickEditCartItemBtn(el) {
  addOrEditCartItem("editCartItem");
}

function onClickAddToCartBtn(el) {
  addOrEditCartItem("addToCart");
}

function visualizeShareBtn(el) {
  // onSaveDesignV2(el);
}

function saveDesignV2(el) {
  onSaveDesignV2(el);
}

function onChangeTerazzo(el) {
  if ($(el).prop("checked")) {
    $("#terazzoLayout")
      .parents(".canvas-container")
      .addClass("show")
      .removeClass("hide");
  } else {
    $("#terazzoLayout")
      .parents(".canvas-container")
      .removeClass("show")
      .addClass("hide");
  }

  // calPricePerArea($("#priceRangeControl").val(), true);
}

function onClickVisualizer3DViewBtn(el) {
  $("div#fragmentPreview").toggleClass("fragment-preview-3d");
  $("div#fragmentContent").toggleClass("fragment-content-3d");
}
