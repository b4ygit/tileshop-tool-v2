/**
 * Created by soicoi on 9/10/17.
 */

function getDefs(key) {
  // return new Promise(function (resolve, reject) {
  //   var obj = document.getElementById(key.replace("canvas", "object"));
  //   if (key == undefined || obj == undefined) return false;
  //   if (isReload) {
  //     var content = obj.contentDocument || obj.contentWindow;
  //     var svg = content.getElementsByTagName("svg");
  //     var defs = $(svg).find(">:first-child")[0];
  //     resolve([obj, defs]);
  //   }

  //   $(obj).on("load", function () {
  //     var content = this.contentDocument || this.contentWindow;
  //     var svg = content.getElementsByTagName("svg");
  //     var defs = $(svg).find(">:first-child")[0];
  //     resolve([obj, defs]);
  //   });
  // });
  var obj = document.getElementById(key.replace("canvas", "object"));
  if (key == undefined || obj == undefined) return false;
  var content = obj.contentDocument || obj.contentWindow;
  var svg = content.getElementsByTagName("svg");
  var defs = $(svg).find(">:first-child")[0];
  return [obj, defs];
}

function applyColor(key) {
  if (
    key === "background-color" &&
    document.getElementById("layerBackground") != undefined
  ) {
    $("#layerBackground img").css("background-color", layerCanvas[key]);
    return false;
  }

  var def = getDefs(key);
  if (!def) return false;
  else def = def[1];
  $(def)
    .find("style")
    .text("*{fill:" + layerCanvas[key]["color"] + ";}");
}

function doSyncLayer(layerId) {
  if (typeof layerCanvas == "undefined" || layerCanvas == null) return false;

  if (layerId.length < 1) {
    // sync all layers
    $.each(layerCanvas, function (key, value) {
      // apply color
      applyColor(key);
      // apply transform
    });
  } else {
    // apply color
    applyColor(layerId);
    // apply transform
  }
  adjustScreenSize();
}

function doSyncPreviewLayer(layerId) {
  if (typeof layerCanvas == "undefined" || layerCanvas == null) return false;

  if (
    layerId == "background-color" ||
    layerId == "" ||
    typeof layerCanvas[layerId] == "undefined"
  ) {
    return false;
  }

  syncPreviewLayer(layerId, !isHover);
  var type = $("#myTabContent > div.tab-pane.show.active").attr("id");
  type = type.split("-")[1];
  syncShapeCoverPreview(type);
  // $(".preview-canvas-parent .not-image-on-top-preview.lower-canvas").css("background-color", layerCanvas['background-color']);
  $(".preview-canvas-parent").css(
    "background-color",
    layerCanvas["background-color"]
  );
  adjustScreenSize();
}

function syncPreviewLayer(layerId, syncWithSmallPreview) {
  var imgSrc = layerCanvas[layerId]["imgSource"];

  var color = layerCanvas[layerId]["color"];

  var rotateDegree = parseInt(layerCanvas[layerId]["rotateDegree"]);

  var position = layerCanvas[layerId]["position"];
  position = position.split(",");
  var left = parseFloat(position[0]);
  var top = parseFloat(position[1]);

  var scale = layerCanvas[layerId]["scale"];
  scale = scale.split(",");
  var scaleX = parseFloat(scale[0]);
  var scaleY = parseFloat(scale[1]);

  var skew = layerCanvas[layerId]["skew"];
  skew = skew.split(",");
  var skewX = parseFloat(skew[0]);
  var skewY = parseFloat(skew[1]);

  var previewRatio = 1.5597;
  var ratio = screenRatio / previewRatio;

  $(".preview-canvas").remove();
  var activeTab = $("#myTabContent > div.tab-pane.show.active").find(
    ".preview-canvas-parent"
  );

  var coverImgSrc = $("#imageUrlOfOnMainPreviewShape").val();
  if ($("#managerSection").hasClass("hide")) {
    coverImgSrc = $("#defaultImageUrlOfOnMainPreviewShape").val();
  }

  $("#preview-canvas-img").remove();
  $("#on-top-preview-canvas-img").remove();
  $(activeTab).html(
    '<img src="' +
      coverImgSrc +
      '" id="preview-canvas-img" alt="preview image" width="200" height="200" />' +
      '<img src="' +
      coverImgSrc +
      '" id="on-top-preview-canvas-img" alt="cover preview image" width="200" height="200" />'
  );

  $("canvas#" + hiddenCanvasID)
    .parents(".canvas-container")
    .hide();
  $("canvas#" + hiddenCanvasID).hide();

  if (!checkIsImageExist(imgSrc)) {
    return;
  }

  fabric.loadSVGFromURL(imgSrc, function (objects, options) {
    var obj = fabric.util.groupSVGElements(objects, options);
    obj
      .set({
        // Default value
        width: tmpSize,
        height: tmpSize,
        selectable: true,
        centeredScaling: true,
        centeredRotation: true,
        transformMatrix: [scaleX, skewY, skewX, scaleY, 0, 0],
        left: left / previewRatio,
        top: top / previewRatio,
        hasControls: false,
        hasBorders: false,
        fill: color || defaultColor,
      })
      .scale(ratio, false)
      .rotate(rotateDegree);

    hiddenCanvas.setWidth(257).setHeight(257).clear().add(obj);

    var canvasCurrentMainObject = hiddenCanvas.getObjects();
    if (typeof canvasCurrentMainObject[0]._objects != "undefined") {
      var canvasCurrentObjects = canvasCurrentMainObject[0]._objects;
      for (
        var objCount = 0;
        objCount < canvasCurrentObjects.length;
        objCount++
      ) {
        canvasCurrentObjects[objCount].fill = color || defaultColor;
      }
    }

    $("#preview-canvas-img").attr("src", hiddenCanvas.toDataURL());
    if (syncWithSmallPreview) {
      $("#smallPreviewImage_" + layerId).attr("src", hiddenCanvas.toDataURL());
      $("#smallPreviewImage_" + layerId).css(
        "background-color",
        layerCanvas["background-color"]
      );
      $("#smallPreviewImage_" + layerId).css("position", "absolute");
      $("#smallPreviewImage_" + layerId).css("overflow", "hidden");
      $("#smallPreviewImage_" + layerId).css("left", "50%");
      $("#smallPreviewImage_" + layerId).css("transform", "translateX(-50%)");
      $("#small-preview-layer-cover-" + layerId).remove();
      var smallPreviewLayerCover = $("#addLayerCover").clone();
      $("#smallPreviewImage_" + layerId)
        .parents("a")
        .append(smallPreviewLayerCover);
      $(smallPreviewLayerCover).attr(
        "id",
        "small-preview-layer-cover-" + layerId
      );
      $(smallPreviewLayerCover).css("position", "absolute");
      var coverSrc =
        $("#imageUrlOfOnSmallPreviewShape").val() ||
        $("#defaultImageUrlOfOnSmallPreviewShape").val();
      $(smallPreviewLayerCover).attr("src", coverSrc);
      $(smallPreviewLayerCover).addClass("small-preview-layer-cover");
    }
  });

  $("#" + hiddenCanvasID).hide();
  $("#" + hiddenCanvasID)
    .parents(".canvas-container")
    .hide();
  $("#preview-canvas-img").css("position", "absolute");
  $("#preview-canvas-img").css("overflow", "hidden");
}

function syncShapeCoverPreview(type) {
  if (type == "color") {
    return false;
  }
  var coverImgSrc = $("#imageUrlOfOnMainPreviewShape").val();
  if ($("#managerSection").hasClass("hide")) {
    coverImgSrc = $("#defaultImageUrlOfOnMainPreviewShape").val();
  }

  var previewRatio = 1.5597;
  var ratio = screenRatio / previewRatio;

  if (!(checkIsImageExist(imgSrc))) {
    return;
  }
  fabric.loadSVGFromURL(coverImgSrc, function (objects, options) {
    var obj = fabric.util.groupSVGElements(objects, options);
    obj
      .set({
        // Default value
        width: tmpSize,
        height: tmpSize,
        selectable: false,
        left: 0,
        top: 0,
        hasControls: false,
        hasBorders: false,
      })
      .scale(ratio, false);

    hiddenCanvas.setWidth(257).setHeight(257).clear().add(obj);
    $("#on-top-preview-canvas-img").attr("src", hiddenCanvas.toDataURL());
  });

  $("#on-top-preview-canvas-img").css("position", "absolute");
  $("#on-top-preview-canvas-img").css("z-index", "998");
  $("#on-top-preview-canvas-img").css("overflow", "hidden");
}
