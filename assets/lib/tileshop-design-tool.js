function disableHoverEvents() {
  if (isMobileBrowser()) {
    $("body").off(
      "mouseenter mouseleave",
      "#pattern-list .btn-pattern-item .small-preview-layer-cover"
    );
    $("body").off("mouseenter mouseleave", "#pattern-list li.btn-pattern-item");
    $("body").off("mouseenter mouseleave", "#layerBackground");
    $("body").off("mouseenter mouseleave", "#customizedTool .btn-add-pattern");
    $("body").off(
      "mouseenter mouseleave",
      "#swiper-new-arrivals .element-tile"
    );
    $("body").off("mouseenter mouseleave", "#btnAddMoreLayer");
  }
}

function disableTouchEventOnSafariIosONLY() {
  if (checkIsIos() && checkIsSafariBrowser()) {
    //console.log("IS IOS AND SAFARI");
    $("body").off("touchstart touchend", "body .element-tile");
  }
  //console.log("NOT IS IOS AND SAFARI");
}

jQuery(document).ready(function ($) {
  if ($("#main .error-404").length) {
    return false;
  }

  startLoading();
  /**
   *
   */

  // select pattern
  var patternSelectedImg = document.getElementById("selected-pattern-img");
  var patternButton = document.getElementById("dropdownPattern");
  var dropdownPattern = document.getElementById("dropdownPattern-list");
  dropdownPattern.addEventListener("click", function (e) {
    e.preventDefault();
    if (e.target && e.target.nodeName === "A") {
      patternSelectedImg.src = e.target.childNodes[0].src;
      patternButton.innerHTML = e.target.dataset.text;
    }
  });

  // var loggedIn = document.body.classList.contains("logged-in");
  // if (!loggedIn) {
  //   // $("#shareBtn").remove();
  //   $("#saveBtn").remove();
  // }
  $("#saveBtn").remove();
  $("#shareBtn").remove();

  $("head").append(
    "<title>Tiles.design App - " + $("#productOriginalTitle").val() + "</title>"
  );

  var isCheckedTerazzo = $("#isCheckedTerazzo").prop("value");
  if (isCheckedTerazzo != "0") {
    $("#terazoOpt").prop("checked", true);
  } else {
    $("#terazoOpt").prop("checked", false);
  }

  loadToolBasedOnProductType();

  main();
  makeTooltipForColorsInUsed();
  $('[data-toggle="tooltip"]').tooltip();

  // loadPricingSlider();

  doSyncLayer("");

  doSyncPreviewLayer("");

  disableHoverEvents();
  disableTouchEventOnSafariIosONLY();

  // show or hide Grid
  toggleGrid(document.getElementById("checkbox-grid"));
  $("#checkbox-grid").change(function (e) {
    toggleGrid(this);
  });

  var m_productDesignTypeInitial = $(
    'input[name="product_design_type"]:checked'
  ).val();
  var m_manufacturerInitial = $('input[name="manufacturer"]:checked').val();

  if (
    typeof m_productDesignTypeInitial != "undefined" &&
    typeof m_manufacturerInitial != "undefined"
  ) {
    $('input[name="manufacturer"]:checked').trigger("click");
  }

  var m_productCategories = JSON.parse($("#selectedCategories").val());
  m_productCategories.forEach(function (item, index) {
    $('input[name="categories[]"][data-name="' + item + '"]').prop(
      "checked",
      true
    );
  });
  stopLoading();
});

$("body").on("click touchend", "#toggleModalElementSlider", function () {
  $("#toolAddElementModalCenter").modal();
});
