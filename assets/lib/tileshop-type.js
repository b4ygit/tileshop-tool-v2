jQuery(document).ready(function ($) {
  if (isMobileBrowser()) {
    $("div.tiles-block").addClass("nohover");
    $(".tiles-block div.img-shape").addClass("nohover");
    $("body").off("mouseenter mouseleave", ".tiles-block");
    $("body").off(
      "mouseenter mouseleave",
      ".shape-list .row .col-auto .choose-shape"
    );
  }

  var choose_shape_val;
  var choose_manu_val = $(".nav-link.active").data("manu-id");
  // $('input[name="choose-shape"]').change(function (e) {
  //   // console.log(e.target.value)
  //   // get value of this shape
  //   choose_shape_val = e.target.value;

  //   if (choose_shape_val) {
  //     $("#btn-next-step").removeClass("disabled");
  //     $(".tiles-block").removeClass("show-first-time");
  //   } else {
  //     $("#btn-next-step").addClass("disabled");
  //   }
  // });
  // choose_shape_val = $('input[name="choose-shape"]:checked').val();

  // if (choose_shape_val) {
  //   $(".tiles-block").removeClass("show-first-time");
  // }

  $(".nav-link").click(function () {
    $(".nav-link").removeClass("active");
    $(this).addClass("active");
    var tabID = $(this).data("tab-id");
    $(".tab-pane").removeClass("active");
    $(tabID).addClass("active");
    choose_manu_val = $(this).data("manu-id");
  });

  $("#btn-next-step").click(function () {
    if (!choose_shape_val) {
      alert("Please choose shape.");
      return;
    }

    window.location.href =
      window.location.origin +
      window.location.pathname +
      "?action=style-selected&shape=" +
      choose_shape_val +
      "&manu_id=" +
      choose_manu_val;
  });

  $(".do-select-shapeX").on("click", function () {
    $(".content-item-sharps-tiles").css("border", "");
    $(".content-item-sharps-tiles img").css("filter", "");
    choose_shape_val = $(this).data("shape");
    var swipeIdx = $(this).data("swipe-index");
    var swiperContainerHomeID = $(this)
      .parents(".swiper-container-home")
      .attr("id");
    var selectShapeSwiper = document.getElementById(
      swiperContainerHomeID
    ).swiper;
    $("#btn-next-step").removeClass("active");
    selectShapeSwiper.slideTo(swipeIdx == 0 ? 0 : swipeIdx - 1, 400, false);
    if (choose_shape_val) {
      $("#btn-next-step").removeClass("disabled");

      // $(".tiles-block").removeClass("show-first-time");
     /* $(this)
        .parents(".content-item-sharps-tiles")
        .css("border", "3px solid #4F94F5");
     /* $(this)
        .find("img")
        .css(
          "filter",
          "invert(63%) sepia(92%) saturate(2488%) hue-rotate(175deg ) brightness(100%) contrast(103%)"
        );*/
    } else {
      $("#btn-next-step").addClass("disabled");
    }
  });
});
 