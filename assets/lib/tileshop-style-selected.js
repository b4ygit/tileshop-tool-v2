function getProductsAjax(shape, categories, stock, page) {
  return new Promise(function (resolve, reject) {
    if (!page) {
      page = 1;
    }

    if (!shape) {
      return "";
    }

    $.ajax({
      url: wpParams.ajax_url,
      type: "GET",
      data: {
        action: "getProductsAjax",
        page: page,
        categories: categories.join(","),
        shape: shape,
        stock: stock ? 1 : 0,
      },
    }).done(function (resp) {
      resolve(JSON.parse(resp));
    });
  });
}

function processProducts(products) {
  var html = "";

  if (typeof products == "object") {
    products = Object.entries(products);
  }

  if (products.length > 0) {
    products.forEach(function (val, idx) {
      html +=
        '<div class="col-auto mb-3">' +
        "   <label>" +
        '       <input type="radio" autocomplete="off" name="product" value="' +
        val[1].ID +
        '" class="choose-shape">' +
        '       <div class="design-block text-center rounded">' +
        '           <div class="img-design">' +
        '               <img src="' +
        val[1].thumbImg +
        '" alt="">' +
        "           </div>" +
        "           <h6>" +
        val[1].post_title +
        "</h6>" +
        "       </div>" +
        "   </label>" +
        "</div>";
    });
  }

  return html;
}

function renderProductsHtml(html) {
  $("#style-selected-contaienr .row").html(html);
  $("#style-selected-next-btn").addClass("disabled");
}

jQuery(document).ready(function ($) {
  var selectedShape = $('input[name="selectedShape"]').val();
  var selectedManuID = $('input[name="selectedManuID"]').val();
  var product_val;
  $(document).on("change", 'input[name="product"]', function (e) {
    product_val = e.target.value;

    if (product_val) {
      $("#style-selected-next-btn").removeClass("disabled");
    } else {
      $("#style-selected-next-btn").addClass("disabled");
    }
  });
  var category_val = $('input[name="selectedCategory"]').val();
  var categories = [];

  if (category_val) {
    categories = category_val.split(",");
  }

  categories = category_val ? category_val.split(",") : [];
  var productPage = 1;
  $('input[name="category"]').click(function (e) {
    productPage = 1;

    if (e.target.checked) {
      categories.push(e.target.value);
    } else {
      var index = categories.indexOf(e.target.value);

      if (index > -1) {
        categories.splice(index, 1);
      }
    }

    $('input[name="selectedCategory"]').val(categories);
    getProductsAjax(
      selectedShape,
      categories,
      $('input[name="stockOpt"]').prop("checked"),
      productPage
    )
      .then(function (resp) {
        return processProducts(resp.products);
      })
      .then(function (html) {
        renderProductsHtml(html);
      });
  });
  $('input[name="stockOpt"]').change(function (e) {
    productPage = 1;
    getProductsAjax(selectedShape, categories, e.target.checked, productPage)
      .then(function (resp) {
        return processProducts(resp.products);
      })
      .then(function (html) {
        renderProductsHtml(html);
      });
  });
  $(window).scroll(function () {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
      productPage++;
      getProductsAjax(
        selectedShape,
        categories,
        $('input[name="stockOpt"]').prop("checked"),
        productPage
      )
        .then(function (resp) {
          var html = $("#style-selected-contaienr .row").html();
          html += processProducts(resp.products);
          return html;
        })
        .then(function (html) {
          renderProductsHtml(html);
        });
    }
  });
  $("#style-selected-next-btn").click(function () {
    window.location.href = [
      window.location.origin,
      window.location.pathname,
      "?action=design&product_id=",
      product_val,
      "&shape=",
      selectedShape,
      "&manu_id=",
      selectedManuID,
    ].join("");
  });
});
