Vue.component('action-menu', {
    data: function () {
      return {
        // count: 0
      }
    },
    template: `<div class="pattern-action-menu">
        <button class="action-menu-up-down"><i class="ti-angle-up"></i></button>
        <button class="btn-copy"></button>
        <button class="btn-trash"></button>
        <button class="action-menu-up-down"><i class="ti-angle-down"></i></button>
    </div>`
  })