var container = document.querySelector('[data-ref="container"]');
if (container != null) {
  var mixer = mixitup(container, {
    animation: {
      duration: 350,
    },
  });
}

// map
function new_map($el) {
  var $markers = $el.find(".marker");
  var args = {
    scrollwheel: false,
    zoom: 16,
    center: new google.maps.LatLng(0, 0),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [
      {
        //              featureType: 'road',
        //              elementType: 'geometry',
        stylers: [
          {
            hue: -180,
          },
          {
            lightness: 0,
          },
          {
            saturation: -100,
          },
        ],
      },
    ],
    //		disableDefaultUI: true,
  };
  var map = new google.maps.Map($el[0], args);
  map.markers = [];
  $markers.each(function () {
    add_marker($(this), map);
  });
  center_map(map);
  return map;
}

function add_marker($marker, map) {
  var latlng = new google.maps.LatLng(
    $marker.attr("data-lat"),
    $marker.attr("data-lng")
  );
  //	console.log(latlng);
  var marker = new google.maps.Marker({
    position: latlng,
    icon: "assets/img/map-marker.png",
    map: map,
    animation: google.maps.Animation.DROP,
  });

  //	console.log(marker);
  map.markers.push(marker);
  if ($marker.html()) {
    var infowindow = new google.maps.InfoWindow({
      content: $marker.html(),
    });
    google.maps.event.addListener(marker, "click", function () {
      infowindow.open(map, marker);
      $(".ori-container").css("display", "none");
      $(".gm-style-iw").parent("div").addClass("ori-container");
      map.setCenter(marker.getPosition());
    });
  }
}

function center_map(map) {
  var bounds = new google.maps.LatLngBounds();
  var flightPlanCoordinates = [];
  //var firstPlan = '';
  $.each(map.markers, function (i, marker) {
    var latlng = new google.maps.LatLng(
      marker.position.lat(),
      marker.position.lng()
    );
    bounds.extend(latlng);
    //		console.log(i);
    // if(i==0){
    // 	firstPlan = {lat: marker.position.lat(), lng: marker.position.lng()};
    // 	console.log(firstPlan);
    // }
    flightPlanCoordinates.push({
      lat: marker.position.lat(),
      lng: marker.position.lng(),
    });
  });
  //console.log(firstPlan);
  //flightPlanCoordinates.push(firstPlan);
  if (map.markers.length == 1) {
    map.setCenter(bounds.getCenter());
    map.setZoom(16);
  } else {
    map.fitBounds(bounds);
  }
}
var map = null;

$(".js-acf-map").each(function () {
  map = new_map($(this));
});

function loadSwiperSlider() {

    //////NEW FOR MULTI PRODUCT
   $('.swiper-container-home').each(function () {

            var {prev, next, pagination} = $(this).data();
            var getId = $(this).attr('id');
            var thisSwiper = getId==null?$(this):document.getElementById(getId);
            var slidesPerView = $(this).data('slides-perview') == null ? 1 : $(this).data('slides-perview');
            var spaceBetween = $(this).data('space-between') == null ? 30 : $(this).data('space-between');
            var slidesPerGroup = $(this).data('slides-pergroup') == null ? 1 : $(this).data('slides-pergroup');
            var slidesPerColumn = $(this).data('slides-percolumn') == null ? 1 : $(this).data('slides-percolumn');
            var slidesLoop = $(this).data('slides-loop') == null ? false : $(this).data('slides-loop');
            var slidesDirection = $(this).data('slides-direction') == null ? 'horizontal' : $(this).data('slides-direction');

            var slidesTablet = $(this).data('slides-tablet') == null ? slidesPerView : $(this).data('slides-tablet');
            var slidesMobile = $(this).data('slides-mobile') == null ? slidesPerView : $(this).data('slides-mobile');
            var slidesType = $(this).data('slides-type') == null ? 'bullets' : $(this).data('slides-type');

            prev = prev==null?$(this).parent().find('.swiper-button-prev'):prev;
            next = next==null?$(this).parent().find('.swiper-button-next'):next;
            pagination = pagination==null?$(this).parent().find('.swiper-pagination'):pagination;
            new Swiper(thisSwiper, {
                slidesPerView: slidesPerView,
                slidesPerColumn: slidesPerColumn,
                spaceBetween: spaceBetween,
                slidesPerGroup: slidesPerGroup,
                loop: slidesLoop,
                direction:slidesDirection,
                pagination: {
                    el: pagination,
                    clickable: true
                },
                navigation: {
                    nextEl: next,
                    prevEl: prev,
                },
                breakpoints: {
                    1199: {
                        slidesPerView: slidesTablet
                    },
                    767: {
                        slidesPerView: slidesMobile
                    }
                },
                observer: true,
                observeParents: true,
                watchOverflow: true

            })



        });
  $(".swiper-container").each(function () {
    var { prev, next, pagination } = $(this).data();
    var getId = $(this).attr("id");
    var thisSwiper = getId == null ? $(this) : document.getElementById(getId);
    // var slidesPerView = $(this).data('slides-perview') == null ? 1 : $(this).data('slides-perview');
    var slidesPerView = 1;
    var spaceBetween =
      $(this).data("space-between") == null
        ? 30
        : $(this).data("space-between");
    var slidesPerGroup =
      $(this).data("slides-pergroup") == null
        ? 1
        : $(this).data("slides-pergroup");
    var slidesPerColumn =
      $(this).data("slides-percolumn") == null
        ? 1
        : $(this).data("slides-percolumn");
    var slidesLoop =
      $(this).data("slides-loop") == null ? false : $(this).data("slides-loop");

    var slidesTablet = 1;
    var slidesMobile = 1;

    prev = prev == null ? $(this).parent().find(".swiper-button-prev") : prev;
    next = next == null ? $(this).parent().find(".swiper-button-next") : next;
    pagination =
      pagination == null
        ? $(this).parent().find(".swiper-pagination")
        : pagination;
    new Swiper(thisSwiper, {
      slidesPerView: slidesPerView,
      slidesPerColumn: slidesPerColumn,
      spaceBetween: spaceBetween,
      slidesPerGroup: slidesPerGroup,
      lazyLoading: true,
      loop: slidesLoop,
      pagination: {
        el: pagination,
        clickable: true,
      },
      navigation: {
        nextEl: next,
        prevEl: prev,
      },
      breakpoints: {
        1199: {
          slidesPerView: slidesTablet,
        },
        767: {
          slidesPerView: slidesMobile,
        },
      },
      on: {
        slideNextTransitionEnd: function () {
          var end = $("#swiper-button-next-element").attr("data-end-list");
          if (!parseInt(end)) {
            $("#swiper-button-next-element").removeClass(
              "swiper-button-disabled"
            );
          }
        },
        init: function () {
          $("#swiper-button-prev-element").css("background-color", "#fff");
          $("#swiper-button-prev-element").find("path").css("fill", "#A1AAB6");

          $("#swiper-button-next-element").css("background-color", "#4F94F5");
          $("#swiper-button-next-element").find("path").css("fill", "#fff");
        },
        slideChange: function () {
          if (
            !$("#swiper-button-prev-element").hasClass("swiper-button-disabled")
          ) {
            $("#swiper-button-prev-element").css("background-color", "#4F94F5");
            $("#swiper-button-prev-element").find("path").css("fill", "#fff");
          } else {
            $("#swiper-button-prev-element").css("background-color", "#fff");
            $("#swiper-button-prev-element")
              .find("path")
              .css("fill", "#A1AAB6");
          }
        },
      },
    });
  });
}
loadSwiperSlider();
 $('.content-item-sharps-tiles').click(function () {
       if($(this).attr('data-ative')==="0") {
           $(this).addClass('active');
           $(this).attr('data-ative',1)
       }else{
                      $(this).removeClass('active');
           $(this).attr('data-ative',0)
       }

    });


if ($(".swiper-why-trio-living").length >= 1) {
  var swiper = new Swiper(".swiper-why-trio-living", {
    direction: "vertical",
    slidesPerView: 1,
    navigation: {
      nextEl: ".swiper-trio-living-button-next",
      prevEl: ".swiper-trio-living-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
  });
}
if ($(".swiper-date-price").length >= 1) {
  var swiper = new Swiper(".swiper-date-price", {
    direction: "vertical",
    slidesPerView: 1,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    breakpoints: {
      991: {
        direction: "horizontal",
        slidesPerView: 1,
      },
      768: {
        direction: "horizontal",
        slidesPerView: 1,
      },
      575: {
        direction: "horizontal",
        slidesPerView: 1,
      },
    },
  });
}

function swipercart2() {
  if ($(".swiper-cart2").length >= 1) {
    swiperCart2 = new Swiper(".swiper-cart2", {
      slidesPerView: 1,
      spaceBetween: 20,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
    });
  }
}
swipercart2();

if ($(".swiper-calendar").length >= 1) {
  var swiper = new Swiper(".swiper-calendar", {
    slidesPerView: 1,
    spaceBetween: 0,

    navigation: {
      nextEl: ".swiper-calendar-nav.right",
      prevEl: ".swiper-calendar-nav.left",
    },
    breakpoints: {
      991: {
        slidesPerView: 1,
      },
      575: {
        slidesPerView: 1,
      },
    },
  });
}

if ($(".swiper-thumb-list").length >= 1) {
  var swiper = new Swiper(".swiper-thumb-list", {
    slidesPerView: 1,
    slidesPerColumn: 2,
    spaceBetween: 30,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    breakpoints: {
      991: {
        spaceBetween: 10,
        slidesPerView: 1,
      },
      575: {
        spaceBetween: 10,
        slidesPerView: 1,
      },
    },
  });
}

if ($(".swiper-property").length >= 1) {
  var swiper = new Swiper(".swiper-property", {
    slidesPerView: 1,
    slidesPerColumn: 2,
    spaceBetween: 30,
    pagination: {
      el: ".swiper-property-pagination",
      clickable: true,
      type: "fraction",
    },
    navigation: {
      nextEl: ".swiper-property-nav.next",
      prevEl: ".swiper-property-nav.prev",
    },
    breakpoints: {
      991: {
        spaceBetween: 10,
        slidesPerView: 1,
        slidesPerColumn: 1,
      },
      767: {
        spaceBetween: 10,
        slidesPerView: 1,
        slidesPerColumn: 2,
      },
      575: {
        spaceBetween: 10,
        slidesPerView: 1,
        slidesPerColumn: 1,
      },
    },
  });
}

if ($(".swiper-unit").length >= 1) {
  var swiper = new Swiper(".swiper-unit", {
    slidesPerView: 1,
    spaceBetween: 0,
    pagination: {
      el: ".swiper-unit-pagination",
      clickable: true,
      type: "fraction",
    },
    navigation: {
      nextEl: ".swiper-unit-nav.next",
      prevEl: ".swiper-unit-nav.prev",
    },
  });
}

if ($(".swiper-unit-banner").length >= 1) {
  var swiper = new Swiper(".swiper-unit-banner", {
    slidesPerView: 1,
    spaceBetween: 0,
    navigation: {
      nextEl: ".swiper-unit-banner-nav.next",
      prevEl: ".swiper-unit-banner-nav.prev",
    },
  });
  swiper.on("slideChange", function () {
    if (trionVideo != null && !trionVideo.paused()) {
      trionVideo.pause();
    }
  });
}

$(document).ready(function () {
  $(".map-choose .point").click(function () {
    var mapContentTop = $("#map-content").position().top;
    $("body, html").animate(
      {
        scrollTop: mapContentTop,
      },
      500
    );
  });
  $("[data-scrollto]").click(function (e) {
    e.preventDefault();
    var scrollID = $(this).data("scrollto");
    $("body, html").animate(
      {
        scrollTop: $("[data-id=" + scrollID + "]").position().top,
      },
      300
    );
  });
});

var myPlayer = null;
var isPaused = null;
if ($("#home-banner-app").length >= 1) {
  var homebannerapp = new Vue({
    el: "#home-banner-app",
    data: {
      index: 0,
      count: 0,
      enableautoplay: true,
    },
    methods: {
      autoplay: function () {
        this.count = $("#home-banner-app .banner-item").length;
        var autoplay = $("#home-banner-app").data("autoplay");
        var speed = $("#home-banner-app").data("speed");
        if (autoplay) {
          setInterval(function () {
            if (homebannerapp.enableautoplay) {
              if (homebannerapp.index >= homebannerapp.count - 1) {
                homebannerapp.index = 0;
              } else {
                homebannerapp.index++;
              }
            }
          }, speed);
        }
      },
    },
    created: function () {
      this.autoplay();
    },
    watch: {
      index: function () {
        if (myPlayer != null) {
          myPlayer.pause();
        }
        var banner_item = $("#home-banner-app .banner-item").eq(this.index);
        var video = banner_item.hasClass("video");
        var videoID = banner_item.find("video").attr("id");
        var wWidth = $(window).width();
        var wHeight = $(window).height();
        if (video) {
          this.enableautoplay = false;
          videojs(videoID).ready(function () {
            myPlayer = this;
            var tile =
              wHeight / myPlayer.videoHeight() > wWidth / myPlayer.videoWidth()
                ? wHeight / myPlayer.videoHeight()
                : wWidth / myPlayer.videoWidth();
            banner_item.find(".video-js").css({
              transform: "translate3d(-50%, -50%, 0) scale(" + tile + ")",
            });
            // alert(myPlayer.videoWidth() + ', ' + myPlayer.videoHeight());
            myPlayer.play();
            myPlayer.on("ended", function () {
              homebannerapp.enableautoplay = true;
            });
          });
        } else {
          this.enableautoplay = true;
        }
        // 	if(this.index===1) {
        // 		this.enableautoplay = false;

        // 		videojs("video_banner2").ready(function(){
        //   myPlayer = this;
        //   myPlayer.play();
        //   myPlayer.on('ended', function() {
        //     homebannerapp.enableautoplay = true;
        //   });
        // });
        // 	}
      },
    },
  });
}

$(".gallery").each(function () {
  // the containers for all your galleries
  $(this).magnificPopup({
    delegate: "a", // the selector for gallery item
    type: "image",
    gallery: {
      enabled: true,
    },
    mainClass: "mfp-fade",
  });
});

if ($(".popup-youtube").length >= 1) {
  popupShow(".popup-youtube");
}
if ($(".popup-vimeo").length >= 1) {
  popupShow(".popup-vimeo");
}
if ($(".popup-gmaps").length >= 1) {
  popupShow(".popup-gmaps");
}

function popupShow(element) {
  $(element).magnificPopup({
    // disableOn: 700,
    type: "iframe",
    mainClass: "mfp-fade",
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false,
  });
}

if ($(".datepicker").length >= 1) {
  $(".datepicker").datetimepicker({
    format: "DD/MM/YYYY",
  });
}
if ($(".timepicker").length >= 1) {
  $(".timepicker").datetimepicker({
    format: "LT",
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-angle-up",
      down: "fa fa-angle-down",
    },
  });
}

$(".lbl-expand").click(function (e) {
  e.preventDefault();
  $("#des5-itinerary-list .faq-answer").collapse("show");
});
$(".lbl-collapse").click(function (e) {
  e.preventDefault();
  $("#des5-itinerary-list .faq-answer").collapse("hide");
});

var trionVideo = null;
$(document).on("click", ".banner-icon-play", function () {
  if (trionVideo != null) {
    trionVideo.pause();
  }
  var videoID = $(this).data("id");
  var videoPoster = $(this).data("poster");
  trionVideo = videojs(videoID);
  trionVideo.poster("assets/img/unit-banner.jpg");
  $(this).hide();
  $(this).parent().find(".content, .img").fadeOut();
  $(this).parent().find(".vjs-big-play-button").trigger("click");
});

var sliderRange = document.getElementById("sliderRange");
if (sliderRange) {
  noUiSlider.create(sliderRange, {
    range: {
      min: Number(sliderRange.dataset.min),
      max: Number(sliderRange.dataset.max),
    },
  });
  sliderRange.noUiSlider.on("update", function (values, handle) {
    $("#sliderRangeMin").val(values[0]);
    $("#sliderRangeMax").val(values[1]);
  });
}

$(".input-search-wrap .input-search").focus(function () {
  $(this).addClass("focused");
});
$(".input-search-wrap .input-search").blur(function () {
  if ($(this).val() === "") {
    $(this).removeClass("focused");
  }
});

$("select.nice-select").niceSelect();

//$(".style-sticker").sticky({ topSpacing: 64 });

function setRootMargin() {
  $("#root").css({ "margin-bottom": $("#footer-design-wrap").height() });
}
// setRootMargin();
$(window).resize(function () {
  debounce(setRootMargin(), 1000);
});

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});


