Vue.component('countdown', {
  template: `
    <div class="date-time">
        <div>
            <div>{{ days | two_digits }}</div>
            <div>Days</div>
        </div>
        <div>
            <div>{{ hours | two_digits }}</div>
            <div>Hours</div>
        </div>
        <div>
            <div>{{ minutes | two_digits }}</div>
            <div>Mins</div>
        </div>
        <div>
            <div>{{ seconds | two_digits }}</div>
            <div>Secs</div>
        </div>
    </div>
  `,
  mounted() {
    window.setInterval(() => {
        this.now = Math.trunc((new Date()).getTime() / 1000);
    },1000);
  },
  props: {
    date: {
      type: String
    }
  },
  data() {
    return {
      now: Math.trunc((new Date()).getTime() / 1000)
    }
  },
  computed: {
    dateInMilliseconds() {
      return Math.trunc(Date.parse(this.date) / 1000)
    },
    seconds() {
      return (this.dateInMilliseconds - this.now) % 60;
    },
    minutes() {
      return Math.trunc((this.dateInMilliseconds - this.now) / 60) % 60;
    },
    hours() {
      return Math.trunc((this.dateInMilliseconds - this.now) / 60 / 60) % 24;
    },
    days() {
      return Math.trunc((this.dateInMilliseconds - this.now) / 60 / 60 / 24);
    }
  }
});

Vue.filter('two_digits', (value) => {
  if (value < 0) {
    return '00';
  }
  if (value.toString().length <= 1) {
    return `0${value}`;
  }
  return value;
});