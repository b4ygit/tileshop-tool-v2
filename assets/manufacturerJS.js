/**
 * Event handler for manufacturer page
 */
function checkValidity(accordion) {
  var notEmpty = 1;
  $(accordion)
    .find("input[required]")
    .each(function (key, value) {
      if ($(value).val() === "") {
        notEmpty *= 0;
        $(value).css("border-color", "red");
      } else {
        $(value).css("border-color", "#bbb");
      }
    });
  return notEmpty;
}
jQuery("body").on("click", ".saveManuShapeMetadata", function () {
  var row = $(this).closest(".dataRow");
  var summary = $(row).find("textarea[name=summary]").val();
  var isEnabled = $(row).find("input[name=is_enabled]:checked").val();
  startLoading();
  jQuery
    .ajax({
      url: wpParams.ajax_url + "?action=updateManuShapeMetadata",
      type: "POST",
      data: {
        manuID: $("#manuID").val(),
        shapeSlug: $(row).find(".shapeSlug").val(),
        summary: summary,
        isEnabled: isEnabled == "on" ? 1 : 0,
      },
    })
    .done(function (resp) {
      var response = JSON.parse(resp);
      alert(response.message);
      stopLoading();
    });
});
jQuery("body").on("click", ".updateManufacturerData", function () {
  var row = $(this).closest(".dataRow");
  var accordion = $(this).closest(".accordion");
  if (checkValidity(accordion) === 0) {
    alert("Please fulfill valid all red fields.");
    return false;
  }

  startLoading();
  var thickness = $(accordion).find("input[name=thickness]").val();
  thickness = thickness.split(",");
  var uniqueThickness = thickness.filter(function (elem, index, self) {
    return index == self.indexOf(elem);
  });
  uniqueThickness = uniqueThickness.join(",");
  $(accordion).find("input[name=thickness]").val(uniqueThickness);
  jQuery
    .ajax({
      url: wpParams.ajax_url + "?action=updateManufacturerData",
      type: "POST",
      data: {
        manuID: $("#manuID").val(),
        shapeSlug: $(row).find(".shapeSlug").val(),
        sizeSlug: $(accordion).find(".sizeSlug").val(),
        color_unit_price: $(accordion)
          .find("input[name=color_unit_price]")
          .val(),
        terazo_price: $(accordion).find("input[name=terazo_price]").val(),
        terazo_link: $(accordion).find("input[name=terazo_link]").val(),
        base_price: $(accordion).find("input[name=base_price]").val(),
        thickness: $(accordion).find("input[name=thickness]").val(),
        bpp: $(accordion).find("input[name=bpp]").val(),
        ppa: $(accordion).find("input[name=ppa]").val(),
        ppb: $(accordion).find("input[name=ppb]").val(),
        ppp: $(accordion).find("input[name=ppp]").val(),
        wpt: $(accordion).find("input[name=wpt]").val(),
        wpb: $(accordion).find("input[name=wpb]").val(),
        image_url_of_on_top_shape: $(accordion)
          .find("input[name=image_url_of_on_top_shape]")
          .val(),
        image_url_of_on_add_more_shape: $(accordion)
          .find("input[name=image_url_of_on_add_more_shape]")
          .val(),
        image_url_of_on_add_more_hover_shape: $(accordion)
          .find("input[name=image_url_of_on_add_more_hover_shape]")
          .val(),
        image_url_of_on_background_shape: $(accordion)
          .find("input[name=image_url_of_on_background_shape]")
          .val(),
        image_url_of_on_background_hover_shape: $(accordion)
          .find("input[name=image_url_of_on_background_hover_shape]")
          .val(),
        image_url_of_on_small_preview_shape: $(accordion)
          .find("input[name=image_url_of_on_small_preview_shape]")
          .val(),
        image_url_of_on_small_preview_hover_shape: $(accordion)
          .find("input[name=image_url_of_on_small_preview_hover_shape]")
          .val(),
        image_url_of_on_top_tile_shadow_shape: $(accordion)
          .find("input[name=image_url_of_on_top_tile_shadow_shape]")
          .val(),
        image_url_of_main_preview_shape: $(accordion)
          .find("input[name=image_url_of_main_preview_shape]")
          .val(),
        template_configuration_data: $(accordion)
          .find("select[name=template_configuration_data]")
          .val(),
        option_1: $(accordion).find("input[name=option_1]").val(),
        option_2: $(accordion).find("input[name=option_2]").val(),
      },
    })
    .done(function (resp) {
      var response = JSON.parse(resp);
      alert(response.message);
      // var thickness = response.thickness;
      // if(thickness.length > 0) {
      //     $(accordion).find('input[name=thickness]').val(thickness.toString());
      // }
      // var accordionRelatedList = $(row).find('.accordion-block');
      // $.each(accordionRelatedList, function(idx,elem) {
      //     $(elem).find('input[name=image_url_of_on_top_shape]').val($(accordion).find('input[name=image_url_of_on_top_shape]').val());
      // });

      // $(accordion).find('input').css('border-color', '#bbb');
      stopLoading();
    });
});
