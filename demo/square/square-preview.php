<!DOCTYPE html>
<html>
<head>
	<title>Demo square grid</title>
	<script type="text/javascript" src="../../assets/raphael.min.js"></script>

</head>
<body>

<script type="text/javascript">

    function loadCustPattern(rect) {
        console.log(rect);
        var img = new Image();
        img.src = "http://i1.sndcdn.com/avatars-000070344618-tv261u-large.jpg?30a2558";
        r.image(img.src,rect.attrs.x, rect.attrs.y, rect.attrs.width, rect.attrs.height);
    }

    var r = Raphael(0, 0, 300, 150);

    var r1 = r.rect(0, 0, 50, 50)
        .attr({fill: "#000"})
        .click(function () {
            loadCustPattern(this);
        });

    var r2 = r.rect(51, 0, 50, 50)
        .attr({fill: "#000"})
        .click(function () {
            loadCustPattern(this);
        });

    var r3 = r.rect(0, 51, 50, 50)
        .attr({fill: "#000"})
        .click(function () {
            loadCustPattern(this);
        });

    var r4 = r.rect(51, 51, 50, 50)
        .attr({fill: "#000"})
        .click(function () {
            loadCustPattern(this);
        });

</script>


</body>
</html>